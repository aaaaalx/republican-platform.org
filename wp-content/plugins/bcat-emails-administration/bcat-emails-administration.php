<?php
/*
Plugin Name: BCat emails administration
Description: Allows to receive emails sent by "Contact Form 7" in admin panel
Version: 1.0
Author: MasterAlex
*/

add_action( 'wpcf7_mail_sent', 'bcat_emails_administration' );

function bcat_emails_administration($contact_form){

    $title = $contact_form->title;
    $submission = WPCF7_Submission::get_instance();
    if ( $submission ) {
        $posted_data = $submission->get_posted_data();
        $posted_data = array_diff_key( $posted_data, [
            '_wpcf7' => '',
            '_wpcf7_version' => '',
            '_wpcf7_locale' => '',
            '_wpcf7_unit_tag' => '',
            '_wpcf7_container_post' => '',
            'mc4wp_checkbox' => '',
        ] );
    }

    echo "<br>title = \n\r";
    print_r($title);
    echo "<br>posted_data = \n\r";
    print_r($posted_data);
    echo "<br>submission = \n\r";
    print_r($submission);
    echo "<br>contact_form = \n\r";
    print_r($contact_form);
    die;
}