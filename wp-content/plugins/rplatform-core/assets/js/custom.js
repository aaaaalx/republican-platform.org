jQuery(document).ready(function($) {
    'use strict';

    $(".header").addClass("fixed");

    $(".pos-reletive .to-bottom").on('click', function (e) {
        var parent = $(this).parent();
        var height = parent.find('[data-purpose="block-height"]').position().top;

        if(!parent.hasClass('collapsed')){
            parent.addClass('collapsed');
            parent.css('max-height', '700px');
        }
        else{
            parent.removeClass('collapsed');
            parent.css('max-height', height+'px');
        }
    });

    $('.video-section, .photo-section').css('display', 'block');

    // Video Carosuel
    var $viddeocarosuel_2 = $('.rplatform-video-carosuel-2');
    $viddeocarosuel_2.owlCarousel({
        loop:true,
        dots:true,
        nav:true,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        margin:30,
        autoplay:false,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        autoHeight: false,
        lazyLoad:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                margin:0,
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:3,
                nav:false
            },
            1300:{
                items:4
            }
        }
    });

    //phone mask
    $('input[type="tel"]').mask("+38 (999) 999-99-99");

    /*btn to-top*/
    var amountScrolled = 200;

    $(window).scroll(function() {
        if ( $(window).scrollTop() > amountScrolled ) {
            $('.to-top').fadeIn('slow');
        } else {
            $('.to-top').fadeOut('slow');
        }
    });
    $('.to-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 700);
        return false;
    });

    $(".gallery").find('br').remove();
    $(".gallery").addClass('owl-carousel');
    $(".gallery").owlCarousel( {
        loop:false,
        dots:false,
        nav:true,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        margin:30,
        autoplay:false,
        autoplayTimeout:3000,
        autoHeight:false,
        lazyLoad:true,
        responsive:{
            0:{
                items:1,
                nav:false,
                margin:0,
            },
            600:{
                items:2,
                nav:false
            },
            1000:{
                items:3,
                nav:false
            },
            1300:{
                items:4
            }
        }
    } );

    $('.top-button').on('mouseenter', function (e) {
        var $this = $(this);
        setTimeout(function () {
            $this.removeClass('hover');
        }, 300);
    });
    $('.top-button').on('mouseleave', function (e) {
        $(this).addClass('hover');
    });
    $('.top-button').on('click', function (e) {
        if($(this).hasClass('hover')){
            e.preventDefault();
        }
    });

    $('.prettySocial').prettySocial();
    $('.share-button').simpleSocialShare();

    $('.paymentform input[type="submit"]').click(function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        if(validate(form)){
            var container = form.parent();
            container.addClass('loading');
            var url = form.data('url');
            var data = {
                'action': 'payment_userdata',
                'data' : form.serializeArray()
            };
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                dataType: 'json',
                success:function(data){
                    if(data){
                        if(data.form){
                            container.find('.liqpay-button').html(data.form);
                            container.find('.liqpay-button form').submit();
                        }
                        else if(data.error){
                            container.prepend('<p>'+data.error+'</p>')
                        }
                    }
                    container.removeClass('loading');
                }
            });
        }
    });

    function validate(form) {
        var valid = true;
        $(form).find('input[type="text"], input[type="tel"], input[type="email"], input[type="number"], input[type="checkbox"]').each(function (index, item) {
            var type = $(item).attr('type');
            var value = $(item).val();
            var attr = $(item).attr('required');
            var validate = $(item).data('action');
            if((typeof attr !== typeof undefined && attr !== false) ||
                (typeof validate !== typeof undefined && validate !== false && (validate == 'validate' && value))
            ){
                if(type == 'text'){
                    if(!value.trim() || (value.length < 2 || value.length > 16)){
                        $(item).css('border-color', 'red');
                        valid = false;
                    }
                    else {
                        $(item).css('border-color', 'lightgreen');
                    }
                }
                else if(type == 'tel'){
                    if(!value.match(/^\+38 \(\d{3}\) \d{3}-\d{2}-\d{2}$/g)){
                        $(item).css('border-color', 'red');
                        valid = false;
                    }
                    else {
                        $(item).css('border-color', 'lightgreen');
                    }
                }
                else if(type == 'email'){
                    if(!validateEmail(value)){
                        $(item).css('border-color', 'red');
                        valid = false;
                    }
                    else {
                        $(item).css('border-color', 'lightgreen');
                    }
                }
                else if(type == 'number'){
                    if(!isNumeric(value) || value < 1){
                        $(item).css('border-color', 'red');
                        valid = false;
                    }
                    else {
                        $(item).css('border-color', 'lightgreen');
                    }
                }
                else if(type == 'checkbox'){
                    if(!$(item).prop('checked')){
                        $(item).next('.alert-container').css('color', 'red');
                        valid = false;
                    }
                    else {
                        $(item).next('.alert-container').css('color', 'lightgreen');
                    }
                }
            }
            else{
                $(item).css('border-color', 'lightgreen');
            }
        });
        return valid;
    }

});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}