/*
* Plugin Name: rplatform Core
* Plugin URI: http://www.rplatform.com/item/core
* Author: rplatform
* Author URI: http://www.rplatform.com
* License - GNU/GPL V2 or Later
* Description: rplatform Core is a required plugin for this theme.
* Version: 1.1
*/

jQuery(document).ready(function($){'use strict';

    // Video Carosuel
    if( jQuery("html").attr("dir") == 'rtl' ){
        var owlrtl = 'true';
    }else {
        var owlrtl = 'false';
    }
    var $viddeocarosuel = $('.rplatform-video-carosuel');
    $viddeocarosuel.owlCarousel({
        loop:true,
        rtl:owlrtl,
        dots:true,
        nav:true,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        margin:30,
        autoplay:false,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        autoHeight: false,
        lazyLoad:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:4
            }
        }
    });

    $('.videoPrev').click(function(){
    $viddeocarosuel.trigger('prev.owl.carousel', [400]);
    });

    $('.videoNext').click(function(){
    $viddeocarosuel.trigger('next.owl.carousel',[400]);
    });


    // Popup 

    if ($(".champ-video").length > 0) {
        $(".champ-video a").magnificPopup({
            disableOn: 0,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 300,
            preloader: false,
            fixedContentPos: false,
        });
    }
    $('.plus-icon').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function (openerElement) {
                return openerElement.next('img') ? openerElement : openerElement.find('img');
            }
        },
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        }

    });


    // Home Slider

    function doAnimations(elems) {
        var animEndEv = 'webkitAnimationEnd animationend';

        elems.each(function () {
            var $this = $(this),
                    $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }
    var $myCarousel = $('.home-two-crousel');
    var $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
    $myCarousel.carousel();
    doAnimations($firstAnimatingElems);
    $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });  
        


    // Start: donation js

    var donation_input = $('.rp-addon-donation .donation-ammount-wrap > input');
    donation_input.on('click', function(){
        // remove previous active class and add class
        donation_input.removeClass('active');
        $(this).addClass('active');

        var currency = $(".rp-addon-donation .donation-ammount-wrap").data('currency'),
        crncy_code = currency.split(':'),
        pid = $(".rp-addon-donation .donation-ammount-wrap").data('pid'),
        this_val = $(this).val(),
        amt = this_val.split('$');

        if (amt[1]) {
            var amt = amt[1];
        } else{
            var amt = this_val;
        };

        if (amt != '' && amt > 0) {
            $(".rp-addon-donation .donation-button .donation-button-link").attr("href", "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business="+pid+"&item_name=donation&amount="+amt+"&currency_code="+crncy_code[0]+"");
        };
        
    });

    //donation custom onkeyup change value
    $('.rp-addon-donation .donation-ammount-wrap > input.input-text').on('keyup', function(event) {
        var this_val = $(this).val(),
        pid = $(".rp-addon-donation .donation-ammount-wrap").data('pid'),
        currency = $(".rp-addon-donation .donation-ammount-wrap").data('currency'),
        crncy_code = currency.split(':');

        if (this_val != '' && this_val > 0) {
            $(".rp-addon-donation .donation-button .donation-button-link").attr("href", "https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business="+pid+"&item_name=donation&amount="+this_val+"&currency_code="+crncy_code[0]+"");
        };
    });


    // Count Down

    var val = $( "#passval" ).data("id");
    var date = $( "#passval" ).data("options");

    $('#clock'+val).countdown(date).on('update.countdown', function (event) {
         var $this = $(this).html(event.strftime( '<div class=\'pull-left single-count\'><span class=\'count-number\'>%D</span><span class=\'count-text\'>day%!d </span></div><div class=\'pull-left single-count\'><span class=\'count-number\'>%H</span><span class=\'count-text\'>Hours</span></div><div class=\'pull-left single-count\'><span class=\'count-number\'>%M</span><span class=\'count-text\'>minutes</span></div><div class=\'pull-left single-count last\'><span class=\'count-number\'>%S</span><span class=\'count-text\'>seconds</span></div>'));
     });


    //  CountDown

    $('.rplatform-shortocde-counter').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
      if (visible) {
       $(this).find('.counter').each(function () {
        var $this = $(this);
        $({ Counter: 0 }).animate({ Counter: $this.text() }, {
         duration: 2000,
         easing: 'swing',
         step: function () {
          $this.text(Math.ceil(this.Counter));
         }
        });
       });
       $(this).unbind('inview');
      }
    });


    // Popup Video

    if ($("#videoPlay, #about-video").length > 0) {
        $("#videoPlay, #about-video").magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 300,
            preloader: false,
            fixedContentPos: false
        });
    }
    $('.plus-icon').magnificPopup({
        type: 'image',
        mainClass: 'mfp-with-zoom',
        zoom: {
            enabled: true,
            duration: 300,
            easing: 'ease-in-out',
            opener: function (openerElement) {
                return openerElement.next('img') ? openerElement : openerElement.find('img');
            }
        },
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        }

    });
    $('.plus-icon').on('click', function () {
        $('html').css('overflow', 'inherit');
    });


    // Product Carousel

    if ($('#product-crousel').length > 0) {
        $('#product-crousel').owlCarousel({
            items: 3,
            autoPlay: 3000,
            rewindNav: false,
            navigation: true,
            pagination: false,
            navText: ['<i class="fa fa-long-arrow-left"></i>', '<i class="fa fa-long-arrow-right"></i>'],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 2,
                    nav: false
                },
                1000: {
                    items: 3,
                    nav: true,
                    loop: false
                }
            }
        });
    }
    
    // Animated Number

    $('.rplatform-counter-number').each(function(){
      var $this = $(this);
      $({ Counter: 0 }).animate({ Counter: $this.data('digit') }, {
        duration: $this.data('duration'),
        step: function () {
          $this.text(Math.ceil(this.Counter));
        }
      });
    });

});