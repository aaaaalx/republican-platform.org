<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Admin functions for the Event post type
 *
 * @author 		rplatform
 * @category 	Admin
 * @package 	rp
 *-------------------------------------------------------------*/

/*--------------------------------------------------------------
*			Register Gallery Post Type
*-------------------------------------------------------------*/

function rplatform_post_type_gallery()
{
	$labels = array(
		'name'                	=> _x( 'Gallery', 'Gallery', 'rplatform-core' ),
		'singular_name'       	=> _x( 'Gallery', 'Gallery', 'rplatform-core' ),
		'menu_name'           	=> __( 'Photo Gallery', 'rplatform-core' ),
		'parent_item_colon'   	=> __( 'Parent Gallery:', 'rplatform-core' ),
		'all_items'           	=> __( 'All Gallery', 'rplatform-core' ),
		'view_item'           	=> __( 'View Gallery', 'rplatform-core' ),
		'add_new_item'        	=> __( 'Add New Gallery', 'rplatform-core' ),
		'add_new'             	=> __( 'New Gallery', 'rplatform-core' ),
		'edit_item'           	=> __( 'Edit Gallery', 'rplatform-core' ),
		'update_item'         	=> __( 'Update Gallery', 'rplatform-core' ),
		'search_items'        	=> __( 'Search Gallery', 'rplatform-core' ),
		'not_found'           	=> __( 'No article found', 'rplatform-core' ),
		'not_found_in_trash'  	=> __( 'No article found in Trash', 'rplatform-core' )
		);

	$args = array(  
		'labels'             	=> $labels,
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_in_menu'       	=> true,
		'show_in_admin_bar'   	=> true,
		'can_export'          	=> true,
		'has_archive'        	=> true,
		'hierarchical'       	=> false,
		'menu_position'      	=> null,
		'menu_icon'				=> 'dashicons-format-image',
		'supports'           	=> array( 'title')
		);

	register_post_type('gallery', $args);

}

add_action('init','rplatform_post_type_gallery');


/**
 * View Message When Updated Project
 *
 * @param array $messages Existing post update messages.
 * @return array
 */

function rplatform_update_message_gallery( $messages )
{
	global $post, $post_ID;

	$message['gallery'] = array(
		0 => '',
		1 => sprintf( __('gallery updated. <a href="%s">View gallery</a>', 'rplatform-core' ), esc_url( get_permalink($post_ID) ) ),
		2 => __('Custom field updated.', 'rplatform-core' ),
		3 => __('Custom field deleted.', 'rplatform-core' ),
		4 => __('gallery updated.', 'rplatform-core' ),
		5 => isset($_GET['revision']) ? sprintf( __('gallery restored to revision from %s', 'rplatform-core' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('gallery published. <a href="%s">View gallery</a>', 'rplatform-core' ), esc_url( get_permalink($post_ID) ) ),
		7 => __('gallery saved.', 'rplatform-core' ),
		8 => sprintf( __('gallery submitted. <a target="_blank" href="%s">Preview gallery</a>', 'rplatform-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('gallery scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview gallery</a>', 'rplatform-core' ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('gallery draft updated. <a target="_blank" href="%s">Preview gallery</a>', 'rplatform-core' ), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		);

return $message;
}

add_filter( 'post_updated_messages', 'rplatform_update_message_gallery' );