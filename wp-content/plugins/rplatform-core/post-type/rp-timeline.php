<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Admin functions for the Event post type
 *
 * @author 		rplatform
 * @category 	Admin
 * @package 	rp
 *-------------------------------------------------------------*/

/*--------------------------------------------------------------
*			Register Gallery Post Type
*-------------------------------------------------------------*/


function rplatform_post_type_time_line()
{
	$labels = array(
		'name'                	=> _x( 'Timeline', 'Timeline', 'rplatform-core' ),
		'singular_name'       	=> _x( 'Timeline', 'Timeline', 'rplatform-core' ),
		'menu_name'           	=> __( 'Timeline', 'timeline' ),
		'parent_item_colon'   	=> __( 'Parent Time Line:', 'rplatform-core' ),
		'all_items'           	=> __( 'All Title', 'rplatform-core' ),
		'view_item'           	=> __( 'View Time Line', 'rplatform-core' ),
		'add_new_item'        	=> __( 'Add New Time Line', 'rplatform-core' ),
		'add_new'               => __( 'Add New', 'rplatform-core' ),
		'edit_item'           	=> __( 'Edit Title', 'rplatform-core' ),
		'update_item'         	=> __( 'Update Time Line', 'rplatform-core' ),
		'search_items'        	=> __( 'Search Time Line', 'rplatform-core' ),
		'not_found'           	=> __( 'No article found', 'rplatform-core' ),
		'not_found_in_trash'  	=> __( 'No article found in Trash', 'rplatform-core' )



		);

	$args = array(  
		'labels'             	=> $labels,
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_in_menu'       	=> true,
		'show_in_admin_bar'   	=> true,
		'can_export'          	=> true,
		'has_archive'        	=> true,
		'hierarchical'       	=> false,
		'menu_position'      	=> null,
		'menu_icon'				=> 'dashicons-welcome-write-blog',
		'supports'           	=> array( 'title'),
		);

	register_post_type('time_line', $args);

}

add_action('init','rplatform_post_type_time_line');