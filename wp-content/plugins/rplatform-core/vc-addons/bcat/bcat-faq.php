<?php

add_shortcode( 'bcat_faq', function($atts, $content = null) {
    $faq_list = vc_param_group_parse_atts($atts['faq_list']);
    if($faq_list){
        ob_start();
        include('views/faq.php');
        $view = ob_get_clean();
        return $view;
    }
});

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map([
        "name" => esc_html__("BCat Faq", 'rplatform-core'),
        "base" => "bcat_faq",
        'icon' => 'icon-thm-person',
        "class" => "",
        "description" => esc_html__("Widget Faq", 'rplatform-core'),
        "category" => esc_html__('rp', 'rplatform-core'),
        "params" => [
            [
                'type' => 'param_group',
                'value' => '',
                'heading' =>  esc_html__("Faq list", 'rplatform-core'),
                'param_name' => 'faq_list',
                // Note params is mapped inside param-group:
                'params' => [
                    [
                        'type' => 'textfield',
                        'heading' => esc_html__("Faq question", 'rplatform-core'),
                        'param_name' => 'faq_question',
                        'admin_label' => true,
                        'value' => esc_html__("Type your question here", 'rplatform-core')
                    ],
                    [
                        'type' => 'textarea',
                        'heading' => esc_html__("Faq answer", 'rplatform-core'),
                        'param_name' => 'faq_answer',
                        'value' => esc_html__("Type your answer here", 'rplatform-core')
                    ]
                ]
            ],
        ]
    ]);
}