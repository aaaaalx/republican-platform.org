<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php foreach($faq_list as $num => $item): ?>
    <div class="panel">
        <div class="panel-heading" role="tab" id="heading_<?= $num ?>">
            <h4 class="panel-title">
                <a class="collapse-button<?= $num>0?' collapsed':''?>" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $num ?>" aria-expanded="true" aria-controls="collapse_<?= $num ?>">
                    <?= $item['faq_question'] ?>
                </a>
            </h4>
        </div>
        <div id="collapse_<?= $num ?>" class="panel-collapse collapse<?= $num<1?' in':''?>" role="tabpanel" aria-labelledby="heading_<?= $num ?>">
            <div class="panel-body">
                <?= $item['faq_answer'] ?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
