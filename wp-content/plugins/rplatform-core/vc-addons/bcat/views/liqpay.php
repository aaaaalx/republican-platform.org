<div class="paymentform-wrap">
    <form action="" method="post" class="paymentform" data-url="<?php echo site_url() ?>/wp-admin/admin-ajax.php">
        <p>
            <label><?= __('Last Name', 'rp') ?>*<br>
                <input type="text" name="lastname" required>
            </label>
        </p>
        <p>
            <label><?= __('First Name', 'rp') ?>*<br>
                <input type="text" name="firstname" required>
            </label>
        </p>
        <p>
            <label><?= __('Surname', 'rp') ?>*<br>
                <input type="text" name="surname" required>
            </label>
        </p>
        <p>
            <label><?= __('Phone number', 'rp') ?>*<br>
                <input type="tel" name="phone" required>
            </label>
        </p>
        <p>
            <label>E-mail<br>
                <input type="email" name="email" data-action="validate">
            </label>
        </p>
        <p>
            <label><?= __('Amount, UAH', 'rp') ?>*<br>
                <input type="number" min="1" name="amount" value="100" class="" required>
            </label>
        </p>
        <p>
            <label>
                <input type="checkbox" value="1" name="submitted" required style="width: unset; margin-right: 5px;">
                <span class="alert-container"><?= __('I would like to inform that the circumstances stipulated in the first and third paragraphs of Article 15 of the Law of Ukraine "On Political Parties in Ukraine", the existence of which are grounds for refusing to accept a payment document for making a financial contribution to support the Party; consent to the collection, processing and use of personal data.', 'rp') ?>*</span>
            </label>
        </p>
        <p>
            * - <?= __('fields are required', 'rp') ?>!
        </p>
        <p>
            <input type="submit" value="<?= __('Send', 'rp') ?>" class="wpcf7-submit">
        </p>
        <input type="hidden" value="<?= currentLang() ?>" name="lang">
    </form>
    <div style="display: none" class="liqpay-button"></div>
</div>