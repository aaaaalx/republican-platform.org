<?php

add_action( 'admin_post_nopriv_process_incall', 'process_incall' );
add_action( 'admin_post_process_incall', 'process_incall' );
function process_incall() {

}

add_action('wp_ajax_payment_userdata', 'payment_userdata');
add_action('wp_ajax_nopriv_payment_userdata', 'payment_userdata');
function payment_userdata(){
    if(!empty($_POST['data'])) {
        $post = array_column($_POST['data'], 'value', 'name');
        if (!empty($post['firstname']) && !empty($post['lastname']) && !empty($post['surname']) && !empty($post['phone']) && !empty($post['amount']) && (!empty($post['submitted']) && $post['submitted'] == 1)) {
            $firstname = sanitize_text_field(trim($post['firstname']));
            $lastname = sanitize_text_field(trim($post['lastname']));
            $surname = sanitize_text_field(trim($post['surname']));
            $phone = sanitize_text_field(trim($post['phone']));
            $email = !empty($post['email'])?sanitize_text_field(trim($post['email'])):'';
            $amount = number_format((float)sanitize_text_field(trim($post['amount'])), 2, '.', '');
            if(
                !validateText($firstname) ||
                !validateText($lastname) ||
                !validateText($surname) ||
                !validatePhone($phone) ||
                !validateNumber($amount) ||
                ($email && !validateEmail($email))){
                wp_send_json(['error' => __('Wrong passed data', 'rp').'!']);
                die;
            }
            $email = $email?"E-mail: $email ":"";
            $lang = !empty($post['lang'])?sanitize_text_field(trim($post['lang'])):'';
            $lang = ($lang == 'en' || $lang == 'uk')?$lang:'uk';
            $public_key = trim(rp_options('payment_liqpay_public_key'));
            $private_key = trim(rp_options('payment_liqpay_private_key'));
            $result_url = apply_filters('wpml_home_url', get_option( 'home' ))?:get_site_url();
            if ($public_key && $private_key) {
                $liqpay = new LiqPay($public_key, $private_key);
                $html = $liqpay->cnb_form(array(
                    'result_url' => $result_url,
                    'language' => $lang,
                    'action' => 'paydonate',
                    'amount' => $amount,
                    'currency' => 'UAH',
                    'description' => __('Voluntary donations to the statutory activities of the "Republican Platform party" from: ', 'rp')."$lastname $firstname $surname ( ".__('Phone number', 'rp').": $phone $email)",
                    'version' => '3'
                ));
                wp_send_json(['form' => '<div style="display: none">'.$html.'</div>']);
                die;
            }
        }
        else{
            wp_send_json(['error' => __('Wrong passed data', 'rp').'!']);
            die;
        }
    }
}

function validateText($text){
    $length = mb_strlen($text);
    if($length > 2 && $length < 16 && preg_match('/^[\p{L}\s]+$/u', $text)){
        return true;
    }
    return false;
}
function validateNumber($number){
    if(is_numeric($number) && $number >= 1){
        return true;
    }
    return false;
}
function validateEmail($email){
    if(filter_var($email, FILTER_VALIDATE_EMAIL)){
        return true;
    }
    return false;
}
function validatePhone($phone){
    if(preg_match('/^\+38\s\(\d{3}\)\s\d{3}\-\d{2}\-\d{2}$/', $phone)){
        return true;
    }
    return false;
}

add_shortcode( 'payment_liqpay', function($atts, $content = null) {
    ob_start();
    include('views/liqpay.php');
    $view = ob_get_clean();
    return $view;
});

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
    vc_map([
        "name" => esc_html__("Payment Liqpay", 'rplatform-core'),
        "base" => "payment_liqpay",
        'icon' => 'icon-thm-person',
        "class" => "",
        "description" => esc_html__("Widget Payment Liqpay", 'rplatform-core'),
        "category" => esc_html__('rp', 'rplatform-core')
    ]);
}

class LiqPay
{
    const CURRENCY_EUR = 'EUR';
    const CURRENCY_USD = 'USD';
    const CURRENCY_UAH = 'UAH';
    const CURRENCY_RUB = 'RUB';
    const CURRENCY_RUR = 'RUR';

    private $_api_url = 'https://www.liqpay.ua/api/';
    private $_checkout_url = 'https://www.liqpay.ua/api/3/checkout';
    protected $_supportedCurrencies = array(
        self::CURRENCY_EUR,
        self::CURRENCY_USD,
        self::CURRENCY_UAH,
        self::CURRENCY_RUB,
        self::CURRENCY_RUR,
    );
    private $_public_key;
    private $_private_key;
    private $_server_response_code = null;

    /**
     * Constructor.
     *
     * @param string $public_key
     * @param string $private_key
     *
     * @throws InvalidArgumentException
     */
    public function __construct($public_key, $private_key)
    {
        if (empty($public_key)) {
            throw new InvalidArgumentException('public_key is empty');
        }

        if (empty($private_key)) {
            throw new InvalidArgumentException('private_key is empty');
        }

        $this->_public_key = $public_key;
        $this->_private_key = $private_key;
    }

    /**
     * Call API
     *
     * @param string $path
     * @param array $params
     * @param int $timeout
     *
     * @return stdClass
     */
    public function api($path, $params = array(), $timeout = 5)
    {
        if (!isset($params['version'])) {
            throw new InvalidArgumentException('version is null');
        }
        $url         = $this->_api_url . $path;
        $public_key  = $this->_public_key;
        $private_key = $this->_private_key;
        $data        = $this->encode_params(array_merge(compact('public_key'), $params));
        $signature   = $this->str_to_sign($private_key.$data.$private_key);
        $postfields  = http_build_query(array(
            'data'  => $data,
            'signature' => $signature
        ));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true); // Avoid MITM vulnerability http://phpsecurity.readthedocs.io/en/latest/Input-Validation.html#validation-of-input-sources
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);    // Check the existence of a common name and also verify that it matches the hostname provided
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,$timeout);   // The number of seconds to wait while trying to connect
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);          // The maximum number of seconds to allow cURL functions to execute
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        $this->_server_response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return json_decode($server_output);
    }

    /**
     * Return last api response http code
     *
     * @return string|null
     */
    public function get_response_code()
    {
        return $this->_server_response_code;
    }

    /**
     * cnb_form
     *
     * @param array $params
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function cnb_form($params)
    {
        $language = 'ru';
        if (isset($params['language']) && $params['language'] == 'en') {
            $language = 'en';
        }

        $params    = $this->cnb_params($params);
        $data      = $this->encode_params($params);
        $signature = $this->cnb_signature($params);

        return sprintf('
            <form method="POST" action="%s" accept-charset="utf-8">
                %s
                %s
                <input type="image" src="//static.liqpay.ua/buttons/p1%s.radius.png" name="btn_text" />
            </form>
            ',
            $this->_checkout_url,
            sprintf('<input type="hidden" name="%s" value="%s" />', 'data', $data),
            sprintf('<input type="hidden" name="%s" value="%s" />', 'signature', $signature),
            $language
        );
    }

    /**
     * cnb_signature
     *
     * @param array $params
     *
     * @return string
     */
    public function cnb_signature($params)
    {
        $params      = $this->cnb_params($params);
        $private_key = $this->_private_key;

        $json      = $this->encode_params($params);
        $signature = $this->str_to_sign($private_key . $json . $private_key);

        return $signature;
    }

    /**
     * cnb_params
     *
     * @param array $params
     *
     * @return array $params
     */
    private function cnb_params($params)
    {
        $params['public_key'] = $this->_public_key;

        if (!isset($params['version'])) {
            throw new InvalidArgumentException('version is null');
        }
        if (!isset($params['amount'])) {
            throw new InvalidArgumentException('amount is null');
        }
        if (!isset($params['currency'])) {
            throw new InvalidArgumentException('currency is null');
        }
        if (!in_array($params['currency'], $this->_supportedCurrencies)) {
            throw new InvalidArgumentException('currency is not supported');
        }
        if ($params['currency'] == self::CURRENCY_RUR) {
            $params['currency'] = self::CURRENCY_RUB;
        }
        if (!isset($params['description'])) {
            throw new InvalidArgumentException('description is null');
        }

        return $params;
    }

    /**
     * encode_params
     *
     * @param array $params
     * @return string
     */
    private function encode_params($params)
    {
        return base64_encode(json_encode($params));
    }

    /**
     * decode_params
     *
     * @param string $params
     * @return array
     */
    public function decode_params($params)
    {
        return json_decode(base64_decode($params), true);
    }

    /**
     * str_to_sign
     *
     * @param string $str
     *
     * @return string
     */
    public function str_to_sign($str)
    {
        $signature = base64_encode(sha1($str, 1));

        return $signature;
    }
}