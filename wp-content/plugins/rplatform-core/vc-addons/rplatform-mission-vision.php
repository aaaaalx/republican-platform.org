<?php
add_shortcode( 'rplatform_mission_vision', 'rplatform_mission_vision_function');

function rplatform_mission_vision_function($atts, $content = null) {
	
	$heading 				= '';
	$icon_img 				= '';
	$person_img 			= '';	
	$title 					= '';
	$title_part1_color 		= '#ed1c24';
	$title_part2 			= '';
	$title_part2_color 		= '#06396a';
	$Description 			= '';
	$btn_text 				= '';
	$btn_url 				= '';
	$class  				= '';

	extract(shortcode_atts(array(
		'heading' 				=> '',
		'icon_img' 				=> '',
		'person_img' 			=> '',		
		'title' 				=> '',
		'title_part1_color' 	=> '#ed1c24',
		'title_part2' 			=> '',
		'title_part2_color' 	=> '#06396a',
		'description'   		=> '',
		'btn_text'   			=> '',
		'btn_url'   			=> '',
		'class' 				=> '',
		), $atts));

	$output = $inline1 = $inline2 = '';

	$src_image   = wp_get_attachment_image_src($icon_img, 'full');

	$person_image   = wp_get_attachment_image_src($person_img, 'full');	

		if($title_part1_color) $inline1 .= 'color:' . esc_attr( $title_part1_color )  . ';';

		if($title_part2_color) $inline2 .= 'color:' . esc_attr( $title_part2_color )  . ';';

        $output .= '<div class="mission-area">';
                $output .= '<div class="row">';
                    $output .= '<div class="col-md-6 col-xs-12 no-padding-right">';
                        $output .= '<div class="mission-content">';
	                        if ($heading) {
	                        	$output .= '<span class="heading">'.$heading.'</span>';
	                        }
                            if ($title) {
	                            $output .= '<h3 class="section-title" style="'.$inline1.'">'.$title.' <span style="'.$inline2.'">'.$title_part2.'</span></h3>';
	                        }
	                        if ($description) {
	                            $output .= '<p>'.$description.'</p>';
	                        }
                            $output .= '<div class="singnature">';
                            	if ($btn_url) {
	                                $output .= '<a href="'.$btn_url.'">'.$btn_text.'<i class="fa fa-long-arrow-right"></i></a>';
	                            }
	                            if ($src_image) {
	                            	$output .= '<img src="'.$src_image[0].'" alt="'.$title.'">';
	                            }
                            $output .= '</div>';
                        $output .= '</div>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= '</div>';
            $output .= '<div class="change-person">';
	            if ($person_image) {
	                $output .= '<img src="'.$person_image[0].'" alt="'.$title.'">';
	            }
        $output .= '</div>';

	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Mission Vision", 'rplatform-core'),
		"base" => "rplatform_mission_vision",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Mission Vision", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Heading", 'rplatform-core'),
				"param_name" => "heading",
				"value" => "",
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Title Part 1 ", 'rplatform-core'),
				"param_name" => "title",
				"value" => "",
				),

			array(
				"type" => "colorpicker",
				"heading" => __("Title Part 1 Color", 'rplatform-core'),
				"param_name" => "title_part1_color",
				"value" => "",
			),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Title Part 2", 'rplatform-core'),
				"param_name" => "title_part2",
				"value" => "",
				),			

			array(
				"type" => "colorpicker",
				"heading" => __("Title Part 2 Color", 'rplatform-core'),
				"param_name" => "title_part2_color",
				"value" => "",
			),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Description", 'rplatform-core'),
				"param_name" => "description",
				"value" => "",
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Button name", 'rplatform-core'),
				"param_name" => "btn_text",
				"value" => "",
				),			

			array(
				"type" => "textfield",
				"heading" => esc_html__("Button URL", 'rplatform-core'),
				"param_name" => "btn_url",
				"value" => "",
				),

			array(
				"type" => "attach_image",
				"heading" => __("Upload Signeture:", "rplatform"),
				"param_name" => "icon_img",
				"value" => "",
			),

			array(
				"type" => "attach_image",
				"heading" => __("Change Person Image:", "rplatform"),
				"param_name" => "person_img",
				"value" => "",
			),			

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => "",
			),

			)

		));
}