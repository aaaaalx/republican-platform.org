<?php
add_shortcode( 'rplatform_woocommerce_products', 'rplatform_woocommerce_products_function');

function rplatform_woocommerce_products_function($atts, $content = null) {
	
	$items 		= '';
	$iorder 	= '';
	$iorderby	= '';
	$class  	= '';

	extract(shortcode_atts(array(
		'items' 	=> '',
		'iorder'	=> '',
		'iorderby'	=> '',
		'class' 	=> '',
	), $atts));

 	global $post;
	global $wpdb;
	$output = '';


if($items<6){
	$items = 6;
}

$args = array('post_type' => 'product', 'post_status' => 'publish', 'orderby' => $iorderby, 'order' => $iorder, 'posts_per_page' => $items);

$query = new WP_Query($args);

$output .= '<div class="row self">';
$output .= '<div id="product-crousel" class="owl-carousel owl-theme">';

if($query->have_posts()) :
	while($query->have_posts()): $query->the_post();
		$product = new WC_Product(get_the_ID());

		$output .= '<div class="col-md-4 col-xs-12 text-center">
					<div class="product-details">
					<div class="product-img">';
		
		$output .= '<a href="'.get_permalink().'" class="wpb_pro_img_url">';
		if (has_post_thumbnail( $query->post->ID )){
			$output .= get_the_post_thumbnail($query->post->ID, 'shop_catalog', array('class' => "wpb_pro_img"));
		}else{
			$output .= '<img id="place_holder_thm" src="'.woocommerce_placeholder_img_src().'" alt="Placeholder" />';
		}
		$output .='</a>';

		#$output .= '<a href="?add-to-cart='.$query->post->ID.'" class="filled-button">Add to Cart</a>';
		$output .='<div class="addtocart-btn">';
		$output .='<a rel="nofollow" href="?add-to-cart='.$query->post->ID.'" data-quantity="1" data-product_id="'.$query->post->ID.'" data-product_sku="" class="filled-button button product_type_simple add_to_cart_button ajax_add_to_cart">Add to cart</a>';	
		$output .= '</div>';

		$output .= '</div>';

		$output .= '<h4><a href="'.get_permalink().'" class="wpb_pro_img_url">'.get_the_title().'</a></h4>';

		if( $price_html = $product->get_price_html() ){
			$output .='<div class="pro_price_area">'. $price_html .'</div>';
		}

		$output .= '</div></div>';
	endwhile;
endif;

$output .= '</div>';
$output .= '</div>';

return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Woocommerce Products Slider", 'rplatform-core'),
		"base" => "rplatform_woocommerce_products",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Woocommerce Products Slider", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Items to Slide", 'rplatform-core'),
				"param_name" => "items",
				"value" => "",
				),

			// Item Order Dropdown
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Item Order Dropdown","rplatform"),
				"param_name" => "iorder",
				"value" => array(
			 		__("No Ordering","rplatform") => "",
					__("Ascending Order","rplatform") => "ASC",
					__("Descending Order","rplatform") => "DESC",
					),
			  ),

			// Item Order By Dropdown
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Item Order By Dropdown","rplatform"),
				"param_name" => "iorderby",
				"value" => array(
			 		__("No Order By","rplatform") => "none",
					__("Order By ID","rplatform") => "ID",
					__("Order By Author","rplatform") => "author",
					__("Order By Title","rplatform") => "title",
					__("Order By Name","rplatform") => "name",
					__("Order By Type","rplatform") => "type",
					__("Order By Date","rplatform") => "date",
					__("Order By Last Modified Date","rplatform") => "modified",
					__("Order By Random","rplatform") => "rand",
					__("Order By number of Comments","rplatform") => "comment_count",
					),
			  ),		  	

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}