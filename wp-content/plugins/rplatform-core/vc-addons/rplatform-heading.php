<?php
add_shortcode( 'rplatform_heading', 'rplatform_heading_function');


function rplatform_heading_function($atts, $content = null) {

	$title_head			= '';
	$top_title_heading	= 'h2';
	$top_title_size 	= '';
	$title_head_color 	= '';
	$title 				='';
	$main_title_heading	='h3';
	$sub_title_heading	='h4';
	$top_title_weight	='700';
	$main_title_weight	='700';
	$subtitle			='';
	$color				='#ed1c24';
	$color2				='#06396a';
	$size				='45';
	$top_title_margin	= '0px 0px 10px 0px';
	$main_title_margin	= '0px 0px 10px 0px';
	$sub_title_margin	= '0px 0px 10px 0px';
	$title_weight		= '700';
	$class				= '';
	$title_alignment	= 'center';
	$text_transform 	= 'capitalize';
	$subtitle2			= '';
	$subsize			= '16px';
	$subcolor			= '#aeaeae';
	$submargin  		= '0px 0px 10px 0px';


	extract(shortcode_atts(array(

		'title_alignment'			=> 'center',
		'title_head'				=> '',
		'top_title_heading'			=> 'h2',
		'top_title_size'			=> '',
		'top_title_weight' 			=> '700',
		'top_title_margin' 			=> '0px 0px 10px 0px',
		'title_head_color'			=> '#000000',

		'title' 					=> '',
		'subtitle'					=> '',
		'main_title_heading' 		=> 'h3',
		'main_title_weight' 		=> '700',
		'main_title_margin' 		=> '0px 0px 10px 0px',
		'color'						=> '#ed1c24',
		'color2'					=> '#06396a',
		'size'						=> '45',
		
		'sub_title_margin' 			=> '0px 0px 10px 0px',
		'subtitle2' 				=> '',
		'subsize' 					=> '16px',
		'subcolor' 					=> '#aeaeae',
		'sub_title_heading' 		=> 'h4',
		'title_weight'				=> '700',
		
		
		'class'						=> '',
		), $atts));


	$inline1 = $inline2 = $inline3 = $output = $inline_css_span = '';


	if($top_title_weight) $inline1 .= 'font-weight:' . (int) esc_attr( $top_title_weight ) . ';';
	if($top_title_size) $inline1 .= 'font-size:' . (int) esc_attr( $top_title_size ) . 'px;line-height: normal;display:block;';
	if($title_head_color) $inline1 .= 'color:' . esc_attr( $title_head_color )  . ';';
	if($top_title_margin) $inline1 .= 'margin:' . esc_attr( $top_title_margin )  . ';';	

	if($main_title_weight) $inline2 .= 'font-weight:' . (int) esc_attr( $main_title_weight ) . ';';
	if($size) $inline2 .= 'font-size:' . (int) esc_attr( $size ) . 'px;line-height: normal;display:block;';
	if($main_title_margin) $inline2 .= 'margin:' . esc_attr( $main_title_margin )  . ';';
	if($color) $inline2 .= 'color:' . esc_attr( $color )  . ';';

	if($color2) $inline_css_span .= 'color:' . esc_attr( $color2 )  . ';';

	if($title_weight) $inline3 .= 'font-weight:' . (int) esc_attr( $title_weight ) . ';';
	if($subsize) $inline3 .= 'font-size:' . (int) esc_attr( $subsize ) . 'px;line-height: normal;display:block;';
	if($sub_title_margin) $inline3 .= 'margin:' . esc_attr( $sub_title_margin )  . ';';
	if($subcolor) $inline3 .= 'color:' . esc_attr( $subcolor )  . ';';

	$output .= '<div class="rplatform-title '.esc_attr($class).'" style="text-align:'. $title_alignment .'">';
		
		if ($title_head) {
			if ( $top_title_heading ) {
				$output .= '<'.$top_title_heading.' class="style-title" style="'.$inline1.'">' . esc_attr( $title_head ) . '</'.$top_title_heading.'>';
			} else {
				$output .= '<h2 class="style-title" style="'.$inline1.'">' . esc_attr( $title_head ) . '</h2>';
			}
			
		}

		if ( $title || $subtitle ) {
			if ( $main_title_heading ) {
			$output .= '<'.$main_title_heading.' style="'.$inline2.'">'.esc_attr($title).' <span style="'.$inline_css_span.'">'.esc_attr($subtitle).'</span></'.$main_title_heading.'>'; 
			} else {
				$output .= '<h3 style="'.$inline2.'">'.esc_attr($title).' <span style="'.$inline_css_span.'">'.$subtitle.'</span></h3>'; 
			}
		}

		if($subtitle2){
			if ( $main_title_heading ) {
				$output .= '<'.$sub_title_heading.' class="style-title" style="'.$inline3.'">' . balanceTags( $subtitle2 ) . '</'.$sub_title_heading.'>';
			} else {
				$output .= '<h4 class="style-title" style="'.$inline3.'">' .balanceTags($subtitle2). '</h4>';
			}
		}

	$output .= '</div>';

	return $output;
}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("rplatform Title", 'rplatform-core'),
	"base" => "rplatform_heading",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Widget Title Heading", 'rplatform-core'),
	"category" => __('rp', 'rplatform-core'),
	"params" => array(		

		array(
			"type" => "dropdown",
			"heading" => __("Title Alignment", 'rplatform-core'),
			"param_name" => "title_alignment",
			"value" => array('Select'=>'','justify'=>'justify','left'=>'left','right'=>'right','center'=>'center'),
			),

		array(
			"type" => "textfield",
			"heading" => __("Top Title", 'rplatform-core'),
			"param_name" => "title_head",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Top Title Heading", 'rplatform-core'),
			"param_name" => "top_title_heading",
			"value" => array('Select'=>'','h1'=>'h1','h2'=>'h2','h3'=>'h3','h4'=>'h4','p'=>'p','span'=>'span'),
		),	

		array(
			"type" => "textfield",
			"heading" => __("Top Title Font Size", 'rplatform-core'),
			"param_name" => "top_title_size",
			"value" => "45",
		),

		array(
			"type" => "colorpicker",
			"heading" => __("Top Title Color", 'rplatform-core'),
			"param_name" => "title_head_color",
			"value" => "#000000",
		),

		array(
			"type" => "dropdown",
			"heading" => __("Top Title Font Wight", 'rplatform-core'),
			"param_name" => "top_title_weight",
			"value" => array('Select'=>'','400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" => "textfield",
			"heading" => __("Top Title Margin", 'rplatform-core'),
			"param_name" => "top_title_margin",
			"value" => "0px 0px 0px 0px",
		),

		array(
			"type" => "textfield",
			"heading" => __("Main Title Part 1", 'rplatform-core'),
			"param_name" => "title",
			"value" => "",
		),

		array(
			"type" => "colorpicker",
			"heading" => __("Main Title Part 1 Color", 'rplatform-core'),
			"param_name" => "color",
			"value" => "#ed1c24",
		),

		array(
			"type" => "textfield",
			"heading" => __("Main Title Part 2", 'rplatform-core'),
			"param_name" => "subtitle",
			"value" => "",
			),

		array(
			"type" => "colorpicker",
			"heading" => __("Main Title Part 2 Color", 'rplatform-core'),
			"param_name" => "color2",
			"value" => "#06396a",
		),
		array(
			"type" => "textfield",
			"heading" => __("Main Title Font Size", 'rplatform-core'),
			"param_name" => "size",
			"value" => "45",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Main Title Heading", 'rplatform-core'),
			"param_name" => "main_title_heading",
			"value" => array('Select'=>'','h1'=>'h1','h2'=>'h2','h3'=>'h3','h4'=>'h4','p'=>'p','span'=>'span'),
		),	

		array(
			"type" => "dropdown",
			"heading" => __("Main Title Font Wight", 'rplatform-core'),
			"param_name" => "main_title_weight",
			"value" => array('Select'=>'','400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" => "textfield",
			"heading" => __("Main Title Margin", 'rplatform-core'),
			"param_name" => "main_title_margin",
			"value" => "0px 0px 0px 0px",
		),

		array(
			"type" => "textarea",
			"heading" => __("Sub Title", 'rplatform-core'),
			"param_name" => "subtitle2",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Sub Title Heading", 'rplatform-core'),
			"param_name" => "sub_title_heading",
			"value" => array('Select'=>'','h1'=>'h1','h2'=>'h2','h3'=>'h3','h4'=>'h4','p'=>'p','span'=>'span'),
		),	

		array(
			"type" => "colorpicker",
			"heading" => __("Sub Title Color", 'rplatform-core'),
			"param_name" => "subcolor",
			"value" => "#06396a",
		),

		array(
			"type" => "textfield",
			"heading" => __("Sub Title Font Size", 'rplatform-core'),
			"param_name" => "subsize",
			"value" => "45",
		),

		array(
			"type" => "dropdown",
			"heading" => __("Title Font Wight", 'rplatform-core'),
			"param_name" => "title_weight",
			"value" => array('Select'=>'','400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" => "textfield",
			"heading" => __("Sub Title Margin", 'rplatform-core'),
			"param_name" => "sub_title_margin",
			"value" => "0px 0px 0px 0px",
		),

		array(
			"type" => "textfield",
			"heading" => __("Custom Class ", 'rplatform-core'),
			"param_name" => "class",
			"value" => "",
			),
		)
	));
}