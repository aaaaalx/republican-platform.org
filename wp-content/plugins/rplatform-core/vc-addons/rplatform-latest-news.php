<?php
add_shortcode( 'rplatform_latest_news', 'rplatform_latest_news_function');

function rplatform_latest_news_function($atts, $content = null) {

    $title 	= '';
	$category 	= 'rplatformall';
	$number 	= '4';
	$class  	= '';
	$order_by	= 'date';
	$order		= 'DESC';
    $style		= 'style_1';

	extract(shortcode_atts(array(
        'title' 		=> '',
		'category' 		=> 'rplatformall',
		'number' 		=> '4',
		'class' 		=> '',
		'order_by'		=> 'date',
		'order'			=> 'DESC',
        'style'			=> 'style_1',
		), $atts));

 	global $post;
	global $wpdb;
	$output = '';

    $customStyle = $style.' ';
	
	// Basic Query
    $args = array(
                'post_status'		=> 'publish',
				'posts_per_page'	=> $number,
				'order'				=> $order,
				'orderby'			=> $order_by
			);

    // Category Add
	if( ( $category != '' ) && ( $category != 'rplatformall' ) ){
		$args2 = array( 'tax_query' => array(
											array(
												'taxonomy' => 'category',
												'field'    => 'slug',
												'terms'    => $category,
											),
										),
						);
		$args = array_merge( $args,$args2 );
	}
	$data = new WP_Query($args);
			
        $output .= '<div class="latest-news'.($class?' '.$class:'').'">';
                $output .= '<div class="row">';
                    $i=1;
                    // The Loop
					if ( $data->have_posts() ) {

                        $categoryData = get_category_by_slug($category);
                        $categoryLink = $categoryData?get_category_link($categoryData->term_id):'';

						while ( $data->have_posts() ) {
							$data->the_post();

                            $leftPart = '<div class="'.$customStyle.'item col-md-6 col-xs-12">';
                            $leftPart .= '<div class="news-wrap">';
                            $leftPart .= '<div class="news-img">';
                            $leftPart .= '<div class="news-date">';
                            $leftPart .= '<p>'. get_the_date( get_option('date_format')) .'</p>';
                            $leftPart .= '</div>';
                            $leftPart .= '<a href="'.get_the_permalink().'">'.get_the_post_thumbnail(get_the_ID(),'rp-medium', array('class' => 'img-responsive')).'</a>';
                            $leftPart .= '</div>';
                            $leftPart .= '<h3><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h3>';
                            $leftPart .= '<div class="short-desc">'. rp_excerpt_max_char(100). '</div>';
                            $leftPart .= '<a href="'.get_the_permalink().'" class="readmore">'.__('Read More', 'rp').'</a>';
                            $leftPart .= '</div>';
                            $leftPart .= '</div>';

                            $rightPart = '<div class="thumbnail-news">';
                            $rightPart .= '<div class="news-img pull-left">';
                            $rightPart .= '<div class="news-date">';
                            $rightPart .= '<p>'. get_the_date( get_option('date_format')) .'</p>';
                            $rightPart .= '</div>';
                            $rightPart .= '<a href="'.get_the_permalink().'">'.get_the_post_thumbnail(get_the_ID(),'rp-medium', array('class' => 'img-responsive')).'</a>';
                            $rightPart .= '</div>';
                            $rightPart .= '<div class="small-news pull-left">';
                            $rightPart .= '<h4><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h4>';
                            $rightPart .= '<div class="short-desc">'.rp_excerpt_max_char(100).'</div>';
                            $rightPart .= '<a href="'.get_the_permalink().'" class="readmore">'.__('Read More', 'rp').'</a>';
                            $rightPart .= '</div>';
                            $rightPart .= '<div class="clearfix"></div>';
                            $rightPart .= '</div>';

                            if($style == 'style_1'){
                                if($i==1){
                                    $output .= $leftPart;
                                }
                                elseif($i>1){
                                    if($i==2){
                                        $output .= '<div class="'.$customStyle.'col-md-6 col-xs-12">';
                                    }
                                    $output .= $rightPart;
                                    if($i==$data->post_count){
                                        $output .= '</div>';
                                        $output .= '<div class="'.$customStyle.'showAll-warp toTextCenter col-md-6 col-xs-12">';
                                        $output .= '<div class="showAll"><a href="'.$categoryLink.'">'.__('Read all news', 'rp').'</a></div>';
                                        $output .= '</div>';
                                    }
                                }
                            }
                            else{
                                if($i==$data->post_count){
                                    $output .= $leftPart;
                                    $output .= '<div class="'.$customStyle.'showAll-warp toTextCenter col-md-6 col-xs-12">';
                                    $output .= '<div class="showAll"><a href="'.$categoryLink.'">'.__('Read all announcement', 'rp').'</a></div>';
                                    $output .= '</div>';
                                }
                                elseif($i>=1){
                                    if($i==1){
                                        $output .= '<div class="'.$customStyle.'col-md-6 col-xs-12">';
                                    }
                                    $output .= $rightPart;
                                    if($i==$data->post_count-1){
                                        $output .= '</div>';
                                    }
                                }
                            }

                            $i++;

                		}

                	}

					wp_reset_postdata();
               $output .= '</div>';
        $output .= '</div>';



	return $output;
}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Latest News", 'rplatform-core'),
		"base" => "rplatform_latest_news",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Latest News", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(

            array(
                "type" => "textfield",
                "heading" => esc_html__("Title", 'rplatform-core'),
                "param_name" => "title",
                "value" => "",
            ),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Category Filter", 'rplatform-core'),
				"param_name" => "category",
				"value" => rplatform_cat_list('category'),
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Number of items", 'rplatform-core'),
				"param_name" => "number",
				"value" => "5",
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("OderBy", 'rplatform-core'),
				"param_name" => "order_by",
				"value" => array('Select'=>'','Date'=>'date','Title'=>'title','Modified'=>'modified','Author'=>'author','Random'=>'rand'),
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Order", 'rplatform-core'),
				"param_name" => "order",
				"value" => array('Select'=>'','DESC'=>'DESC','ASC'=>'ASC'),
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => "",
				),

            array(
                "type" => "dropdown",
                "heading" => esc_html__("Style", 'rplatform-core'),
                "param_name" => "style",
                "value" => array('Select'=>'','Style 1'=>'style_1','Style 2'=>'style_2'),
                ),

			)

		));
}