<?php
add_shortcode( 'rplatform_call_to_signup', 'rplatform_call_to_signup_function');


function rplatform_call_to_signup_function($atts, $content = null) {

	$title 			='';
	$color			='#ed1c24';
	$size			='45';
	$title_margin	='';
	$title_padding	='0px 0px 30px 0px';
	$title_weight	='700';
	$class			='';
	$title_alignment='center';
	$text_transform = 'capitalize';
	$subtitle2	= '';
	$subsize	= '16px';
	$subcolor	= '#aeaeae';
	$submargin  = '0px 0px 10px 0px';
	$icon_img = '';
	$button_text = '';
	$button_url = '';
	$bg_color       = '#04396b';

	extract(shortcode_atts(array(
		'subtitle2' 	=> '',
		'subsize' 		=> '16px',
		'subcolor' 		=> '#aeaeae',
		'submargin' 	=> '0px 0px 10px 0px',
		'title' 		=> '',
		'title_alignment'=> 'center',
		'color'			=> '#ed1c24',
		'size'			=> '45',
		'title_margin'	=> '0px 0px 30px 0px',
		'title_padding'	=> '',
		'title_weight'	=> '700',
		'class'			=> '',
		'text_transform'=> 'capitalize',
		'icon_img'		=> '',
		'button_text'	=> '',
		'button_url'	=> '',
		'bg_color'      => '#04396b',
		), $atts));

	
	
	$b_color = 'style="background:'. esc_attr( $bg_color ) .'"';

	$inline_css = $output = $inline_css_span = $style = '';

	if($color){ $inline_css .= 'color:'.esc_attr($color).';'; }
	if($size){ $inline_css .= 'font-size:'.esc_attr($size).'px;'; }
	if($title_margin){  $inline_css .= 'margin:'.esc_attr($title_margin).';';  }
	if($title_padding){  $inline_css .= 'padding:'.esc_attr($title_padding).';';  }
	if($title_weight) $inline_css .= 'font-weight:'. esc_attr($title_weight) .';';
	if($text_transform) $inline_css .= 'text-transform:'. esc_attr($text_transform) .';';


	if($subsize){ $style .= 'font-size:'.esc_attr($subsize).'px;'; }
	if($subcolor){ $style .= 'color:'.esc_attr($subcolor).';'; }
	if($submargin){  $style .= 'margin:'.esc_attr($submargin).';';  }




	$output .= '<div class="rplatform-title rplatform-callto-signup '.esc_attr($class).'" style="text-align:'. $title_alignment .'">';
		
		//Image
		$src_image   = wp_get_attachment_image_src($icon_img, 'full');
		if(isset($src_image[0])){
			$output .= '<img src="'.$src_image[0].'" alt="image">';
		}

		//Title
		if($title != ''){
			$output .= '<h2 style="'.$inline_css.'">'.esc_attr($title).' </h2>'; 
		}

		//Sub Title
		if($subtitle2!=''){
			$output .= '<p style="'. $style .'">'.$subtitle2.'</p>';
		}

		//Button
		if($button_text!=''){
			$output .= '<a class="filled-button" href="'.$button_url.'" '.$b_color.'>'.$button_text.'</a>';
		}

	$output .= '</div>';

	return $output;

}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("Call To Signup", "rplatform"),
	"base" => "rplatform_call_to_signup",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Widget Call to Signup", "rplatform"),
	"category" => __('rp', "rplatform"),
	"params" => array(		

		array(
			"type" => "attach_image",
			"heading" => __("Upload Image Icon:", "rplatform"),
			"param_name" => "icon_img",
			"value" => "",
		),	

		array(
			"type" => "textfield",
			"heading" => __("Title", "rplatform"),
			"param_name" => "title",
			"value" => "",
			),
		array(
			"type" => "textarea",
			"heading" => __("Sub Title", "rplatform"),
			"param_name" => "subtitle2",
			"value" => "",
			),
		array(
			"type" => "dropdown",
			"heading" => __("Title Alignment", "rplatform"),
			"param_name" => "title_alignment",
			"value" => array('left'=>'left','right'=>'right','center'=>'center'),
			),
		array(
			"type" => "textfield",
			"heading" => __("Font Size", "rplatform"),
			"param_name" => "size",
			"value" => "45",
			),
		array(
			"type" => "textfield",
			"heading" => __("Sub Title Font Size", "rplatform"),
			"param_name" => "subsize",
			"value" => "45",
			),
		array(
			"type" => "colorpicker",
			"heading" => __("Title Color", "rplatform"),
			"param_name" => "color",
			"value" => "#ed1c24",
			),
		array(
			"type" => "colorpicker",
			"heading" => __("Sub Title Color", "rplatform"),
			"param_name" => "subcolor",
			"value" => "#06396a",
			),	
		array(
			"type" => "dropdown",
			"heading" => __("Title Font Wight", "rplatform"),
			"param_name" => "title_weight",
			"value" => array('400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
			),
		array(
			"type" => "dropdown",
			"heading" => __("Text Transform", "rplatform"),
			"param_name" => "text_transform",
			"value" => array('capitalize'=>'capitalize','uppercase'=>'uppercase','lowercase'=>'lowercase'),
			),
			

		array(
			"type" => "textfield",
			"heading" => __("Title Margin", "rplatform"),
			"param_name" => "title_margin",
			"value" => "0px 0px 30px 0px",
			),

		array(
			"type" => "textfield",
			"heading" => __("Sub Title Margin", "rplatform"),
			"param_name" => "submargin",
			"value" => "0px 0px 30px 0px",
			),
		array(
			"type" => "textfield",
			"heading" => __("Title Padding", "rplatform"),
			"param_name" => "title_padding",
			"value" => "0px 0px 0px 0px",
			),
		array(
			"type" => "textfield",
			"heading" => __("Button Text", "rplatform"),
			"param_name" => "button_text",
			"value" => "",
			),
		array(
			"type" => "textfield",
			"heading" => __("Button URL", "rplatform"),
			"param_name" => "button_url",
			"value" => "",
			),
		array(
            "type" => "colorpicker",
            "heading" => __("Button Background Color", "rplatform"),
            "param_name" => "bg_color",
            "value" => "#04396b",
            ),
		array(
			"type" => "textfield",
			"heading" => __("Custom Class ", "rplatform"),
			"param_name" => "class",
			"value" => "",
			),

		)
	));
}