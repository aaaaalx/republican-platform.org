<?php
add_shortcode( 'rplatform_timeline', 'rplatform_timeline_function');

function rplatform_timeline_function($atts, $content = null) {
	
	$heading 		= '';
	$count_post 	= '';
	$title_color  	= '';
	$sub_color  	= '';
	$date_color  	= '';
	$class  		= '';


	extract(shortcode_atts(array(
		'heading' 			=> '',
    	'count_post' 		=>	7,		
		'title_color' 		=> '',
		'sub_color' 		=> '',
		'date_color' 		=> '',
		'class' 			=> '',
		), $atts));

	$t_color = 'style="color:'. esc_attr( $title_color ) .'"';
	$s_color = 'style="color:'. esc_attr( $sub_color ) .'"';
	$b_color = 'style="color:'. esc_attr( $date_color ) .'"';

	global $wpdb;
  	global $post;

  	$args = array(
      'post_type' => 'time_line',
      'order' => 'ASC',
      'posts_per_page' => esc_attr($count_post)
    );

  	$timeline = new WP_Query($args);

	$output = '';
    $output .= '<div id="resume-carousel" class="carousel slide" data-ride="carousel" data-interval="0">';
    $output .= '<div class="carousel-inner" role="listbox">';

    $i=0;
  	if ( $timeline->have_posts() ){
		while($timeline->have_posts()) {
			$timeline->the_post();
			$subtitle = get_post_meta(get_the_ID(),'rplatform_timeline_subtitle',true);
			$date = get_post_meta(get_the_ID(),'rplatform_timeline_date',true);
            
            if($i==0){
            $output .= '<div class="item text-center active">';
                $output .= '<h3 '.$t_color.'>'.get_the_title().'</h3>';
                $output .= '<p '.$s_color.'>'.$subtitle.'</p>';
            $output .= '</div>';
            }
            else{
            $output .= '<div class="item text-center">';
                $output .= '<h3>'.get_the_title().'</h3>';
                $output .= '<p>'.$subtitle.'</p>';
            $output .= '</div>';
            }
            $i++;

		}//End of while
	}//End of IF
	$output .= '</div>';

    $output .= '<ol class="carousel-indicators">';
    $i=0;
    if ( $timeline->have_posts() ){
        while($timeline->have_posts()) {
            $timeline->the_post();
            $date = get_post_meta(get_the_ID(),'rplatform_timeline_date',true);
            
            $output .= '<li data-target="#resume-carousel" data-slide-to="'.$i++.'"><span '.$b_color.'>'.$date.'</span></li>';

        }//End of while
    }//End of IF
    $output .= '</ol>';

    $output .= '</div>';      

	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Timeline", 'rplatform-core'),
		"base" => "rplatform_timeline",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Timeline", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Heading", 'rplatform-core'),
				"param_name" => "heading",
				"value" => "",
				),
				
			array(
				"type" => "textfield",
				"heading" => esc_html__("Post Number To Show", 'rplatform-core'),
				"param_name" => "count_post",
				"value" => "",
				),

			array(
				"type" => "colorpicker",
				"heading" => esc_html__("Title Color", 'rplatform-core'),
				"param_name" => "title_color",
				"value" => "",
				),
			array(
				"type" => "colorpicker",
				"heading" => esc_html__("Sub Title Color", 'rplatform-core'),
				"param_name" => "sub_color",
				"value" => "",
				),
			array(
				"type" => "colorpicker",
				"heading" => esc_html__("Date Color", 'rplatform-core'),
				"param_name" => "date_color",
				"value" => "",
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}