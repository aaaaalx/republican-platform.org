<?php
add_shortcode( 'rplatform_video_popup', 'rplatform_video_popup_function');


function rplatform_video_popup_function($atts, $content = null) {

        $video_url             = '';
        $video_image           = '';
        $top_title             = '';
        $color1                = '#000';
        $title1                = '';
        $color2                = '#ed1c24';
        $title2                = '';
        $color3                = '#06396a';
        $sub_title             = '';
        $color4                = '#aeaeae';
        $button_text           = '';
        $button_url            = '';
        $datetime              = '2016/04/07';

    extract(shortcode_atts(array(

        'video_url'             => '',
        'video_image'           => '',
        'top_title'             => '',
        'color1'                => '#000',
        'title1'                => '',
        'color2'                => '#ed1c24',
        'title2'                => '',
        'color3'                => '#06396a',
        'sub_title'             => '',
        'color4'                => '#aeaeae',
        'button_text'           => '',
        'button_url'            => '',
        'datetime'              => '',

        ), $atts));

    #$datetime              = '2016/04/07';

    $output = '';

    // Video BG
    $src_image   = wp_get_attachment_image_src($video_image, 'full');

    if($src_image[0] != "" && $top_title=='' && $title2 == '' && $sub_title == '' && $button_text == ''){
        $image_style = 'style = "background: url('.esc_url($src_image[0]).') no-repeat; width:100%;"';
        $counter_style = 'style = "display:none;"';
    }
    elseif ( $src_image[0] != "" ) {
       $image_style = 'style = "background: url('.esc_url($src_image[0]).') no-repeat; width:50%;"';
       $counter_style = 'style = "display:block;"';
    }
    else{
       $image_style = 'style="background-color: #444;width:50%;"';
       $counter_style = 'style = "display:block;"';
    }

    $rand_value = rand(10,100);

//---------------------------------------------------------------------------------------------

        $output .= '<section class="champaign-area">';
            $output .= '<div class="video-container pull-left" '.$image_style.'>';
                $output .= '<a href="'. $video_url .'" id="videoPlay" data-rel="prettyPhoto"><i class="fa fa-youtube-play"></i></a>';
            $output .= '</div>';
            $output .= '<div class="counter-area pull-left" '.$counter_style.'>';
                
                if($top_title!=''){
                    $output .= '<h4>'. $top_title .'</h4>';
                }

                if($title2 != ''){
                    $output .= '<h2 class="section-title">'. $title1 .' <span>'. $title2 .'</span></h2>';
                }else{
                    $output .= '<h2 class="section-title">'. $title1 .'</h2>';
                }
                
                if( $sub_title != '' ) {
                    $output .= '<p class="section-subtitle">'. $sub_title .'</p>';
                }

                $output .= '<div id="clock'.$rand_value.'"></div>';

                if( $button_text != '' ){
                    $output .= '<a href="'. $button_url .'" class="filled-button">'. $button_text .'</a>';    
                }
                
            $output .= '</div>';
            $output .= '<div class="clearfix"></div>';
        $output .= '</section>';
        $output .= '<div class="hidden" id="passval" data-id="'.$rand_value.'" data-options="'.$datetime.'"></div>';
//---------------------------------------------------------------------------------------------

    return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("Video Popup", "rplatform"),
	"base" => "rplatform_video_popup",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Widget Video Popup", "rplatform"),
	"category" => __('rp', "rplatform"),
	"params" => array(

    	array(
            "type" => "textfield",
            "heading" => __("Video URL:","rplatform"),
            "param_name" => "video_url",
            "description" => __("Youtube/Vimo video URL", "rplatform"),
            "value" => "", 
            ),

        array(
            "type" => "attach_image",
            "heading" => __("Insert Image BG image(Video)", "rplatform"),
            "param_name" => "video_image",
            "value" => "",
            ),  

        array(
            "type" => "textfield",
            "heading" => __("Top Title", "rplatform"),
            "param_name" => "top_title",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Top Title Color", "rplatform"),
            "param_name" => "color1",
            "value" => "#000",
            ),
        array(
            "type" => "textfield",
            "heading" => __("Title Part 1", "rplatform"),
            "param_name" => "title1",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Title Part 1 Color", "rplatform"),
            "param_name" => "color2",
            "value" => "#ed1c24",
            ),    
        array(
            "type" => "textfield",
            "heading" => __("Title Part 2", "rplatform"),
            "param_name" => "title2",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Title Part 2 Color", "rplatform"),
            "param_name" => "color3",
            "value" => "#06396a",
            ),
        array(
            "type" => "textfield",
            "heading" => __("Sub Title", "rplatform"),
            "param_name" => "sub_title",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Sub Title Color", "rplatform"),
            "param_name" => "color4",
            "value" => "#aeaeae",
            ),
        array(
            "type" => "textfield",
            "heading" => __("Button Text", "rplatform"),
            "param_name" => "button_text",
            "value" => "",
            ),       
        array(
            "type" => "textfield",
            "heading" => __("Button URL", "rplatform"),
            "param_name" => "button_url",
            "value" => "",
            ),
        // Date Time Picker
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Target Time For Countdown", "rplatform"),
            "param_name" => "datetime",
            "value" => "", 
            "description" => __("Date and time format (yyyy/mm/dd hh:mm:ss).", "rplatform"),
        ),
		)
	));
}