<?php

add_shortcode( 'rplatform_volunteer', function($atts, $content = null) {

	extract(shortcode_atts(array(
		'volunteer_img' => '',
		'volunteer_name' => '',
		'volunteer_email'	=> '',
		'class' => ''
		), $atts));

		$output ='';	

		$output  	.= '<div class="volunteer-details">';
		
			$output  	.= '<div class="volunteer-img">';
				$volunteer_img   = wp_get_attachment_image_src($volunteer_img, 'full');
				$output  	.= '<img alt="" src="' . esc_url($volunteer_img[0]). '">';
				$output  	.= '<div class="volunteer-overlay"><i class="fa fa-user"></i></div>';
			$output  	.= '</div>';//volunteer-img

			if ($volunteer_name)
			$output  	.= '<h4>' . esc_attr($volunteer_name) . '</h4>';
			if ($volunteer_email) 
			$output  	.= '<a href="">' . esc_attr($volunteer_email) . '</a>';
			
		$output  	.= '</div>';//volunteer-details


	return $output;

});


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("rplatform Volunteer", 'rplatform-core'),
		"base" => "rplatform_volunteer",
		'icon' => 'icon-thm-person',
		"class" => "",
		"description" => esc_html__("Widget Title Heading", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(		

			array(
				"type" => "attach_image",
				"heading" => esc_html__("Volunteer Image", 'rplatform-core'),
				"param_name" => "volunteer_img",
				"value" => "",
				),			

			array(
				"type" => "textfield",
				"heading" => esc_html__("Volunteer Name", 'rplatform-core'),
				"param_name" => "volunteer_name",
				"value" => "",
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Volunteer Email", 'rplatform-core'),
				"param_name" => "volunteer_email",
				"value" => "",
				),			

			array(
				"type" => "textfield",
				"heading" => esc_html__("Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => ""
				),					

			)
		));
}