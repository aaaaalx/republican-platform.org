<?php
add_shortcode( 'rplatform_resume', 'rplatform_resume_function');

function rplatform_resume_function($atts, $content = null) {
	
	$heading 	= '';
	$count_post = '';
    $title_color  	= '';
    $sub_color  	= '';
    $date_color  	= '';
    $class  	= '';

	extract(shortcode_atts(array(
		'heading' 		    => '',
    	'count_post' 	    =>	-1,
        'title_color' 		=> '',
        'sub_color' 		=> '',
        'date_color' 		=> '',
        'class' 		    => '',
        ), $atts));
        
    $t_color = 'style="color:'. esc_attr( $title_color ) .'"';
    $s_color = 'style="color:'. esc_attr( $sub_color ) .'"';
    $b_color = 'style="color:'. esc_attr( $date_color ) .'"';

	global $wpdb;
  	global $post;

  	$args = array(
      'post_type' => 'time_line',
      'order' => 'ASC',
      'posts_per_page' => esc_attr($count_post),
    'post_status' => 'publish',
    );

  	$timeline = new WP_Query($args);
    $empty = $timeline->have_posts()?false:true;

	$output = '';
    $output .= '<div class="row pos-reletive collapsed'.(!$empty?' posts':'').'">';

    $output .= '<div class="timeline-row"></div>';

    $output .= '<div class="col-md-6 col-xs-6">';
    $output .= '<div class="timeline-left-content text-right">';

    $i=0;
  	if ( $timeline->have_posts() ){
		while($timeline->have_posts()) {
			$timeline->the_post();
			$subtitle = get_post_meta(get_the_ID(),'rplatform_timeline_subtitle',true);
			$date = get_post_meta(get_the_ID(),'rplatform_timeline_date',true);

            if($i%2 == 0){
                $output .= '<div class="timeline-description">';
                    $output .= '<h6 '.$t_color.'>'.get_the_title().'</h6>';
                    $output .= '<p '.$s_color.'>'.$subtitle.'</p>';
                $output .= '</div>';
            }    
            if($i%2 == 1){
                $output .= '<div class="timeline-date">';
                    $output .= '<p '.$b_color.'>'.$date.'</p>';
                $output .= '</div>';
            }
            $i++;

            /* get_the_title(); | $subtitle; | $date;*/
		}//End of while
	}//End of IF
    $output .= '</div>';
    $output .= '</div>';
    
    $output .= '<div class="col-md-6 col-xs-6">';
    $output .= '<div class="timeline-right-content text-left">';
    $i=0;
    if ( $timeline->have_posts() ){
        while($timeline->have_posts()) {
            $timeline->the_post();
            $subtitle = get_post_meta(get_the_ID(),'rplatform_timeline_subtitle',true);
            $date = get_post_meta(get_the_ID(),'rplatform_timeline_date',true);
            
            if($i%2 == 1){
                $output .= '<div class="timeline-description">';
                    $output .= '<h6  '.$t_color.'>'.get_the_title().'</h6>';
                    $output .= '<p  '.$s_color.'>'.$subtitle.'</p>';
                $output .= '</div>';
            }    
            if($i%2 == 0){
                $output .= '<div class="timeline-date">';
                    $output .= '<p '.$b_color.'>'.$date.'</p>';
                $output .= '</div>';
            }
            $i++;

        }//End of while
    }//End of IF
    $output .= '</div>';
    $output .= '</div>';

    if($i>4){
        $output .= '<div data-purpose="block-height" class="col-md-12 col-xs-12"></div>';
        $output .= '<div class="to-bottom"></div>';
    }

    $output .='</div>';         

	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Political Resume", 'rplatform-core'),
		"base" => "rplatform_resume",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Political Resume", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Heading", 'rplatform-core'),
				"param_name" => "heading",
				"value" => "",
                ),	
            array(
                "type" => "colorpicker",
                "heading" => esc_html__("Title Color", 'rplatform-core'),
                "param_name" => "title_color",
                "value" => "#ed1c24",
                ),
            array(
                "type" => "colorpicker",
                "heading" => esc_html__("Sub Title Color", 'rplatform-core'),
                "param_name" => "sub_color",
                "value" => "#2a2a2a",
                ),
            array(
                "type" => "colorpicker",
                "heading" => esc_html__("Date Color", 'rplatform-core'),
                "param_name" => "date_color",
                "value" => "#2a2a2a",
                ),	

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}