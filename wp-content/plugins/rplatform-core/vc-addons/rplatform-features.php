<?php
add_shortcode( 'rplatform_features', function($atts, $content = null) {

	extract(shortcode_atts(array(
		'alignment'				=> 'left',
		'feature_type'			=> 'fontawesome',
		'layout'				=> 'layout_left',
		'icon'					=> '',
		'fontawesome'			=> '',
		'icon_size'				=> '48',
		'icon_color'			=> '',
		'image'					=> '',		
		'title'					=> '',		
		'title_heading'			=> 'h2',		
		'title_size'			=> '24',		
		'title_color'			=> '',		
		'title_weight'			=> '700',		
		'title_margin'			=> '0 0 10px 0',		
		'sub_title'				=> '',		
		'sub_title_heading'		=> 'h3',		
		'sub_title_size'		=> '',		
		'sub_title_color'		=> '',		
		'sub_title_weight'		=> '500',		
		'sub_title_margin'		=> '0 0 10px 0',		
		'intro' 				=> '',
		'intro_size' 			=> '',
		'intro_color' 			=> '',
		'intro_margin' 			=> '',
		'class'					=> '',

		), $atts));

		$inline1 ='';
		$inline2 ='';
		$inline3 ='';
		$inline4 ='';
		$output = '';
		$align = '';

		if($alignment) { $align = 'style="text-align:'. esc_attr( $alignment ) .';"';}
		$src_image   = wp_get_attachment_image_src($image, 'full');

		// title
		if($title_weight) $inline1  	.= 'font-weight:' . (int) esc_attr( $title_weight ) . ';';
		if($title_size) $inline1 		.= 'font-size:' . (int) esc_attr( $title_size ) . 'px;line-height: normal;';
		if($title_color) $inline1		.= 'color:' . esc_attr( $title_color )  . ';';	
		if($title_margin) $inline1		.= 'margin:' . esc_attr( $title_margin )  . ';';	

		// subtitle
		if($sub_title_weight) $inline2  	.= 'font-weight:' . (int) esc_attr( $sub_title_weight ) . ';';
		if($sub_title_size) $inline2 	.= 'font-size:' . (int) esc_attr( $sub_title_size ) . 'px;line-height: normal;';
		if($sub_title_color) $inline2	.= 'color:' . esc_attr( $sub_title_color )  . ';';	
		if($sub_title_margin) $inline2	.= 'margin:' . esc_attr( $sub_title_margin )  . ';';	

		// Intro Text
		if($intro_size) $inline3 	.= 'font-size:' . (int) esc_attr( $intro_size ) . 'px;';
		if($intro_color) $inline3	.= 'color:' . esc_attr( $intro_color )  . ';';
		if($intro_margin) $inline3	.= 'margin-bottom:' . (int) esc_attr( $intro_margin )  . 'px;';			

		// Icon
		if($icon_size) $inline4 	.= 'font-size:' . (int) esc_attr( $icon_size ) . 'px;';
		if($icon_color) $inline4	.= 'color:' . esc_attr( $icon_color )  . ';';


		$output  = '<div class="addons-rplatform-features ' . esc_attr($class) . '">';

		    switch ($layout) {
		        case 'layout_left':
		        	$output  .= '<div class="rplatform-features media" '.$align.'>';
						if($feature_type == 'image_type'){
							$output  .= '<div class="pull-left layout-left image">';
								$output  	.= '<img style="display:inline-block;" src="' . esc_url($src_image[0]). '" class="img-responsive" alt="photo">';
							$output  .= '</div>';	
						}else{
							$output  .= '<div class="pull-left layout-left icon" style="display:inline-block;">';
								$output  .= '<i class="fa ' . esc_attr($fontawesome) . '" style="' . $inline4 . '"></i>';
							$output  .= '</div>';	
						}		
						$output  .= '<div class="media-body rplatform-feature-content">';
							if ($title) {
								$output 	.= '<'.esc_attr($title_heading).' class="feature-title" style="'.$inline1.'">' . esc_attr( $title ) . '</'.esc_attr($title_heading).'>';
							}	
							if ($sub_title) {
								$output 	.= '<'.esc_attr($sub_title_heading).' class="feature-sub-title" style="'.$inline2.'">' . balanceTags( $sub_title ) . '</'.esc_attr($sub_title_heading).'>';	
							}
							if ($intro) {
								$output 	.= '<p class="feature-intro-text" style="'.$inline3.'">' . balanceTags( $intro ) . '</p>';	
							}	

						$output  .= '</div>';#media-body
					$output .= '</div>'; #rplatform-features
		        break;

		        case 'layout_top':
		        	$output  .= '<div class="rplatform-features" '.$align.'>';
						if($feature_type == 'image_type'){
							$output  .= '<div class="layout-top image">';
								$output  	.= '<img style="display:inline-block;" src="' . esc_url($src_image[0]). '" class="img-responsive" alt="photo">';
							$output  .= '</div>';	
						}else{
							$output  .= '<div class="layout-top icon" style="display:inline-block;">';
								$output  .= '<i class="fa ' . esc_attr($fontawesome) . '" style="' . $inline4 . '"></i>';
							$output  .= '</div>';	
						}		
						$output  .= '<div class="rplatform-feature-content">';
							if ($title) {
								$output 	.= '<'.esc_attr($title_heading).' class="feature-title" style="'.$inline1.'">' . esc_attr( $title ) . '</'.esc_attr($title_heading).'>';
							}	
							if ($sub_title) {
								$output 	.= '<'.esc_attr($sub_title_heading).' class="feature-sub-title" style="'.$inline2.'">' . balanceTags( $sub_title ) . '</'.esc_attr($sub_title_heading).'>';	
							}
							if ($intro) {
								$output 	.= '<p class="feature-intro-text" style="'.$inline3.'">' . balanceTags( $intro ) . '</p>';	
							}	
							
						$output  .= '</div>';#rplatform-feature-content

					$output .= '</div>'; #rplatform-features
		        break;   

		        case 'layout_right':
		        	$output  .= '<div class="rplatform-features media" '.$align.'>';
						if($feature_type == 'image_type'){
							$output  .= '<div class="pull-right layout-right image">';
								$output  	.= '<img style="display:inline-block;" src="' . esc_url($src_image[0]). '" class="img-responsive" alt="photo">';
							$output  .= '</div>';	
						}else{
							$output  .= '<div class="pull-right layout-right icon" style="display:inline-block;">';
								$output  .= '<i class="fa ' . esc_attr($fontawesome) . '" style="' . $inline4 . '"></i>';
							$output  .= '</div>';	
						}		
						$output  .= '<div class="media-body rplatform-feature-content">';
							if ($title) {
								$output 	.= '<'.esc_attr($title_heading).' class="feature-title" style="'.$inline1.'">' . esc_attr( $title ) . '</'.esc_attr($title_heading).'>';
							}	
							if ($sub_title) {
								$output 	.= '<'.esc_attr($sub_title_heading).' class="feature-sub-title" style="'.$inline2.'">' . balanceTags( $sub_title ) . '</'.esc_attr($sub_title_heading).'>';	
							}
							if ($intro) {
								$output 	.= '<p class="feature-intro-text" style="'.$inline3.'">' . balanceTags( $intro ) . '</p>';	
							}	
							
						$output  .= '</div>';#media-body
					$output .= '</div>'; #rplatform-features
		        break;  

		        default:
		        	$output  .= '<div class="rplatform-features media" '.$align.'>';
						if($feature_type == 'image_type'){
							$output  .= '<div class="pull-left layout-left image">';
								$output  	.= '<img style="display:inline-block;" src="' . esc_url($src_image[0]). '" class="img-responsive" alt="photo">';
							$output  .= '</div>';	
						}else{
							$output  .= '<div class="pull-left layout-left icon" style="display:inline-block;">';
								$output  .= '<i class="fa ' . esc_attr($fontawesome) . '" style="' . $inline4 . '"></i>';
							$output  .= '</div>';
						}		
						$output  .= '<div class="media-body rplatform-feature-content">';
							if ($title) {
								$output 	.= '<'.esc_attr($title_heading).' class="feature-title" style="'.$inline1.'">' . esc_attr( $title ) . '</'.esc_attr($title_heading).'>';
							}	
							if ($sub_title) {
								$output 	.= '<'.esc_attr($sub_title_heading).' class="feature-sub-title" style="'.$inline2.'">' . balanceTags( $sub_title ) . '</'.esc_attr($sub_title_heading).'>';	
							}
							if ($intro) {
								$output 	.= '<p class="feature-intro-text" style="'.$inline3.'">' . balanceTags( $intro ) . '</p>';	
							}	
							
						$output  .= '</div>';#media-body
					$output .= '</div>'; #rplatform-features
		        break;
		    }

		$output .= '</div>'; #addons-rplatform-features

		return $output;


});


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => esc_html__("rplatform Features", 'rplatform-core'),
	"base" => "rplatform_features",
	'icon' => 'icon-thm-features',
	"class" => "",
	"description" => esc_html__("Widget Title Heading", 'rplatform-core'),
	"category" => esc_html__('rp', 'rplatform-core'),
	"params" => array(

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Alignment", 'rplatform-core'),
			"param_name" => "alignment",
			"value" => array('Select'=>'','left'=>'left','center'=>'center','right'=>'right'),
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Select Layout", 'rplatform-core'),
			"param_name" => "layout",
			"value" => array('Select'=>'','Icon/Image Left'=>'layout_left','Icon/Image Top'=>'layout_top','Icon/Image Right'=>'layout_right'),
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Features type(icon/image)", 'rplatform-core'),
			"param_name" => "feature_type",
			"value" => array('Select'=>'','Font Awesome'=>'fontawesome','Image'=>'image_type'),
		),	

		array(
			"type" => "dropdown",
			"heading" => esc_html__("FontAwesome Icon Name ", 'rplatform-core'),
			"param_name" => "fontawesome",
			"value" => getIconsList(),
			"dependency" => Array("element" => "feature_type", "value" => array("fontawesome")),
		),	

		array(
			"type" => "attach_image",
			"heading" => esc_html__("Image Box", 'rplatform-core'),
			"param_name" => "image",
			"value" => "",
			"dependency" => Array("element" => "feature_type", "value" => array("image_type")),
		),	

		array(
			"type" => "textfield", 
			"heading" => esc_html__("Icon Font Size", 'rplatform-core'),
			"param_name" => "icon_size",
			"value" => "48",
			"dependency" => Array("element" => "feature_type", "value" => array("fontawesome")),
		),	

		array(
			"type" => "colorpicker",
			"heading" => esc_html__("Title Color", 'rplatform-core'),
			"param_name" => "icon_color",
			"value" => "",
			"dependency" => Array("element" => "feature_type", "value" => array("fontawesome")),
		),

		//title
		array(
			"type" => "textfield",
			"heading" => esc_html__("Title", 'rplatform-core'),
			"param_name" => "title",
			"value" => "",
		),	

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Title Heading", 'rplatform-core'),
			"param_name" => "title_heading",
			"value" => array('Select'=>'','h1'=>'h1','h2'=>'h2','h3'=>'h3','h4'=>'h4','h5'=>'h5'),
		),	

		array(
			"type" => "textfield", 
			"heading" => esc_html__("Title Font Size", 'rplatform-core'),
			"param_name" => "title_size",
			"value" => "24",
		),		

		array(
			"type" => "colorpicker",
			"heading" => esc_html__("Title Color", 'rplatform-core'),
			"param_name" => "title_color",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Title Font Weight", 'rplatform-core'),
			"param_name" => "title_weight",
			"value" => array('Select'=>'','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700'),
		),

		array(
			"type" => "textfield",
			"heading" => esc_html__("Title Margin ex. 5px 5px 5px 5px", 'rplatform-core'),
			"param_name" => "title_margin",
			"value" => "",
		),	

		//subtile
		array(
			"type" => "textarea", 
			"heading" => esc_html__("Date", 'rplatform-core'),
			"param_name" => "sub_title",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Date Heading", 'rplatform-core'),
			"param_name" => "sub_title_heading",
			"value" => array('Select'=>'','h1'=>'h1','h2'=>'h2','h3'=>'h3','h4'=>'h4','h5'=>'h5'),
		),	

		array(
			"type" => "textfield",
			"heading" => esc_html__("Date Font Size", 'rplatform-core'),
			"param_name" => "sub_title_size",
			"value" => "",
		),	

		array(
			"type" => "colorpicker",
			"heading" => esc_html__("Date Color", 'rplatform-core'),
			"param_name" => "sub_title_color",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Date Font Weight", 'rplatform-core'),
			"param_name" => "sub_title_weight",
			"value" => array('Select'=>'','200'=>'200','300'=>'300','400'=>'400','500'=>'500','600'=>'600','700'=>'700'),
		),	

		array(
			"type" => "textfield",
			"heading" => esc_html__("Date Margin ex. 5px 5px 5px 5px", 'rplatform-core'),
			"param_name" => "sub_title_margin",
			"value" => "",
		),	

		//intro
		array(
			"type" => "textarea", 
			"heading" => esc_html__("Description", 'rplatform-core'),
			"param_name" => "intro",
			"value" => "",
		),

		array(
			"type" => "textfield",
			"heading" => esc_html__("Description Font Size", 'rplatform-core'),
			"param_name" => "intro_size",
			"value" => "",
		),	

		array(
			"type" => "colorpicker",
			"heading" => esc_html__("Description Color", 'rplatform-core'),
			"param_name" => "intro_color",
			"value" => "",
		),	

		array(
			"type" => "textfield",
			"heading" => esc_html__("Description Margin Bottom", 'rplatform-core'),
			"param_name" => "intro_margin",
			"value" => "",
		),	

		array(
			"type" => "textfield",
			"heading" => esc_html__("Custom Class ", 'rplatform-core'),
			"param_name" => "class",
			"value" => "",
		),

		)
	));
}