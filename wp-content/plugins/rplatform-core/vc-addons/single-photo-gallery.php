<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

//Upcoming Events
add_shortcode( 'single_photogallery', function($atts, $content = null) {

	extract(shortcode_atts(array(
		'title' => '',
		'limit' => 6,
		'boxed'	=> '',
		'class'	=> ''
		), $atts));

	global $post;

	$post_ids = array();

	$posts = get_posts(array('post_type'=>'gallery', 'posts_per_page'=>1, 'order' => 'ASC',));

	$output  = '<section class="conference-area '. $boxed . ' ' . $class .'">';


	if ( count($posts) ) {
		

		foreach ($posts as $post) {
			setup_postdata( $post );

			$photos = get_post_meta(get_the_ID(), 'rplatform_rp_event_gallery');
			

			$slices = (count($photos) > $limit) ? array_slice($photos, 0, $limit , true ) : $photos;

			foreach ($slices as $key=>$photo) {

                $photo_thumb = wp_get_attachment_image_src( $photo, 'rp-medium' );
                $photo_thumb_full = wp_get_attachment_image_src( $photo, 'full_url' );

				$output .= '<div class="conference-img">';
				$output 	.= '<a href="' . $photo_thumb_full . '" class="plus-icon" rel="prettyPhoto"><img src="' . $photo_thumb[0] . '" class="img-responsive" alt="" /></a>';
				$output .= '</div>';
				
			}
		}

		wp_reset_postdata();

		
	}


	$output .= '</section>';

	return $output;

});

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => __("Single Photo Gallery", "rp-event"),
		"base" => "single_photogallery",
		'icon' => 'icon-thm-gallery',
		"class" => "",
		"description" => "Photo Gallery",
		"category" 	=> __('rp', "rplatform"),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => __("Title"),
				"param_name" => "title",
				"value" => "",
				"admin_label"=>true,
				),

			array(
				"type" => "textfield",
				"heading" => __("Limit"),
				"param_name" => "limit",
				"value" => "",
				"description" => "Please enter the maximum number of audio files. eg. 12"
				),

			array(
				"type" => 'checkbox',
				"heading" => __("Boxed layout?", "rp-event"),
				"param_name" => "boxed",
				"description" => __("If selected, widget will display inside box.", "rp-event"),
				"value" => Array(__("Yes", "rp-event") => "box")
				),

			array(
				"type" => "textfield",
				"heading" => __("Extra CSS Class", "rp-event"),
				"param_name" => "class",
				"value" => "",
				"description" => "Add your class"
				),

			)
		));
}
