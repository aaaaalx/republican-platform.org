<?php
add_shortcode( 'rplatform_video_popup_timecounter', 'rplatform_video_popup_timecounter_function');


function rplatform_video_popup_timecounter_function($atts, $content = null) {

        $video_url             = '';
        $video_image           = '';
        $top_title             = '';       
        $color1                = '#000';
        $title1                = '';
        $color2                = '#ed1c24';
        $bg_color              = '#04396b';
        $title2                = '';
        $color3                = '#06396a';
        $sub_title             = '';
        $color4                = '#aeaeae';
        $button_text           = '';
        $button_url            = '';
        $button_float          = '';        
        $datetime              = '2016/04/07';
        $time_counter_class    = '#FFFFFF';

    extract(shortcode_atts(array(

        'video_url'             => '',
        'video_image'           => '',
        'top_title'             => '',
        'color1'                => '#000',
        'bg_color'              => '#04396b',
        'title1'                => '',
        'color2'                => '#ed1c24',
        'title2'                => '',
        'color3'                => '#06396a',
        'sub_title'             => '',
        'color4'                => '#aeaeae',
        'button_text'           => '',
        'button_url'            => '',
        'button_float'          => '',
        'datetime'              => '',
        'time_counter_class'    => '',

        ), $atts));


    $output = $clock_style = $button_style = $inline1 = $inline2 = $inline3  = '';

    $b_color = 'style="background:'. esc_attr( $bg_color ) .'"';

    // Video BG
    $src_image   = wp_get_attachment_image_src($video_image, 'full');

    if($src_image[0] != "" && $top_title=='' && $title2 == '' && $sub_title == '' && $button_text == ''){
        $image_style = 'style = "background: url('.esc_url($src_image[0]).') no-repeat; width:100%;"';
        $counter_style = 'style = "display:none;"';
    }
    elseif ( $src_image[0] != "" ) {
       $image_style = 'style = "background: url('.esc_url($src_image[0]).') no-repeat;"';
       $counter_style = 'style = "display:block;"';
    }
    else{
       $image_style = 'style="background-color: #444;width:50%;"';
       $counter_style = 'style = "display:block;"';       
    }

    if($src_image[0] == ""){
        $image_style = 'style = "display:none;"';
        $counter_style = 'style = "display:block; width:100%; padding: 30px 0 0px 0px;"';
        $clock_style .= 'style = "display:table; width:42%; color: white; margin: 0 auto; padding-bottom: 0px;"';
        $button_style .= 'style = "width:200px; display: block; float:'.$button_float.';"';
    }

    $rand_value = rand(10,100);

    if($color2) $inline1 .= 'color:' . esc_attr( $color2 )  . ';';

    if($color3) $inline2 .= 'color:' . esc_attr( $color3 )  . ';';

    if($color4) $inline3 .= 'color:' . esc_attr( $color4 )  . ';';

//---------------------------------------------------------------------------------------------
/*
 
*/    

        $output .= '<section class="champaign-area">';
        $output .= '<div class="container-fluid">';
        $output .= '<div class="row">'; 

            $output .= '<div class="col-md-6 no-padding col-xs-12 col-sm-6" '.$image_style.'>'; 
            $output .= '<div class="video-container pull-left" '.$image_style.'>';
                $output .= '<a href="'. $video_url .'" id="videoPlay" data-rel="prettyPhoto"><i class="fa fa-youtube-play"></i></a>';
            $output .= '</div>';
            $output .= '</div>';


            if($src_image[0] == ""){
                $output .= '<div class="col-md-12 no-padding col-xs-12 col-sm-12">';     
            }
            else{
                $output .= '<div class="col-md-6 no-padding col-xs-12 col-sm-6">';
            }
            
            $output .= '<div class="counter-area pull-left" '.$counter_style.'>';
                if($top_title!=''){
                    $output .= '<h4 style="color:'.$color1.';">'. $top_title .'</h4>';
                }

                if($title2 != ''){
                    $output .= '<h2 class="section-title" style="'.$inline1.'">'. $title1 .' <span style="'.$inline2.'">'. $title2 .'</span></h2>';
                }else{
                    $output .= '<h2 class="section-title" style="'.$inline1.'">'. $title1 .'</h2>';
                }
                
                if( $sub_title != '' ) {
                    $output .= '<p class="section-subtitle" style="'.$inline3.'">'. $sub_title .'</p>';
                }

                $output .= '<div class="'.$time_counter_class.' clock" '.$clock_style.' id="clock'.$rand_value.'"></div>';

                if( $button_text != '' ){
                    $output .= '<a href="'. $button_url .'" class="filled-button" '.$button_style.' '.$b_color.'>'. $button_text .'</a>';    
                }
            $output .= '</div>';
            $output .= '</div>';            
            #$output .= '<div class="clearfix"></div>';

        $output .= '</div>';
        $output .= '</div>';    
        $output .= '</section>';
        $output .= '<div class="hidden" id="passval" data-id="'.$rand_value.'" data-options="'.$datetime.'"></div>';

    return $output;

}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("Video Popup-Time Countdown", "rplatform"),
	"base" => "rplatform_video_popup_timecounter",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Widget Video Popup", "rplatform"),
	"category" => __('rp', "rplatform"),
	"params" => array(

    	array(
            "type" => "textfield",
            "heading" => __("Video URL:","rplatform"),
            "param_name" => "video_url",
            "description" => __("Youtube/Vimo video URL", "rplatform"),
            "value" => "", 
            ),

        array(
            "type" => "attach_image",
            "heading" => __("Insert Image BG image(Video)", "rplatform"),
            "param_name" => "video_image",
            "value" => "",
            ),  

        array(
            "type" => "textfield",
            "heading" => __("Top Title", "rplatform"),
            "param_name" => "top_title",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Top Title Color", "rplatform"),
            "param_name" => "color1",
            "value" => "#000",
            ),
        array(
            "type" => "textfield",
            "heading" => __("Title Part 1", "rplatform"),
            "param_name" => "title1",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Title Part 1 Color", "rplatform"),
            "param_name" => "color2",
            "value" => "#ed1c24",
            ),    
        array(
            "type" => "textfield",
            "heading" => __("Title Part 2", "rplatform"),
            "param_name" => "title2",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Title Part 2 Color", "rplatform"),
            "param_name" => "color3",
            "value" => "#06396a",
            ),
        array(
            "type" => "textarea",
            "heading" => __("Sub Title", "rplatform"),
            "param_name" => "sub_title",
            "value" => "",
            ),
        array(
            "type" => "colorpicker",
            "heading" => __("Sub Title Color", "rplatform"),
            "param_name" => "color4",
            "value" => "#aeaeae",
            ),
        array(
            "type" => "textfield",
            "heading" => __("Button Text", "rplatform"),
            "param_name" => "button_text",
            "value" => "",
            ),       
        array(
            "type" => "textfield",
            "heading" => __("Button URL", "rplatform"),
            "param_name" => "button_url",
            "value" => "",
            ),
        //Button float
        array(
            "type" => "dropdown",
            "heading" => __("Button Float", "rplatform"),
            "param_name" => "button_float",
            "value" => array('Select'=>'','Left'=>'left','Right'=>'right'), 
            ),  
        array(
            "type" => "colorpicker",
            "heading" => __("Button Background Color", "rplatform"),
            "param_name" => "bg_color",
            "value" => "#04396b",
            ),
        // Date Time Picker
        array(
            "type" => "textfield",
            "class" => "",
            "heading" => __("Target Time For Countdown", "rplatform"),
            "param_name" => "datetime",
            "value" => "", 
            "description" => __("Date and time format (yyyy/mm/dd hh:mm:ss).", "rplatform"),
        ),
        array(
            "type" => "textfield",
            "heading" => esc_html__("Custom Class", 'rplatform-core'),
            "param_name" => "time_counter_class",
            "value" => "",
        ),               

		)
	));
}