<?php
add_shortcode( 'rplatform_news_feed', function($atts, $content = null) {

	extract(shortcode_atts(array(
		'category' 						=> '',
		'number' 						=> '4',
		'class' 						=> '',
        'order_by'  					=> 'date',
        'show_date'  					=> 'yes',
        'order'   						=> 'DESC',
		), $atts));

 	global $post;
 	$output     = '';
 	$counter = 1;

 	if (isset($category) && $category!='') {
 		$idObj 	= get_category_by_slug( $category );

 		if (isset($idObj) && $idObj!='') {
			$idObj 	= get_category_by_slug( $category );
			$cat_id = $idObj->term_id;

			$args = array(
		    	'category' => $cat_id,
		        'orderby' => $order_by,
		        'order' => $order,
		        'posts_per_page' => esc_attr($number),

		    );
		    $posts = get_posts($args);
 		}else{
 			$args = 0;
 		}
 		}else{
			$args = array(
		        'orderby' => $order_by,
		        'order' => $order,
		        'posts_per_page' => esc_attr($number),
		    );
		    $posts = get_posts($args);
	 	}




	    if(count($posts)>0){
	    	$output .= '<div class="news-feed row">';
	    	foreach ($posts as $post): setup_postdata($post);


				$output .= '<div class="col-sm-10 rp-news-feed-pro">';
                    $output .= '<div class="news-feed-wrap">';
                        $output .= '<div class="news-feed-meta"><i class="rplatform-moviewclock"></i><span class="meta-date">'. get_the_date( get_option('date_format')) .'</span></div>';

                        $output .= '<h3><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h3>';
                        $output .= '<p class="news-feed-content">'. rp_excerpt_max_char(200). '</p>';
                    $output .= '</div>';
                $output .= '</div>';



		$counter++;
		endforeach;
		wp_reset_postdata();
		$output .= '</div>';
		}

	return $output;

});


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("News Feed", 'rplatform-core'),
		"base" => "rplatform_news_feed",
		'icon' => 'icon-thm-news-feeds',
		"class" => "",
		"description" => esc_html__("Widget News Feed", 'rplatform-core'),
		"category" => esc_html__('rp', 'rplatform-core'),
		"params" => array(


			array(
				"type" => "dropdown",
				"heading" => esc_html__("Category Filter", 'rplatform-core'),
				"param_name" => "category",
				"value" => rplatform_cat_list('category'),
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Number of items", 'rplatform-core'),
				"param_name" => "number",
				"value" => "4",
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("OderBy", 'rplatform-core'),
				"param_name" => "order_by",
				"value" => array('Date'=>'date','Title'=>'title','Modified'=>'modified','Author'=>'author','Random'=>'rand'),
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Show Date", 'rplatform-core'),
				"param_name" => "show_date",
				"value" => array('YES'=>'yes','NO'=>'no'),
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Order", 'rplatform-core'),
				"param_name" => "order",
				"value" => array('DESC'=>'DESC','ASC'=>'ASC'),
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'rplatform-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}
