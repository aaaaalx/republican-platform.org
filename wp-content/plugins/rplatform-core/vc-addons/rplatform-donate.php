<?php
add_shortcode( 'rplatform_donate', 'rplatform_donate_function');


function rplatform_donate_function($atts, $content = null) {

	$currency 	= '$';
	$paypalid 	= '';	
	$bg_color	='#06396a';
	$class		= '';
	$target 	= '';
	$more 		= '';
	$amount1	= '';
	$amount2	= '';
	$amount3	= '';		
	$amount4	= '';
	$btn_name	= 'Donate Now';

	extract(shortcode_atts(array(
		'currency' 	=> '$',
		'paypalid'	=> '',
		'target' 	=> '_self',
		'more' 		=> '',
		'bg_color'	=> '#06396a',
		'amount1'	=> '',
		'amount2'	=> '',
		'amount3'	=> '',
		'amount4'	=> '',	
		'btn_name'	=> 'Donate Now',	
		'class'		=> '',								
	), $atts));

	$b_color = 'style="background:'. esc_attr( $bg_color ) .'"';

	$amounts = 0;
 	$donations = array(intval($amount1), intval($amount2), intval($amount3), intval($amount4));
	$output  = '<div class="rp-addon-donation ' . $class . '">';
	$output .= '<div class="donation-ammount-wrap donate-buttons" data-currency="'.$currency.'" data-pid="'.$paypalid.'">';

	$crcy_code = explode(':', $currency);

		foreach ($donations as $key => $donation) {
			$active = ( $key > 1 && ((count($donations)/$key) == $key) ) ? 'active' : '' ;
			if($key==2){
				$output .= '<input class="donation-input '.$active.'" type="text" name="amount" value="'.$crcy_code[1].$donation.'" readonly>';	
				$amounts = $donation;
			}else{
				$output .= '<input class="donation-input" type="text" name="amount" value="'.$crcy_code[1].$donation.'" readonly>';					
			}
		}
		if ( $more ) {
			$output .= '<input class="donation-input input-text" type="number" name="amount" autocomplete="off" placeholder="'.esc_html__( 'More', 'rp' ).'" min="1">';
		}
	$output .= '</div>'; //.donation-ammount
	$output .= '<div class="donation-button">';
		$output .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business='.$paypalid.'&item_name=donation&amount='.$amounts.'&currency_code='.$crcy_code[0].'" target="' . $target . '" class="filled-button donation-button-link" '.$b_color.'>' . $btn_name . '</a>';
	$output .= '</div>'; //.donation-button
	$output .= '</div>'; //.rp-addon-donation

	return $output;
}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" 				=> __("Donate Now", "rplatform"),
	"base" 				=> "rplatform_donate",
	'icon' 				=> 'icon-thm-title',
	"class" 			=> "",
	"description" 		=> __("Donate now widget", "rplatform"),
	"category" 			=> __('rp', "rplatform"),
	"params" 			=> array(			

		array(
			"type" 			=> "dropdown",
			"heading" 		=> __("Select Currency", "rplatform"),
			"param_name" 	=> "currency",
			"value" 		=> getCurrencyList(),
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Paypal ID", "rplatform"),
			"param_name" 	=> "paypalid",
			"value" 		=> "",
		),

		array(
			"type" => "colorpicker",
			"heading" => __("Background Color", "rplatform"),
			"param_name" => "bg_color",
			"value" => "#04396b",
			),

		array(
			"type" 			=> "dropdown",
			"heading" 		=> __("The target attribute for link", "rplatform"),
			"param_name" 	=> "target",
			"value" 		=> array('Select'=>'','Same Window'=>'_self','New Window'=>'_blank'),
		),

		array(
			"type" 			=> "dropdown",
			"heading" 		=> __("More", "rplatform"),
			"param_name" 	=> "more",
			"value" 		=> array('Select'=>'','Yes' => 1,'No' => 0),
		),

		// Amount-1 Number
		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Amount-1: ", "rplatform"),
			"param_name" 	=> "amount1",
			"value" 		=> "",
		),		

		// Amount-2 Number
		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Amount-2: ", "rplatform"),
			"param_name" 	=> "amount2",
			"value" 		=> "",
		),

		// Amount-3 Number
		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Amount-3: ", "rplatform"),
			"param_name" 	=> "amount3",
			"value" 		=> "",
		),		

		// Amount-4 Number
		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Amount-4: ", "rplatform"),
			"param_name" 	=> "amount4",
			"value" 		=> "",
		),

		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Button name: Ex. Donate Now ", "rplatform"),
			"param_name" 	=> "btn_name",
			"value" 		=> "Donate Now",
		),		

		array(
			"type" 			=> "textfield",
			"heading" 		=> __("Custom CSS", "rplatform"),
			"param_name" 	=> "class",
			"value" 		=> "",
			),		

		)
	));
}
?>