<?php
add_shortcode( 'rplatform_counter', function($atts, $content = null) {

	extract(shortcode_atts(array(
		'number'			=> '',
		'duration'			=> '',
		'font_size'			=> '36',
		'border_color' 		=> 'rgba(255, 255, 255, 0)',
		'border_width' 		=> '3',
		'border_radius' 	=> '5',
		'padding' 			=> '10px',
		'margin' 			=> '10px 0',
		'color' 			=> 'rgba(255, 255, 255, 0)',
		'background' 		=> 'rgba(255, 255, 255, 0)',
		'counter_title' 	=> '',
		'title_font_size'	=> '18',
		'counter_color' 	=> 'rgba(255, 255, 255, 0)',
		'alignment'			=> 'left',
		'class'				=>'',
		), $atts));

	$style 			= '';
	$number_style 	= '';
	$text_style 	= '';
	$align 			= '';

	if($alignment) $align .= 'text-align:'. $alignment .';';

	if($background) $style .= 'background-color:' . $background  . ';';
	if($border_color) $style .= 'border-style:solid;border-color:' . $border_color  . ';';
	if($border_width) $style .= 'border-width:' . (int) $border_width  . 'px;';
	if($border_radius) $style .= 'border-radius:' . (int) $border_radius  . 'px;';
	if($padding) $style .= 'padding:' . $padding  . ';';

	if($color) $number_style .= 'color:' . $color  . ';';
	if($font_size) $number_style .= 'font-size:' . (int) $font_size . 'px;line-height:' . (int) $font_size . 'px;';

	if($counter_color) $text_style .= 'color:' . $counter_color  . ';';
	if($margin) $text_style .= 'margin:' . $margin  . ';';
	if($title_font_size) $text_style .= 'font-size:' . (int) $title_font_size . 'px;line-height:' . (int) $title_font_size . 'px;';

	$output  = '<div class="rplatform-shortocde-counter ' . $class . '" style="'. $align . ' background-repeat:no-repeat; background-position: center; background-size: contain;">';
	$output .= '<div class="counter-content" style="' . $style . '">';
	$output .= '<div class="counter" data-digit="'. $number .'" data-duration="' . $duration . '" style="'. $number_style .'">'. $number .'</div>';
	if($counter_title) {
		$output .= '<div class="counter-number-title" style="' . $text_style . '">' . $counter_title . '</div>';
	}
	
	$output .= '</div>';
	$output .= '</div>';

	return $output;

});


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("rplatform Counter", "rplatform"),
	"base" => "rplatform_counter",
	'icon' => 'icon-thm-counter',
	"class" => "",
	"description" => __("Widget Counter", "rplatform"),
	"category" => __('rp', "rplatform"),
	"params" => array(

		array(
			'type'=>'textfield', 
			"heading" => __("Digit", "rplatform"),
			"param_name" => "number",
			"value" => "",
			),		

		array(
			'type'=>'textfield', 
			"heading" => __("Duration", "rplatform"),
			"param_name" => "duration",
			"value" => "",
			),		

		array(
			'type'=>'textfield', 
			"heading" => __("Counter Title", "rplatform"),
			"param_name" => "counter_title",
			"value" => "Counter Number",
			),

		array(
			"type" => "dropdown",
			"heading" => __("Content Alignment", "rplatform"),
			"param_name" => "alignment",
			"value" => array('Left'=>'left','Center'=>'center','Right'=>'right'),
			),	

		array(
			"type" => "colorpicker",
			"heading" => __("Animated Number Color", "rplatform"),
			"param_name" => "color",
			"value" => "",
			),	

		array(
			'type'=>'textfield', 
			"heading" => __("Animated Number Font Size", "rplatform"),
			"param_name" => "font_size",
			"value" => "",
			),		

		array(
			'type'=>'textfield', 
			"heading" => __("Title Font Size", "rplatform"),
			"param_name" => "title_font_size",
			"value" => "",
			),

		array(
			"type" => "textfield",
			"heading" => __("Title Margin ex. 10px 0", "rplatform"),
			"param_name" => "margin",
			"value" => "",
			),			

		array(
			"type" => "colorpicker",
			"heading" => __("Title Font Color", "rplatform"),
			"param_name" => "counter_color",
			"value" => "",
			),	

		array(
			"type" => "colorpicker",
			"heading" => __("Background Color", "rplatform"),
			"param_name" => "background",
			"value" => "",
			),						

		array(
			"type" => "colorpicker",
			"heading" => __("Border Color", "rplatform"),
			"param_name" => "border_color",
			"value" => "",
			),	

		array(
			"type" => "textfield",
			"heading" => __("Border Width", "rplatform"),
			"param_name" => "border_width",
			"value" => "",
			),	

		array(
			"type" => "textfield",
			"heading" => __("Border Radius", "rplatform"),
			"param_name" => "border_radius",
			"value" => "",
			),			

		array(
			"type" => "textfield",
			"heading" => __("Padding ex. 20px 20px 20px 20px", "rplatform"),
			"param_name" => "padding",
			"value" => "",
			),			

		array(
			"type" => "textfield",
			"heading" => __("Extend Class", "rplatform"),
			"param_name" => "class",
			"value" => "",
			),			

		)
	));
}