<?php
add_shortcode( 'rplatform_call_to_join', 'rplatform_call_to_join_function');


function rplatform_call_to_join_function($atts, $content = null) {

	$title 				='';
	$color				='#ed1c24';
	$size				='45';
	$title_margin		='';
	$title_padding		='0px 0px 30px 0px';
	$title_weight		='700';
	$class				='';
	$title_alignment	='center';
	$text_transform 	= 'capitalize';

	$button_text 		= '';
	$button_url 		= '';
	$button_text_two 	= '';
	$button_url_two 	= '';
	$btn_layout			= 'normal';

	extract(shortcode_atts(array(

		'title' 			=> '',
		'title_alignment'	=> 'center',
		'color'				=> '#ed1c24',
		'size'				=> '45',
		'title_margin'		=> '0px 0px 30px 0px',
		'title_padding'		=> '',
		'title_weight'		=> '700',
		'class'				=> '',
		'text_transform'	=> 'capitalize',

		'button_text'		=> '',
		'button_url'		=> '',
		'button_text_two' 	=> '',
		'button_url_two' 	=> '',
		'btn_layout'		=> 'normal',
	), $atts));

	
	

	$inline_css = $output = $inline_css_span = $style = $inline1 = '';

	if($color){ $inline_css .= 'color:'.esc_attr($color).';'; }
	if($size){ $inline_css .= 'font-size:'.esc_attr($size).'px;'; }
	if($title_margin){  $inline_css .= 'margin:'.esc_attr($title_margin).';';  }
	if($title_padding){  $inline_css .= 'padding:'.esc_attr($title_padding).';';  }
	if($title_weight) $inline_css .= 'font-weight:'. esc_attr($title_weight) .';';
	if($text_transform) $inline_css .= 'text-transform:'. esc_attr($text_transform) .';';







	$output .= '<div class="rplatform-title-donate '.esc_attr($class).'" style="text-align:'. $title_alignment .'">';
		// $output .= '<div class="container">';
  //           $output .= '<div class="row">';
			//Title
			$output .= '<div class="col-sm-12 col-md-12 col-lg-6 col-xs-12 rplatform-title-call no-padding">';
    		if($title != ''){
					$output .= '<h2 class="call-to-title" style="'.$inline_css.'">'.esc_attr($title).' </h2>'; 
				}
			$output .= '</div>';

			# Button
			$output .= '<div class="col-sm-12 col-md-12 col-lg-6 col-xs-12 rplatform-join-campaign text-right no-padding">';

    		if($button_text!=''){
					switch ($btn_layout) {
						case 'transparent':
							$inline1 = 'btn btn-transparent';
							break;				
						case 'white':
							$inline1 = 'btn btn-transparent white';
							break;				
						case 'underline':
							$inline1 = 'btn-underline';
							break;				
						case 'normal':
							$inline1 = 'btn-plain';
							break;
						default:
							$inline1 = 'btn-plain';
							break;
					}
					$output .= '<a class="bordered-button champ '.$inline1.' " href="'.$button_url.'">'.$button_text.'</a>';
				}

				# Button Two
				if($button_text_two!=''){

					switch ($btn_layout) {
						case 'transparent':
							$inline1 = 'btn btn-transparent';
							break;				
						case 'white':
							$inline1 = 'btn btn-transparent white';
							break;				
						case 'underline':
							$inline1 = 'btn-underline';
							break;				
						case 'normal':
							$inline1 = 'btn-plain';
							break;
						default:
							$inline1 = 'btn-plain';
							break;
					}

					$output .= '<a class="bordered-button donate '.$inline1.' " href="'.$button_url_two.'">'.$button_text_two.'</a>';
				}
			$output .= '</div>';

		// 	$output .= '</div>';
		// $output .= '</div>';
	$output .= '</div>';

	return $output;

}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" => __("Call To Join", "rplatform"),
	"base" => "rplatform_call_to_join",
	'icon' => 'icon-thm-title',
	"class" => "",
	"description" => __("Widget Call to Signup", "rplatform"),
	"category" => __('rp', "rplatform"),
	"params" => array(		

		array(
			"type" => "textfield",
			"heading" => __("Title", "rplatform"),
			"param_name" => "title",
			"value" => "",
			),

		array(
			"type" => "dropdown",
			"heading" => __("Title Alignment", "rplatform"),
			"param_name" => "title_alignment",
			"value" => array('left'=>'left','right'=>'right','center'=>'center'),
			),
		array(
			"type" => "textfield",
			"heading" => __("Font Size", "rplatform"),
			"param_name" => "size",
			"value" => "45",
			),

		array(
			"type" => "colorpicker",
			"heading" => __("Title Color", "rplatform"),
			"param_name" => "color",
			"value" => "#ed1c24",
		),
	
		array(
			"type" => "dropdown",
			"heading" => __("Title Font Wight", "rplatform"),
			"param_name" => "title_weight",
			"value" => array('400'=>'400','100'=>'100','200'=>'200','300'=>'300','500'=>'500','600'=>'600','700'=>'700'),
			),
		array(
			"type" => "dropdown",
			"heading" => __("Text Transform", "rplatform"),
			"param_name" => "text_transform",
			"value" => array('capitalize'=>'capitalize','uppercase'=>'uppercase','lowercase'=>'lowercase'),
			),
			

		array(
			"type" => "textfield",
			"heading" => __("Title Margin", "rplatform"),
			"param_name" => "title_margin",
			"value" => "0px 0px 30px 0px",
			),


		array(
			"type" => "textfield",
			"heading" => __("Title Padding", "rplatform"),
			"param_name" => "title_padding",
			"value" => "0px 0px 0px 0px",
			),

		array(
			"type" => "textfield",
			"heading" => __("Button Text One", "rplatform"),
			"param_name" => "button_text",
			"value" => "",
		),

		array(
			"type" => "textfield",
			"heading" => __("Button URL One", "rplatform"),
			"param_name" => "button_url",
			"value" => "",
		),


		array(
			"type" => "textfield",
			"heading" => __("Button Text Two", "rplatform"),
			"param_name" => "button_text_two",
			"value" => "",
		),

		array(
			"type" => "textfield",
			"heading" => __("Button URL Two", "rplatform"),
			"param_name" => "button_url_two",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => esc_html__("Button Layout", 'rplatform-core'),
			"param_name" => "btn_layout",
			"value" => array('Select'=>'','Normal'=>'normal','Black Border'=>'transparent','White Border'=>'white','Underline'=>'underline'),
		),


		array(
			"type" => "textfield",
			"heading" => __("Custom Class ", "rplatform"),
			"param_name" => "class",
			"value" => "",
			),

		)
	));
}