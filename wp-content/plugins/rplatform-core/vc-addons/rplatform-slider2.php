<?php
if(!class_exists('rplatformSlider2Shortcode'))
{
	class rplatformSlider2Shortcode
	{
		
		function __construct()
		{	
			$active_counter = 0;
			add_action('init', array($this, 'add_rplatform_slider2'));
			add_shortcode( 'rplatform_slider2_wrap', array($this, 'rplatform_slider2_wrap' ) );
			add_shortcode( 'rplatform_slider2_item', array($this, 'rplatform_slider2_item' ) );
		}

		function rplatform_slider2_wrap($atts, $content = null)
		{
			$class 				= '';
			$time 				= '';
			$disable_slider 	= '';
			extract(shortcode_atts(array(
				'class' 			=> '',
				'time' 				=> '',
				'disable_slider' 	=> '',
			), $atts));
			if($disable_slider == 'enable'){
		        $time = 'false';
		    }else {
		    	$time = 'true';
		    }
			$randId = rand(10,100);
			$output  = '<div id="home-two-crousel'.$randId.'" class="home-two-crousel carousel carousel-fade slide ' . esc_attr($class) . '">';
				$output .= '<div class="carousel-inner">';
					$output .= do_shortcode($content);
				$output .= '</div>';//carousel-inner
                // Controls
                $output .= '<a class="left carousel-control" href="#home-two-crousel'.$randId.'" role="button" data-slide="prev">';
                    $output .= '<i class="fa fa-angle-left"></i>';
                $output .= '</a>';
                $output .= '<a class="right carousel-control" href="#home-two-crousel'.$randId.'" role="button" data-slide="next">';
                    $output .= '<i class="fa fa-angle-right"></i>';
                $output .= '</a>';
			$output .= '</div>';//#home-two-crousel

			//JS time
    		$output .= "<script type='text/javascript'>jQuery(document).ready(function() { jQuery('#home-two-crousel".$randId."').carousel({ interval: ".$time." }) });</script>";
			return $output;
		}

		function rplatform_slider2_item($atts,$content = null)
		{
			$slider_type = '';
			$title = '';
			$subtitle = '';
			$image = '';
			$bgimage = '';
			$btn_url = '';
			$btn_text = '';
			extract(shortcode_atts(array(
				'slider_type'   => 'text_slide',
				'title'     	=> '',
				'subtitle'  	=> '',
				'btn_url' 		=> '',
				'btn_text' 		=> '',
				'bgimage' 		=> '',
				'image' 		=> '',
			), $atts));
			
			$style = '';
			$output = '';
			$src_imagebg   = wp_get_attachment_image_src($bgimage, 'full');
			$src_image   = wp_get_attachment_image_src($image, 'full');
			if ($slider_type == 'image_slide') {
				$style = 'style="background: #333 url('.esc_url($src_imagebg[0]).'); background-size: cover; background-repeat: no-repeat;background-position: center center;"';
			} else {
				$style = 'style="overflow: hidden;backface-visibility: hidden;"';
			}	
			
			//foreach ($title as $value) {
				global $active_counter;
				if( $active_counter == 0 ){
	            $output   .= '<div class="item active" '.$style.'>';
	            $active_counter = 2;
	            }else{
	            $output   .= '<div class="item" '.$style.'>';
	            }
					$output  .= '<div class="container">';
						$output  .= '<div class="slider-content text-center">';
							if ($src_image) {
					            $output .= '<div class="slide-icon" data-animation="animated fadeInUp">';
                $output  .= '<img src="'.esc_url($src_image[0]).'" alt="">';
	                            $output  .= '</div>';
	                        }
							if ($title) {
								$output .= '<h2 data-animation="animated fadeInLeft">' . esc_attr($title) . '</h2>';
            }
							if ($subtitle) {
								$output .= '<h3 data-animation="animated fadeInRight">' . $subtitle . '</h3>';
            }
							if ($btn_url) {
								$output .= '<a class="bordered-button" data-animation="animated fadeInUp" href="' . esc_url($btn_url) . '" target="_blank">' . esc_attr($btn_text) . '<i class="fa fa-long-arrow-right"></i></a>';
            }
						$output  .= '</div>';//slider-content
					$output  .= '</div>';//container
				$output  .= '</div>';//item
				
			return $output;
		}
		
		// Shortcode Functions for frontend editor
		function front_rplatform_slider2_wrap($atts, $content = null)
		{
			// Do nothing
			$class 				= '';
			$time 				= '';
			$disable_slider 	= '';
			extract(shortcode_atts(array(
				'class' 			=> '',
				'time' 				=> '',
				'disable_slider' 	=> '',
			), $atts));

			    if($disable_slider == 'enable'){
			        $time = 'false';
			    }	
				$output  = '<div id="home-two-crousel" class="carousel carousel-fade slide ' . esc_attr($class) . '">';
					$output .= '<div class="carousel-inner">';
								$output .= do_shortcode($content);
					$output .= '</div>';//carousel-inner

	                // Controls
	                $output .= '<a class="left carousel-control" href="#home-two-crousel" role="button" data-slide="prev">';
	                    $output .= '<i class="fa fa-angle-left"></i>';
	                $output .= '</a>';
	                $output .= '<a class="right carousel-control" href="#home-two-crousel" role="button" data-slide="next">';
	                    $output .= '<i class="fa fa-angle-right"></i>';
	                $output .= '</a>';
				$output .= '</div>';//#home-two-crousel

				// JS time
    			$output .= "<script type='text/javascript'>jQuery(document).ready(function() { jQuery('#home-one-crousel".$randId."').carousel({ interval: ".$time." }) });</script>";

			return $output;
		}
		function front_rplatform_slider2_item($atts,$content = null)
		{
			// Do nothing
			$title = '';
			$subtitle = '';
			$image = '';
			$btn_url = '';
			$btn_text = '';
			extract(shortcode_atts(array(
				'title'     => '',
				'subtitle'  => '',
				'btn_url' 	=> '',
				'btn_text' 	=> '',
				'image' 	=> '',
			), $atts));
			
			$style = '';
			$src_image   = wp_get_attachment_image_src($image, 'full');
			$style = 'style="background: #333 url('.esc_url($src_image[0]).') no-repeat center center cover; overflow: hidden;backface-visibility: hidden;padding: 154px 0 145px;"';

			$output = '<li class="icon_list_item">';
			$output   = '<div class="item" '.$style.'>';
				$output  .= '<div class="slider-content">';
						if ($title) {
							$output .= '<h2  data-animation="animated fadeInLeft">' . esc_attr($title) . '</h2>';
            }
						if ($subtitle) {
							$output .= '<h3 data-animation="animated fadeInRight">' . esc_attr($subtitle) . '</h3>';
            }
						if ($btn_url) {
							$output .= '<a href="' . esc_url($btn_url) . '" class="bordered-button" data-animation="animated fadeInUp">' . esc_attr($btn_text) . '<i class="fa fa-long-arrow-right"></i></a>';
            }
				$output  .= '</div>';//slider-content
				$output .= wpb_js_remove_wpautop($content, true);
			$output  .= '</div>';//item

			return $output;
		}
		function add_rplatform_slider2()
		{
			if(function_exists('vc_map'))
			{
				vc_map(
				array(
				   "name" => __("rplatform Slider 2","rplatform-core"),
				   "base" => "rplatform_slider2_wrap",
				   "class" => "",
				   "icon" => "icon-slider-wrap",
				   "category" => "rp",
				   "as_parent" => array('only' => 'rplatform_slider2_item'),
				   "description" => __("Text blocks connected together in one list.","rplatform-core"),
				   "content_element" => true,
				   "show_settings_on_create" => true,
				   "params" => array(
						array(
							"type" => "textfield",
							"class" => "",
							"heading" => __("Add Custom Class","rplatform-core"),
							"param_name" => "class",
							"description" => __("Add Custom Class","rplatform-core")
						),	

			            array(
			                "type" => "checkbox",
			                "class" => "",
			                "heading" => esc_html__("Disable Auto Slide: ","rplatform-core"),
			                "param_name" => "disable_slider",
			                "value" => array ( esc_html__('Disable','rplatform-core') => 'enable'),
			                "description" => esc_html__("If you want disable slide check this.","rplatform-core"),
			            ),

			            array(
			                "type" => "textfield",
			                "heading" => esc_html__("Sliding Time(Milliseconds Ex: 4000)", "rplatform-core"),
			                "param_name" => "time",
			                "value" => "3000",
			                ),

					),
					"js_view" => 'VcColumnView'
				));
				// Add slider Item
				vc_map(
					array(
					   "name" => __("rplatform Slider2 Item","rplatform-core"),
					   "base" => "rplatform_slider2_item",
					   "class" => "",
					   "icon" => "icon-slider-list",
					   "category" => "rp",
					   "content_element" => true,
					   "as_child" => array('only' => 'rplatform_slider2_wrap'),
					   "is_container"    => false,
					   "params" => array(
					   		array(
								"type" => "dropdown",
								"heading" => esc_html__("Slider Type", 'rplatform-core'),
								"param_name" => "slider_type",
								"value" => array('Select'=>'','Only Text Slide'=>'text_slide','Slide With Image '=>'image_slide'),
							),	
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Title","rplatform-core"),
								"param_name" => "title",
								"description" => __("Title","rplatform-core")
							),
							array(
								"type" => "textarea",
								"class" => "",
								"heading" => __("Sub Title","rplatform-core"),
								"param_name" => "subtitle",
								"description" => __("Sub Title","rplatform-core")
							),	
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Add Button URL","rplatform-core"),
								"param_name" => "btn_url",
								"description" => __("Add URL","rplatform-core")
							),							
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Add Button Text","rplatform-core"),
								"param_name" => "btn_text",
							),														
							array(
								"type" => "attach_image",
								"heading" => esc_html__("Upload Icon Image", 'rplatform-core'),
								"param_name" => "image",
								"value" => "",
							),							
							array(
								"type" => "attach_image",
								"heading" => esc_html__("Upload Slider Background Image", 'rplatform-core'),
								"param_name" => "bgimage",
								"value" => "",
								"dependency" => Array("element" => "slider_type", "value" => array("image_slide")),
							),	
					   )
					) 
				);
			}//endif
		}
	}
}
global $rplatformSlider2Shortcode;
if(class_exists('WPBakeryShortCodesContainer'))
{
	class WPBakeryShortCode_rplatform_slider2_wrap extends WPBakeryShortCodesContainer {
        function content( $atts, $content = null ) {
            global $rplatformSlider2Shortcode;
            return $rplatformSlider2Shortcode->front_rplatform_slider2_wrap($atts, $content);
        }
	}
	class WPBakeryShortCode_rplatform_slider2_item extends WPBakeryShortCode {
        function content($atts, $content = null ) {
            global $rplatformSlider2Shortcode;
            return $rplatformSlider2Shortcode->front_rplatform_slider2_item($atts, $content);
        }
	}
}
if(class_exists('rplatformSlider2Shortcode'))
{
	$rplatformSlider2Shortcode = new rplatformSlider2Shortcode;
}