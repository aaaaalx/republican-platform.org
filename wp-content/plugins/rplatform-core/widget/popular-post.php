<?php

add_action('widgets_init','register_rplatform_popular_post_widget');

function register_rplatform_popular_post_widget()
{
	register_widget('rplatform_popular_post_widget');
}

class rplatform_popular_post_widget extends WP_Widget{

	function __construct()
	{
		parent::__construct( 'rplatform_popular_post_widget','rplatform Popular Post',array('description' => 'rplatform post widget to display Popular Post'));
	}


	function widget($args, $instance)
	{
		extract($args);

		$title 	= apply_filters('widget_title', $instance['title'] );
		$count 	= esc_attr($instance['count']);

		$popular_post = new WP_Query( 
			array(
				'post_type' 	=> 'post', 
				'posts_per_page'=> $count, 
				'meta_key' 		=> '_post_views_count' ,
				'orderby' 		=> 'meta_value_num', 
				'order' 		=> 'DESC'  
			) );


		echo $before_widget;
       	if ( $title ) {
			echo $before_title . $title . $after_title;
		}
		// The Loop
		if ( $popular_post->have_posts() ) {
			while ( $popular_post->have_posts() ) {
				$popular_post->the_post();

				echo '<article class="widget-post">';
    				echo '<h5>'.get_the_date().'</h5>';
    				echo '<a href="'.get_the_permalink().'">'. get_the_title() .'</a>';
				echo '</article>';

			}
		} else {
			echo 'No post found';
		}

		echo $after_widget;

		/* Restore original Post Data */
		wp_reset_postdata();
		
	}


	function update( $new_instance, $old_instance )
	{
		$instance = $old_instance;

		$instance['title'] 	= strip_tags( $new_instance['title'] );
		$instance['count'] 	= strip_tags( $new_instance['count'] );

		return $instance;
	}


	function form($instance)
	{
		$defaults = array( 
			'title' 	=> 'Popular Post',
			'count' 	=> 5
		);
		$instance = wp_parse_args( (array) $instance, $defaults );
	?>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>"><?php _e('Widget Title', 'rplatform'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" style="width:100%;" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'count' )); ?>"><?php _e('Count', 'rplatform'); ?></label>
			<input id="<?php echo esc_attr($this->get_field_id( 'count' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'count' )); ?>" value="<?php echo esc_attr($instance['count']); ?>" style="width:100%;" />
		</p>

	<?php
	}
}