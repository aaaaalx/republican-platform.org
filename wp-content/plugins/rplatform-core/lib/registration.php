<?php
global $rplatform_options;
ob_start();
function custom_registration_function() {

    $username = $password = $email = $website = $first_name = $last_name = $nickname = $bio = '';

        if (isset($_POST['submit'])) {
            registration_validation(
                sanitize_user($_POST['username']),
                esc_attr($_POST['password']),
                sanitize_email($_POST['email']),
                esc_url($_POST['website']),
                sanitize_text_field($_POST['fname']),
                sanitize_text_field($_POST['lname']),
                sanitize_text_field($_POST['nickname']),
                esc_textarea($_POST['bio'])
            );
            
            // sanitize user form input
            global $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
            $username   =   sanitize_user($_POST['username']);
            $password   =   esc_attr($_POST['password']);
            $email      =   sanitize_email($_POST['email']);
            $website    =   esc_url($_POST['website']);
            $first_name =   sanitize_text_field($_POST['fname']);
            $last_name  =   sanitize_text_field($_POST['lname']);
            $nickname   =   sanitize_text_field($_POST['nickname']);
            $bio        =   esc_textarea($_POST['bio']);

            // only when no WP_error is found
            complete_registration(
            $username,
            $password,
            $email,
            $website,
            $first_name,
            $last_name,
            $nickname,
            $bio
            );
        }

        registration_form(
            $username,
            $password,
            $email,
            $website,
            $first_name,
            $last_name,
            $nickname,
            $bio
        );
    }



    function registration_form( $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio ) {
        global $rplatform_options;

        $output = '';

        $output .= '<style>
            .center-image{margin: 0 auto 35px;} .alert{padding-left: 10px; margin-bottom: 5px; margin-top: 5px; }
        </style>';
        $output .= '<div class="col-sm-4 col-sm-offset-4 text-center">';
            $output .= '<form action="' . esc_url($_SERVER['REQUEST_URI']) . '" method="post" class="rplatform-register">';
                $output .= '<a href="'.esc_url(get_site_url()).'">';
                    if ( isset($rplatform_options['registration-logo']) && $rplatform_options['registration-logo'] ){
                        if(!empty($rplatform_options['registration-logo']['url'])) {
                            $output .= '<div class="rplatform-reg"><img class="img-responsive center-image" src="'.esc_url($rplatform_options['registration-logo']['url']).'" alt="" width="180">
                                </div>';
                        } else {
                            $output .= '';
                        }       
                    } else {
                        $output .= '';
                    }
 
                $output .= '</a>';
                $output .= '<p class="lead">'.__("Register New Account","rplatform-core").'</p>';
                $output .= '<div class="form-group">
                    <input type="text" autocomplete="off" class="required form-control"  placeholder="'.__("Username *","rplatform-core").'" name="username" value="' . (isset($_POST['username']) ? $username : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <input type="password" class="required form-control"  placeholder="'.__("Password *","rplatform-core").'" name="password" value="' . (isset($_POST['password']) ? $password : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <input type="text" autocomplete="off" class="required form-control" placeholder="'.__("Email *","rplatform-core").'" name="email" value="' . (isset($_POST['email']) ? $email : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <input type="text" autocomplete="off" class="form-control" placeholder="'.__("Website","rplatform-core").'" name="website" value="' . (isset($_POST['website']) ? $website : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <input type="text" autocomplete="off" class="form-control" placeholder="'.__("First Name","rplatform-core").'" name="fname" value="' . (isset($_POST['fname']) ? $first_name : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <input type="text" autocomplete="off" class="form-control" placeholder="'.__("Last Name","rplatform-core").'" name="lname" value="' . (isset($_POST['lname']) ? $last_name : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <input type="text" autocomplete="off" class="form-control" placeholder="'.__("Nickname","rplatform-core").'" name="nickname" value="' . (isset($_POST['nickname']) ? $nickname : null) . '">
                </div>';
                $output .= '<div class="form-group">
                    <textarea autocomplete="off" style="resize:none" class="form-control" placeholder="'.__("About / Bio","rplatform-core").'" name="bio">' . (isset($_POST['bio']) ? $bio : null) . '</textarea>
                </div>';
                $output .= '<div class="form-group">
                    <input type="submit" class="btn-lg btn-block rplatform-register-btn" name="submit" value="'.__("Register","rplatform-core").'"/>
                </div>';
            $output .= '</form>';
        $output .= '</div>';
        echo $output;
    }



    function registration_validation( $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio )  {
        global $reg_errors;
        $reg_errors = new WP_Error;

        if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
            $reg_errors->add('field', __('Required form field is missing','rplatform-core'));
        }

        if ( strlen( $username ) < 4 ) {
            $reg_errors->add('username_length', __('Username too short. At least 4 characters is required','rplatform-core'));
        }

        if ( username_exists( $username ) )
            $reg_errors->add('user_name', __('Sorry, that username already exists!','rplatform-core'));

        if ( !validate_username( $username ) ) {
            $reg_errors->add('username_invalid', __('Sorry, the username you entered is not valid','rplatform-core'));
        }

        if ( strlen( $password ) < 5 ) {
            $reg_errors->add('password', __('Password length must be greater than 5','rplatform-core'));
        }

        if ( !is_email( $email ) ) {
            $reg_errors->add('email_invalid', __('Email is not valid','rplatform-core'));
        }

        if ( email_exists( $email ) ) {
            $reg_errors->add('email', __('Email Already in use','rplatform-core'));
        }
        
        if ( !empty( $website ) ) {
            if ( !filter_var($website, FILTER_VALIDATE_URL) ) {
                $reg_errors->add('website', __('Website is not a valid URL','rplatform-core'));
            }
        }

        if ( is_wp_error( $reg_errors ) ) {

            foreach ( $reg_errors->get_error_messages() as $error ) {
                echo '<div class="col-sm-4 col-sm-offset-4 text-center"><div class="alert alert-danger" role="alert"><strong>'.__('ERROR','rplatform-core').'</strong>:'.$error.'</div></div>';
            }
        }
    }




function complete_registration() {
    global $reg_errors, $username, $password, $email, $website, $first_name, $last_name, $nickname, $bio;
    if ( count($reg_errors->get_error_messages()) < 1 ) {
        $userdata = array(
        'user_login'    =>  $username,
        'user_email'    =>  $email,
        'user_pass'     =>  $password,
        'user_url'      =>  $website,
        'first_name'    =>  $first_name,
        'last_name'     =>  $last_name,
        'nickname'      =>  $nickname,
        'description'   =>  $bio,
        );
        $user = wp_insert_user( $userdata );
        echo '<div class="col-sm-4 col-sm-offset-4 text-center"><div class="alert alert-success" role="alert">'.__("Registration complete.","rplatform-core").'</div></div>';
    }
}



// Register a new shortcode: [custom_registration]
add_shortcode('custom_registration', 'custom_registration_shortcode');

// The callback function that will replace [book]
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
