<?php

/**
  ReduxFramework Sample Config File
  For full documentation, please visit: https://docs.reduxframework.com
 * */

if (!class_exists('Redux_Framework_sample_config')) {

    class Redux_Framework_sample_config {

        public $args        = array();
        public $sections    = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }

            // This is needed. Bah WordPress bugs.  ;)
            if (  true == Redux_Helpers::isTheme(__FILE__) ) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }

        }

        public function initSettings() {

            // Just for demo purposes. Not needed per say.
            $this->theme = wp_get_theme();

            // Set the default arguments
            $this->setArguments();

            // Set a few help tabs so you can see how it's done
            $this->setHelpTabs();

            // Create the sections and fields
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );
            
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);
            
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        /**

          This is a test function that will let you see when the compiler hook occurs.
          It only runs if a field	set with compiler=>true is changed.

         * */
        function compiler_action($options, $css, $changed_values) {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
            //print_r($options); //Option values
            //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )

            /*
              if( $wp_filesystem ) {
                $wp_filesystem->put_contents(
                    $filename,
                    $css,
                    FS_CHMOD_FILE // predefined mode settings for WP files
                );
              }
             */
        }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        function dynamic_section($sections) {
            //$sections = array();
            $sections[] = array(
                'title' => esc_html__('Section via hook', 'rplatform-core'),
                'desc' => esc_html__('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'rplatform-core'),
                'icon' => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array()
            );

            return $sections;
        }

        /**

          Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        function change_arguments($args) {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        function change_defaults($defaults) {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
            $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
            $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
            $sample_patterns        = array();

            if (is_dir($sample_patterns_path)) :

                if ($sample_patterns_dir = opendir($sample_patterns_path)) :
                    $sample_patterns = array();

                    while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

                        if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                            $name = explode('.', $sample_patterns_file);
                            $name = str_replace('.' . end($name), '', $sample_patterns_file);
                            $sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                        }
                    }
                endif;
            endif;

            ob_start();

            $ct             = wp_get_theme();
            $this->theme    = $ct;
            $item_name      = $this->theme->get('Name');
            $tags           = $this->theme->Tags;
            $screenshot     = $this->theme->get_screenshot();
            $class          = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(esc_html__('Customize &#8220;%s&#8221;', 'rplatform-core'), $this->theme->display('Name'));
            
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
            <?php if ($screenshot) : ?>
                <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo esc_url(wp_customize_url()); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview','rp'); ?>" />
                        </a>
                <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_html_e('Current theme preview','rp'); ?>" />
                <?php endif; ?>

                <h4><?php echo esc_html($this->theme->display('Name')); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(esc_html__('By %s', 'rplatform-core'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(esc_html__('Version %s', 'rplatform-core'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . esc_html__('Tags', 'rplatform-core') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
            <?php
            if ($this->theme->parent()) {
                printf(' <p class="howto">' . esc_html__('This child theme requires its parent theme','rp') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'rp'), $this->theme->parent()->display('Name'));
            }
            ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            // ACTUAL DECLARATION OF SECTIONS

            /**********************************
             ********* Custom settings ***********
             ***********************************/
            $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
            $languages = array_reverse($languages);
            if(count($languages)){
                add_filter('pre_option_blogname', 'multilang_title');
                function multilang_title($title){
                    $fieldID = currentLang()?'site-title-'.currentLang():false;
                    if($titleMultilang = trim(rp_options($fieldID))){
                        return $titleMultilang;
                    }
                    return $title;
                }
                add_filter('pre_option_blogdescription', 'multilang_description');
                function multilang_description($description){
                    $fieldID = currentLang()?'site-description-'.currentLang():false;
                    if($descriptionMultilang = trim(rp_options($fieldID))){
                        return $descriptionMultilang;
                    }
                    return $description;
                }
                foreach ($languages as $lang => $value){
                    $fields = [
                        [
                            'id'        => 'site-title-'.$lang,
                            'type'      => 'text',
                            'title'     => esc_html__('Site title', 'rplatform-core'),
                            'default'   => ''
                        ],
                        [
                            'id'        => 'site-description-'.$lang,
                            'type'      => 'text',
                            'title'     => esc_html__('Site description', 'rplatform-core'),
                            'default'   => ''
                        ],
                        [
                            'id'        => 'share-logo-image-'.$lang,
                            'type'      => 'media',
                            'title'     => esc_html__('Share Image', 'rplatform-core'),
                            'subtitle'  => esc_html__('Add Image Shown as Share Icon', 'rplatform-core'),
                            'default'   => ''
                        ],
                        [
                            'id'        => 'logo-'.$lang,
                            'type'      => 'media',
                            'title'     => esc_html__('Logo', 'rplatform-core'),
                            'subtitle'  => esc_html__('Logo image', 'rplatform-core'),
                            'default'   => ''
                        ],
                        [
                            'id'        => 'photo-widget-text-'.$lang,
                            'type'      => 'textarea',
                            'title'     => esc_html__('Photo Text', 'rplatform-core'),
                            'subtitle'  => esc_html__('Photo Widget Text', 'rplatform-core'),
                            'default'   => ''
                        ],
                        [
                            'id'        => 'video-widget-text-'.$lang,
                            'type'      => 'textarea',
                            'title'     => esc_html__('Video Text', 'rplatform-core'),
                            'subtitle'  => esc_html__('Video Widget Text', 'rplatform-core'),
                            'default'   => ''
                        ],
                        [
                            'id'        => 'copyright-text-'.$lang,
                            'type'      => 'editor',
                            'title'     => esc_html__('Copyright Text', 'rplatform-core'),
                            'subtitle'  => esc_html__('Add Copyright Text', 'rplatform-core'),
                            'default'   => ''
                        ]
                    ];

                    $this->sections[] = [
                        'title'     => esc_html__('Custom settings', 'Home Setting')." ($lang)",
                        'icon'      => 'el-icon-bookmark',
                        'icon_class' => 'el-icon-large',
                        'fields'    => $fields
                    ];
                }
            }
            else{
                $fields = [
                    [
                        'id'        => 'share-logo-image',
                        'type'      => 'media',
                        'title'     => esc_html__('Share Image', 'rplatform-core'),
                        'subtitle'  => esc_html__('Add Image Shown as Share Icon', 'rplatform-core'),
                        'default'   => ''
                    ],
                    [
                        'id'        => 'photo-widget-text',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Photo Text', 'rplatform-core'),
                        'subtitle'  => esc_html__('Photo Widget Text', 'rplatform-core'),
                        'default'   => ''
                    ],
                    [
                        'id'        => 'video-widget-text',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Video Text', 'rplatform-core'),
                        'subtitle'  => esc_html__('Video Widget Text', 'rplatform-core'),
                        'default'   => ''
                    ],
                ];

                $this->sections[] = [
                    'title'     => esc_html__('Custom settings', 'Home Setting'),
                    'icon'      => 'el-icon-bookmark',
                    'icon_class' => 'el-icon-large',
                    'fields'    => $fields
                ];
            }


            /**********************************
            ********* General ***********
            ***********************************/
            $this->sections[] = array(
                'title'     => esc_html__('Topbar', 'Home Setting'),
                'icon'      => 'el-icon-bookmark',
                'icon_class' => 'el-icon-large',
                'fields'    => array(

                    [
                        'id'        => 'payment_liqpay_public_key',
                        'type'      => 'text',
                        'title'     => esc_html__('Liqpay public key', 'rplatform-core'),
                        'default'   => ''
                    ],
                    [
                        'id'        => 'payment_liqpay_private_key',
                        'type'      => 'textarea',
                        'title'     => esc_html__('Liqpay private key', 'rplatform-core'),
                        'default'   => ''
                    ],
 
                    array(
                        'id'        => 'topbar-en',
                        'type'      => 'switch',
                        'title'     => __('Topbar Enable/Disable', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Topbar', 'rplatform-core'),
                        'default'   => false,
                    ),  

                    array(
                        'id'        => 'topbar-social',
                        'type'      => 'switch',
                        'title'     => __('Topbar With Social', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Topbar Social', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('topbar-en', "=", 1),
                    ), 

                    array(
                        'id'        => 'topbar-login',
                        'type'      => 'switch',
                        'title'     => __('Topbar With Login', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Topbar Login', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('topbar-en', "=", 1),
                    ),                     

                    array(
                        'id'        => 'topbar-cart',
                        'type'      => 'switch',
                        'title'     => __('Topbar With Cart', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Topbar Cart', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('topbar-en', "=", 1),
                    ),                     

                    array(
                        'id'        => 'topbar-search',
                        'type'      => 'switch',
                        'title'     => __('Topbar With Search', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Topbar Search', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('topbar-en', "=", 1),
                    ),


                    # Topbar padding section start.

                    array(
                        'id'        => 'topbar-bg',
                        'type'      => 'color',
                        'title'     => esc_html__('Topbar Background Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a link color (default: #DEDED5).', 'rplatform-core'),
                        'default'   => '#DEDED5',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ), 

                    array(
                        'id'        => 'topbar-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Text Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a link color (default: #383847).', 'rplatform-core'),
                        'default'   => '',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ),
                    array(
                        'id'        => 'topbar-link-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Link Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a link color (default: #a3a39d).', 'rplatform-core'),
                        'default'   => '#a3a39d',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ),                                                                                          
                    array(
                        'id'        => 'topbar-padding-top',
                        'type'      => 'text',
                        'title'     => __('Topbar Top Padding', 'rplatform-core'),
                        'subtitle' => __('Enter custom topbar top padding', 'rplatform-core'),
                        'default'   => '10',

                    ),  

                    array(
                        'id'        => 'topbar-padding-bottom',
                        'type'      => 'text',
                        'title'     => __('Topbar Bottom Padding', 'rplatform-core'),
                        'subtitle' => __('Enter custom topbar bottom padding', 'rplatform-core'),
                        'default'   => '10',
                    ),

                    # Topbar pading section end
                                                                           

                )
            );


            /**********************************
            ********* Header Setting ***********
            ***********************************/
            $this->sections[] = array(
                'title'     => esc_html__('Header', 'Home Setting'),
                'icon'      => 'el-icon-bookmark',
                'icon_class' => 'el-icon-large',
                'fields'    => array(

                    array(
                        'id'        => 'preloader-en',
                        'type'      => 'switch',
                        'title'     => __('Preloader', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Preloader', 'rplatform-core'),
                        'default'   => true,
                    ), 

                    array(
                        'id'       => 'menu-style',
                        'type'     => 'select',
                        'title'    => esc_html__('Select Nav Style', 'rplatform-core'),
                        'subtitle' => esc_html__('Select Nav Style', 'rplatform-core'),
                        'options'  => array(
                            'menulogocenterwithsc'  => 'Logo center with Social/search',
                            'menulogocenter'        => 'Logo center',
                            'menuwithsearch'        => 'Left Logo with Search/cart', 
                            'menuclassic'           => 'Classic nav',    
                        ),
                        'default'  => 'menulogocenter',
                    ), 

                    array(
                        'id'        => 'menu-login',
                        'type'      => 'switch',
                        'title'     => __('Menu With Login', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Login', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menulogocenterwithsc"),
                    ),                     

                    array(
                        'id'        => 'menu-cart',
                        'type'      => 'switch',
                        'title'     => __('Menu With Cart', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Cart', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menulogocenterwithsc"),
                    ),                     

                    array(
                        'id'        => 'menu-search',
                        'type'      => 'switch',
                        'title'     => __('Menu With Search', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Search', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menulogocenterwithsc"),
                    ), 

                    array(
                        'id'        => 'menu2-login',
                        'type'      => 'switch',
                        'title'     => __('Menu With Login', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Login', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menuwithsearch"),
                    ),                     

                    array(
                        'id'        => 'menu2-cart',
                        'type'      => 'switch',
                        'title'     => __('Menu With Cart', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Cart', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menuwithsearch"),
                    ),                     

                    array(
                        'id'        => 'menu2-search',
                        'type'      => 'switch',
                        'title'     => __('Menu With Search', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Search', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menuwithsearch"),
                    ), 

                    array(
                        'id'        => 'menu3-search',
                        'type'      => 'switch',
                        'title'     => __('Menu With Search', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Search', 'rplatform-core'),
                        'default'   => true,
                        'required'  => array('menu-style', "=", "menuclassic"),
                    ),                     

                    array(
                        'id'        => 'header-padding-top',
                        'type'      => 'text',
                        'title'     => esc_html__('Header Top Padding', 'rplatform-core'),
                        'subtitle' => esc_html__('Enter custom header top padding', 'rplatform-core'),
                        'default'   => '0',

                    ),  

                    array(
                        'id'        => 'header-padding-bottom',
                        'type'      => 'text',
                        'title'     => esc_html__('Header Bottom Padding', 'rplatform-core'),
                        'subtitle' => esc_html__('Enter custom header bottom padding', 'rplatform-core'),
                        'default'   => '0',
                    ),     

                    array(
                        'id'        => 'header-height',
                        'type'      => 'text',
                        'title'     => esc_html__('Header Height ex. 66', 'rplatform-core'),
                        'subtitle' => esc_html__('Enter custom header Height', 'rplatform-core'),
                        'default'   => '66',
                    ),  

                    array(
                        'id'        => 'header-fixed',
                        'type'      => 'switch',
                        'title'     => __('Sticky Header', 'rplatform-core'),
                        'subtitle' => __('Enable or disable sicky Header', 'rplatform-core'),
                        'default'   => true,
                    ),    

                    array(
                        'id'        => 'menu-social',
                        'type'      => 'switch',
                        'title'     => __('Menu With Social', 'rplatform-core'),
                        'subtitle' => __('Enable or disable Menu Social', 'rplatform-core'),
                        'default'   => true,
                    ),                                                     
                )
            );



            /**********************************
            ********* Menu Setting ************
            ***********************************/
            $this->sections[] = array(
                'title'     => esc_html__('Menu Settings', 'Home Setting'),
                'icon'      => 'el-align-justify',
                'icon_class' => 'el-icon-large',
                'fields'    => array(

                    array(
                        'id'        => 'menu_bg',
                        'type'      => 'color',
                        'title'     => esc_html__('Menu Background Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Menu background color Defalt(#eaeae0 )', 'rplatform-core'),
                        'default'   => '#eaeae0',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ), 

                    array(
                        'id'        => 'menu_font_color',
                        'type'      => 'color',
                        'title'     => esc_html__('Menu Text Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Menu Text color Defalt(#183b6a )', 'rplatform-core'),
                        'default'   => '#183b6a',
                        'validate'  => 'color',
                    ),


                    array(
                        'id'        => 'menu-hover-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Menu Hover Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Menu hover color', 'rplatform-core'),
                        'default'   => '',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ),

                   
                    array(
                        'id'        => 'manu-padding-top',
                        'type'      => 'text',
                        'title'     => esc_html__('Menu Top Padding', 'rplatform-core'),
                        'subtitle' => esc_html__('Enter custom header top padding', 'rplatform-core'),
                        'default'   => '0',

                    ),  

                    array(
                        'id'        => 'menu-padding-bottom',
                        'type'      => 'text',
                        'title'     => esc_html__('Menu Bottom Padding', 'rplatform-core'),
                        'subtitle' => esc_html__('Enter custom header bottom padding', 'rplatform-core'),
                        'default'   => '0',
                    ),
                    # Submenu...
                    array(
                        'id'        => 'submenu_bg',
                        'type'      => 'color',
                        'title'     => esc_html__('Sub Menu Background Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Sub menu background color Defalt(#eaeae0)', 'rplatform-core'),
                        'default'   => '#eaeae0',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ), 
                    array(
                        'id'        => 'submenu-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Sub Menu text Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Sub menu text color', 'rplatform-core'),
                        'default'   => '#85857f',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ),                    
                    array(
                        'id'        => 'submenu-hover-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Sub Menu Hover Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Sub menu hover color', 'rplatform-core'),
                        'default'   => '',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ),                                                                                                          
                )
            );
                    

            /**********************************
            ********* Logo & Favicon ***********
            ***********************************/

            $this->sections[] = array(
                'title'     => esc_html__('All Logo & favicon', 'rplatform-core'),
                'icon'      => 'el-icon-leaf',
                'icon_class' => 'el-icon-large',
                'fields'    => array(

                    array( 
                        'id'        => 'favicon', 
                        'type'      => 'media',
                        'desc'      => 'upload favicon image',
                        'title'      => esc_html__('Favicon','rplatform-core'),
                        'subtitle' => esc_html__('Upload favicon image', 'rplatform-core'),
                        'default' => array( 'url' => get_template_directory_uri() .'/images/favicon.ico' ), 
                    ),                                        

                    array(
                        'id'=>'logo',
                        'url'=> false,
                        'type' => 'media', 
                        'title' => esc_html__('Logo', 'rplatform-core'),
                        'default' => array( 'url' => get_template_directory_uri() .'/images/logo.png' ),
                        'subtitle' => esc_html__('Upload your custom site logo.', 'rplatform-core'),
                    ),

                    array(
                        'id'        => 'logo-width',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Widtht', 'rplatform-core'),
                        'subtitle' => esc_html__('Logo width', 'rplatform-core'),
                        'default'   => '',
                    ), 

                    array(
                        'id'        => 'logo-height',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Height', 'rplatform-core'),
                        'subtitle' => esc_html__('Logo height', 'rplatform-core'),
                        'default'   => '',
                    ),

                    array(
                        'id'        => 'logo-text-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Text Type Logo', 'rplatform-core'),
                        'subtitle' => esc_html__('Enable or disable text type logo', 'rplatform-core'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'logo-text',
                        'type'      => 'text',
                        'title'     => esc_html__('Logo Text', 'rplatform-core'),
                        'subtitle' => esc_html__('Use your Custom logo text Ex. Moview', 'rplatform-core'),
                        'default'   => 'rplatform-core',
                        'required'  => array('logo-text-en', "=", 1),
                    ), 

                    array( 
                        'id'        => 'errorpage', 
                        'type'      => 'media',
                        'desc'      => 'upload 404 Page Logo',
                        'title'      => esc_html__('404 Page Logo','rplatform-core'),
                        'subtitle' => esc_html__('Upload 404 Page Logo', 'rplatform-core'),
                        'default' => array( 'url' => get_template_directory_uri() .'/images/404.png' ), 
                    ),
                    
                    array( 
                        'id'        => 'comingsoon-logo', 
                        'type'      => 'media',
                        'desc'      => 'Upload Coming Soon Page Logo',
                        'title'     => esc_html__('Coming Soon Page Logo','rplatform-core'),
                        'subtitle' => esc_html__('Upload Coming Soon Page Logo', 'rplatform-core'),
                    ),

                    array( 
                        'id'        => 'comingsoon', 
                        'type'      => 'media',
                        'desc'      => 'Upload Coming Soon Page Background',
                        'title'      => esc_html__('Coming Soon Page Background','rplatform-core'),
                        'subtitle' => esc_html__('Upload Coming Soon Page Background', 'rplatform-core'),
                    ),
                    
                )
            );


            /**********************************
            **** Default Banner  *****
            ***********************************/
            $this->sections[] = array(
                'title'     => esc_html__('Sub Title', 'rplatform-core'),
                'icon'      => 'sub-banner-icon',
                'icon_class' => 'el-icon-compass',
                'fields'    => array(
                    array( 
                        'id'        => 'blog-banner', 
                        'type'      => 'media',
                        'desc'      => 'Upload Blog Banner image',
                        'title'      => esc_html__('Blog Banner','rplatform-core'),
                        'subtitle' => esc_html__('Upload Blog Banner image', 'rplatform-core'),
                    ),  

                    array( 
                        'id'        => 'blog-subtitle-bg-color', 
                        'type'      => 'color',
                        'desc'      => 'Blog Subtitle BG Color',
                        'title'     => esc_html__('Background Color','rplatform-core'),
                        'subtitle'  => esc_html__('Blog Subtitle BG Color', 'rplatform-core'),
                        'default'   => '#191919',
                        'transparent'   =>false,

                    ),

                    # Banner title 
                    array(
                        'id'        => 'banner_title_color',
                        'type'      => 'color',
                        'title'     => esc_html__('Banner Title Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Banner title color Defalt(#e7272d)', 'rplatform-core'),
                        'default'   => '#e7272d',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ), 
                    array(
                        'id'        => 'banner_title_size',
                        'type'      => 'text',
                        'title'     => esc_html__('Banner Title Font Size', 'rplatform-core'),
                        'subtitle' => esc_html__('Banner title font size', 'rplatform-core'),
                        'default'   => '',
                    ),                                          
                    array(
                        'id'        => 'banner_subtitle_color',
                        'type'      => 'color',
                        'title'     => esc_html__('Breadcrumb Title Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Banner sub title color Defalt(#fff)', 'rplatform-core'),
                        'default'   => '#fff',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ),
                    array(
                        'id'        => 'banner_subtitle_size',
                        'type'      => 'text',
                        'title'     => esc_html__('Breadcrumb Title Font Size', 'rplatform-core'),
                        'subtitle' => esc_html__('Banner sub title font size', 'rplatform-core'),
                        'default'   => '',
                    ),
                    # end banner title
                )
            );


            /**********************************
            ********* Layout & Styling ***********
            ***********************************/

            $this->sections[] = array(
                'icon' => 'el-icon-brush',
                'icon_class' => 'el-icon-large',
                'title'     => esc_html__('Layout & Styling', 'rplatform-core'),
                'fields'    => array(

                   array(
                        'id'       => 'boxfull-en',
                        'type'     => 'select',
                        'title'    => esc_html__('Select Layout', 'rplatform-core'),
                        'subtitle' => esc_html__('Select BoxWidth of FullWidth', 'rplatform-core'),
                        // Must provide key => value pairs for select options
                        'options'  => array(
                            'boxwidth' => 'BoxWidth',
                            'fullwidth' => 'FullWidth'
                        ),
                        'default'  => 'fullwidth',
                    ), 
                   
                    array(
                        'id'        => 'box-background',
                        'type'      => 'background',
                        'output'    => array('body'),
                        'title'     => esc_html__('Body Background', 'rplatform-core'),
                        'subtitle'  => esc_html__('You can set Background color or images or patterns for site body tag', 'rplatform-core'),
                        'default'   => '#fff',
                        'transparent'   =>false,
                    ), 


                    array(
                        'id'        => 'preset',
                        'type'      => 'image_select',
                        'compiler'  => true,
                        'title'     => esc_html__('Preset Layout', 'rplatform-core'),
                        'subtitle'  => esc_html__('select any preset', 'rplatform-core'),
                        'options'   => array(
                            '1' => array('alt' => 'Preset 1',       'img' => ReduxFramework::$_url . 'assets/img/presets/preset1.png'),
                            '2' => array('alt' => 'Preset 2',       'img' => ReduxFramework::$_url . 'assets/img/presets/preset2.png'),
                            '3' => array('alt' => 'Preset 3',       'img' => ReduxFramework::$_url . 'assets/img/presets/preset3.png'),
                            '4' => array('alt' => 'Preset 4',       'img' => ReduxFramework::$_url . 'assets/img/presets/preset4.png'),
                            ),
                        'default'   => '1'
                    ),  
                    

                    array(
                        'id'        => 'custom-preset-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Select Custom Color', 'rplatform-core'),
                        'subtitle' => esc_html__('You can use unlimited color', 'rplatform-core'),
                        'default'   => true,
                        
                    ),

                     array(
                        'id'        => 'link-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Link Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a link color (default: #ed1c24).', 'rplatform-core'),
                        'default'   => '#ed1c24',
                        'validate'  => 'color',
                        'transparent'   =>false,
                        'required'  => array('custom-preset-en', "=", 1),
                    ),

                     array(
                        'id'        => 'hover-color',
                        'type'      => 'color',
                        'title'     => esc_html__('Hover Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a hover color (default: #c61017).', 'rplatform-core'),
                        'default'   => '#c61017',
                        'validate'  => 'color',
                        'transparent'   =>false,
                        'required'  => array('custom-preset-en', "=", 1),
                    ), 

                    array(
                        'id'        => 'header-bg',
                        'type'      => 'color',
                        'title'     => esc_html__('Header Background Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a background color for the header (default: #eaeae0).', 'rplatform-core'),
                        'default'   => '#eaeae0',
                        'validate'  => 'color',
                        'transparent'   =>false,
                    ), 

                    array(
                        'id'        => 'bottom-section',
                        'type'      => 'switch',
                        'title'     => esc_html__('Select Bottom Section Enable/Disable', 'rplatform-core'),
                        'subtitle' => esc_html__('You can use unlimited color', 'rplatform-core'),
                        'default'   => true,
                        
                    ),
                    array(
                        'id'       => 'bottom-column',
                        'type'     => 'select',
                        'title'    => esc_html__('Select Bottom Column', 'rplatform-core'),
                        // Must provide key => value pairs for select options
                        'options'  => array(
                            '3' => '4 Column',
                            '4' => '3 Column',
                            '6' => '2 Column',
                        ),
                        'default'  => '4',
                    ),

                    array(
                        'id'        => 'bottom-background',
                        'type'      => 'color',
                        'title'     => esc_html__('Bottom Background', 'rplatform-core'),
                        'subtitle'  => esc_html__('You can set Background color for Bottom Background (default: #06396a)', 'rplatform-core'),
                        'default'   => '#06396a',
                        'transparent'   =>false,
                    ),   

                    array(
                        'id'        => 'footer-top-border',
                        'type'      => 'color',
                        'title'     => esc_html__('Footer Top Border Background', 'rplatform-core'),
                        'default'   => '#094278',
                        'transparent'   =>false,
                    ),   

                    array(
                        'id'        => 'footer-background',
                        'type'      => 'color',
                        'title'     => esc_html__('Footer Background', 'rplatform-core'),
                        'subtitle'  => esc_html__('You can set Background color for Footer Background (default: #06396a)', 'rplatform-core'),
                        'default'   => '#06396a',
                        'transparent'   =>false,
                    ),                     

                )
            );

            /**********************************
            ********* Typography ***********
            ***********************************/

            $this->sections[] = array(
                'icon'      => 'el-icon-font',
                'icon_class' => 'el-icon-large',                
                'title'     => esc_html__('Typography', 'rplatform-core'),
                'fields'    => array(

                    array(
                        'id'            => 'body-font',
                        'type'          => 'typography',
                        'title'         => esc_html__('Body Font', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        //'font-size'     => ture,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => true,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('body'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Body Font', 'rplatform-core'),
                        'default'       => array(
                            'color'         => '#666666',
                            'font-weight'    => '400',
                            'font-family'   => 'Montserrat',
                            'google'        => true,
                            'font-size'     => '16px'),
                    ), 

                    array(
                        'id'            => 'menu-font',
                        'type'          => 'typography',
                        'title'         => esc_html__('Menu Font', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => false,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('.common-menu-wrap .nav>li>a'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Menu Font', 'rplatform-core'),
                        'default'       => array(
                            'font-weight'    => '400',
                            'font-family'   => 'Montserrat',
                            'google'        => false,
                            'font-size'     => '12px'),
                    ),


                    array(
                        'id'            => 'headings-font_h1',
                        'type'          => 'typography',
                        'title'         => esc_html__('Headings Font h1', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => true,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('h1'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Headings Font', 'rplatform-core'),
                        'default'       => array(
                            'color'         => '#000',
                            'font-weight'    => '700',
                            'font-family'   => 'Montserrat',
                            'google'        => true,
                            'font-size'     => '36px'),
                    ),                      

                    array(
                        'id'            => 'headings-font_h2',
                        'type'          => 'typography',
                        'title'         => esc_html__('Headings Font h2', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => true,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('h2'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Headings Font', 'rplatform-core'),
                        'default'       => array(
                            'color'         => '#000',
                            'font-weight'    => '700',
                            'font-family'   => 'Montserrat',
                            'google'        => true,
                            'font-size'     => '42px'),
                    ),                      

                    array(
                        'id'            => 'headings-font_h3',
                        'type'          => 'typography',
                        'title'         => esc_html__('Headings Font h3', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => true,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('h3'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Headings Font', 'moview'),
                        'default'       => array(
                            'color'         => '#000',
                            'font-weight'    => '700',
                            'font-family'   => 'Montserrat',
                            'google'        => true,
                            'font-size'     => '24px'),
                    ),                     

                    array(
                        'id'            => 'headings-font_h4',
                        'type'          => 'typography',
                        'title'         => esc_html__('Headings Font h4', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => true,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('h4'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Headings Font', 'rplatform-core'),
                        'default'       => array(
                            'color'         => '#000',
                            'font-weight'    => '700',
                            'font-family'   => 'Montserrat',
                            'google'        => true,
                            'font-size'     => '20px'),
                    ),                      

                    array(
                        'id'            => 'headings-font_h5',
                        'type'          => 'typography',
                        'title'         => esc_html__('Headings Font h5', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => true,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('h5'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Headings Font', 'rplatform-core'),
                        'default'       => array(
                            'color'         => '#000',
                            'font-weight'    => '700',
                            'font-family'   => 'Montserrat',
                            'google'        => true,
                            'font-size'     => '18px'),
                    ),  

                    array(
                        'id'            => 'other-fonts',
                        'type'          => 'typography',
                        'title'         => esc_html__('Slider Font', 'rplatform-core'),
                        'compiler'      => false,  // Use if you want to hook in your own CSS compiler
                        'google'        => true,    // Disable google fonts. Won't work if you haven't defined your google api key
                        'font-backup'   => false,    // Select a backup non-google font in addition to a google font
                        'font-style'    => true, // Includes font-style and weight. Can use font-style or font-weight to declare
                        'subsets'       => true, // Only appears if google is true and subsets not set to false
                        'font-size'     => true,
                        // 'text-align'    => false,
                        'line-height'   => false,
                        'word-spacing'  => false,  // Defaults to false
                        'letter-spacing'=> false,  // Defaults to false
                        'color'         => false,
                        'preview'       => true, // Disable the previewer
                        'all_styles'    => true,    // Enable all Google Font style/weight variations to be added to the page
                        'output'        =>array('.slider-content h2'),
                        'units'         => 'px', // Defaults to px
                        'subtitle'      => esc_html__('Select your website Headings Font', 'rplatform-core'),
                        'default'       => array(
                            'font-weight'    => '400',
                            'font-family'   => 'Grand Hotel',
                            'google'        => true,
                            'font-size'     => '70px'),
                    ),                          

                )
            );




            /**********************************
            ********* Social Media Link ***********
            ***********************************/

            $this->sections[] = array(
                'icon'      => 'el-icon-asterisk',
                'icon_class' => 'el-icon-large', 
                'title'     => esc_html__('Social Media', 'rplatform-core'),
                'fields'    => array(
                 

                    array(
                        'id'        => 'wp-facebook',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Facebook URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-twitter',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Twitter URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-google-plus',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Google Plus URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-pinterest',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Pinterest URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-youtube',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Youtube URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-linkedin',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Linkedin URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-dribbble',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Dribbble URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-behance',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Behance URL', 'rplatform-core'),
                    ), 
                    array(
                        'id'        => 'wp-flickr',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Flickr URL', 'rplatform-core'),
                    ), 
                    array(
                        'id'        => 'wp-vk',
                        'type'      => 'text',
                        'title'     => esc_html__('Add vk URL', 'rplatform-core'),
                    ),  
                    array(
                        'id'        => 'wp-skype',
                        'type'      => 'text',
                        'title'     => esc_html__('Add skype URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-instagram',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Instagram URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-envelope',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Email Address', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'wp-phone',
                        'type'      => 'text',
                        'title'     => esc_html__('Add phone number', 'rplatform-core'),
                    ),

                )
            );




            /**********************************
            ********* Coming Soon  ***********
            ***********************************/

            $this->sections[] = array(
                'icon'      => 'el-icon-time',
                'icon_class' => 'el-icon-large',                  
                'title'     => esc_html__('Coming Soon', 'rplatform-core'),
                'fields'    => array(

                    array(
                        'id'        => 'comingsoon-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Enable Coming Soon', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable or disable coming soon mode', 'rplatform-core'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'comingsoon-date',
                        'type'      => 'date',
                        'title'     => esc_html__('Coming Soon date', 'rplatform-core'),
                        'subtitle' => esc_html__('Coming Soon Date', 'rplatform-core'),
                        'default'   => esc_html__('08/30/2018', 'rplatform-core')
                        
                    ),

                    array(
                        'id'        => 'comingsoon-title',
                        'type'      => 'text',
                        'title'     => esc_html__('Title', 'rplatform-core'),
                        'subtitle' => esc_html__('Coming Soon Title', 'rplatform-core'),
                        'default'   => esc_html__("Coming Soon", 'rplatform-core')
                    ),

                    array(
                        'id'        => 'comingsoon-subtitle',
                        'type'      => 'text',
                        'title'     => esc_html__('Sub Title', 'rplatform-core'),
                        'subtitle' => esc_html__('Coming Soon Sub Title', 'rplatform-core'),
                        'default'   => esc_html__("We are working on something awesome!", 'rplatform-core')
                    ),                    

                    array(
                        'id'        => 'comingsoon-copyright',
                        'type'      => 'text',
                        'title'     => esc_html__('Coming Soon Copyright', 'rplatform-core'),
                        'subtitle' => esc_html__('Coming Soon Copyright Text', 'rplatform-core'),
                        'default'   => esc_html__("Copyright 2016 © Moview. All Rights Reserved", 'rplatform-core')
                    ),
                    array(
                        'id'        => 'comingsoon-facebook',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Facebook URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-twitter',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Twitter URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-google-plus',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Google Plus URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-pinterest',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Pinterest URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-youtube',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Youtube URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-linkedin',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Linkedin URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-dribbble',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Dribbble URL', 'rplatform-core'),
                    ),
                    array(
                        'id'        => 'comingsoon-instagram',
                        'type'      => 'text',
                        'title'     => esc_html__('Add Instagram URL', 'rplatform-core'),
                    ),

                )
            );


            /**********************************
            ********* Blog  ***********
            ***********************************/

            $this->sections[] = array(
                'icon'      => 'el-icon-edit',
                'icon_class' => 'el-icon-large',                  
                'title'     => esc_html__('Blog', 'rplatform-core'),
                'fields'    => array(

                    array(
                        'id'        => 'blog-social',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Single Page Social Share', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable or disable blog social share for single page', 'rplatform-core'),
                        'default'   => false,
                    ), 

                    array(
                        'id'        => 'twitter-username',
                        'type'      => 'text',
                        'title'     => esc_html__('Twitter Username', 'newedge'),
                        'required'  => array('blog-social', "=", 1),
                    ),

                    array(
                        'id'        => 'related-post',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Single Page Related Posts', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable or disable Related Posts for single page', 'rplatform-core'),
                        'default'   => true,
                    ),  

                    array(
                        'id'        => 'blog-view',
                        'type'      => 'switch',
                        'title'     => esc_html__('Post View Count', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable or disable Post View Count', 'rplatform-core'),
                        'default'   => false,
                    ),                 

                    array(
                        'id'        => 'blog-author',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Author', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Blog Author ex. Admin', 'rplatform-core'),
                        'default'   => false,
                    ),

                    array(
                        'id'        => 'blog-date',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Date', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Blog Date ', 'rplatform-core'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'blog-category',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Category', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable or disable blog category', 'rplatform-core'),
                        'default'   => false,
                    ),                     

                    array(
                        'id'        => 'blog-comment',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Comment', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable or disable blog Comment', 'rplatform-core'),
                        'default'   => false,
                    ), 


                    array(
                        'id'        => 'blog-tag',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Tag', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Blog Tag ', 'rplatform-core'),
                        'default'   => false,
                    ),  

                    array(
                        'id'        => 'blog-single-comment-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Single Post Comment', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Single post comment ', 'rplatform-core'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'post-nav-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Post navigation', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Post navigation ', 'rplatform-core'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'blog-continue-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Blog Readmore', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Blog Readmore', 'rplatform-core'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'blog-continue',
                        'type'      => 'text',
                        'title'     => esc_html__('Continue Reading', 'rplatform-core'),
                        'subtitle' => esc_html__('Continue Reading', 'rplatform-core'),
                        'default'   => esc_html__('Continue Reading', 'rplatform-core'),
                        'required'  => array('blog-continue-en', "=", 1),
                    ),  

                )
            );




            /* *********************************
            ************** Footer **************
            ********************************** */

            $this->sections[] = array(
                'icon'      => 'el-icon-bookmark',
                'icon_class' => 'el-icon-large', 
                'title'     => esc_html__('Footer', 'rplatform-core'),
                'fields'    => array(

                    array(
                        'id'       => 'footer-style',
                        'type'     => 'select',
                        'title'    => esc_html__('Select Footer Style', 'rplatform-core'),
                        'options'  => array(
                            'footer1'   => 'Footer Style 1',
                            'footer2'   => 'Footer Style 2'
                        ),
                        'default'  => 'footer1',
                    ),

                    array(
                        'id'        => 'footer_text_color',
                        'type'      => 'color',
                        'title'     => esc_html__('Footer text Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a link color (default: #fff).', 'rplatform-core'),
                        'default'   => '#fff',
                        'validate'  => 'color',
                    ),

                    array(
                        'id'        => 'footer_link_color',
                        'type'      => 'color',
                        'title'     => esc_html__('Footer link Color', 'rplatform-core'),
                        'subtitle'  => esc_html__('Pick a link color (default: #e32).', 'rplatform-core'),
                        'default'   => '#e32',
                        'validate'  => 'color',
                    ),

                    array(
                        'id'        => 'footer-logo',
                        'type'      => 'media',
                        'title'      => esc_html__('Upload Footer Logo','rplatform-core'),
                        'default' => array( 'url' => get_template_directory_uri() .'/images/footer-logo.png' ),
                        'required'  => array('footer-style', "=", "footer2"),
                    ),
                 
                    array(
                        'id'        => 'copyright-en',
                        'type'      => 'switch',
                        'title'     => esc_html__('Copyright', 'rplatform-core'),
                        'subtitle'  => esc_html__('Enable Copyright Text', 'rplatform-core'),
                        'default'   => true,
                    ),

                    array(
                        'id'        => 'footer-padding-top',
                        'type'      => 'text',
                        'title'     => __('Footer Padding Top', 'rplatform-core'),
                        'subtitle' => __('Enter custom footer padding top', 'rplatform-core'),
                        'default'   => '30',

                    ),  

                    array(
                        'id'        => 'footer-padding-footer',
                        'type'      => 'text',
                        'title'     => __('Footer Padding Bottom', 'rplatform-core'),
                        'subtitle' => __('Enter custom footer padding footer', 'rplatform-core'),
                        'default'   => '30',
                    ),

                    array(
                        'id'        => 'copyright-text',
                        'type'      => 'editor',
                        'title'     => esc_html__('Copyright Text', 'rplatform-core'),
                        'subtitle'  => esc_html__('Add Copyright Text', 'rplatform-core'),
                        'default'   => esc_html__('&copy; 2016 rp', 'rplatform-core'),
                        'required'  => array('copyright-en', "=", 1),
                    ), 
                )
            );



            /**********************************
            ********* Custom CSS & JS ***********
            ***********************************/
            $this->sections[] = array(
                'title'     => esc_html__('Custom CSS &amp; JS', 'rplatform-core'),
                'icon'      => 'el-icon-bookmark',
                'icon_class' => 'el-icon-large',
                'fields'    => array(

                    array(
                        'id'        => 'custom-css',
                        'type'      => 'ace_editor',
                        'mode'      => 'css',
                        'title'     => __('Custom CSS', 'rplatform-core'),
                        'subtitle' => __('Add some custom CSS', 'rplatform-core'),
                        'default'   => '',
                    ),

                    array(
                        'id'        => 'custom_js',
                        'type'      => 'ace_editor',
                        'mode'      => 'javascript',
                        'title'     => __('Custom JS', 'rplatform-core'),
                        'subtitle' => __('Add some custom CSS', 'rplatform-core'),
                        'default'   => '',
                    ),

                    array(
                        'id'        => 'google-analytics',
                        'type'      => 'textarea',
                        'title'     => __('Google Analytics Code', 'eventum'),
                        'subtitle'  => __('Paste Your Google Analytics Code Here. This code will added to the footer', 'eventum'),                                            
                    ), 




                )
            );


            /**********************************
            ********* Import / Export ***********
            ***********************************/

            $this->sections[] = array(
                'title'     => esc_html__('Import / Export', 'rplatform-core'),
                'desc'      => esc_html__('Import and Export your Theme Options settings from file, text or URL.', 'rplatform-core'),
                'icon'      => 'el-icon-refresh',
                'fields'    => array(
                    array(
                        'id'            => 'opt-import-export',
                        'type'          => 'import_export',
                        'title'         => esc_html__('Import Export','rplatform-core'),
                        'subtitle'      => 'Save and restore your Redux options',
                        'full_width'    => false,
                    ),
                ),
            ); 

        }

        public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-1',
                'title'     => esc_html__('Theme Information 1', 'rplatform-core'),
                'content'   => esc_html__('<p>This is the tab content, HTML is allowed.</p>', 'rplatform-core')
            );

            $this->args['help_tabs'][] = array(
                'id'        => 'redux-help-tab-2',
                'title'     => esc_html__('Theme Information 2', 'rplatform-core'),
                'content'   => esc_html__('<p>This is the tab content, HTML is allowed.</p>', 'rplatform-core')
            );

            // Set the help sidebar
            $this->args['help_sidebar'] = esc_html__('<p>This is the sidebar content, HTML is allowed.</p>', 'rplatform-core');
        }

        /**

          All the possible arguments for Redux.
          For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments

         * */
        public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'rplatform_options',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => esc_html__('Theme Options', 'rplatform-core'),
                'page_title'        => esc_html__('Theme Options', 'rplatform-core'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => '', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                // OPTIONAL -> Give you extra features
                'page_priority'     => null,                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => '',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => '_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                    'icon'          => 'icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color'    => 'lightgray',
                    'icon_size'     => 'normal',
                    'tip_style'     => array(
                        'color'         => 'light',
                        'shadow'        => true,
                        'rounded'       => false,
                        'style'         => '',
                    ),
                    'tip_position'  => array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect'    => array(
                        'show'          => array(
                            'effect'        => 'slide',
                            'duration'      => '500',
                            'event'         => 'mouseover',
                        ),
                        'hide'      => array(
                            'effect'    => 'slide',
                            'duration'  => '500',
                            'event'     => 'click mouseleave',
                        ),
                    ),
                )
            );
         }

    }
    
    global $reduxConfig;
    $reduxConfig = new Redux_Framework_sample_config();
}

/**
  Custom function for the callback referenced above
 */
if (!function_exists('redux_my_custom_field')):
    function redux_my_custom_field($field, $value) {
        print_r($field);
        echo '<br/>';
        print_r($value);
    }
endif;

/**
  Custom function for the callback validation referenced above
 * */
if (!function_exists('redux_validate_callback_function')):
    function redux_validate_callback_function($field, $value, $existing_value) {
        $error = false;
        $value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

        $return['value'] = $value;
        if ($error == true) {
            $return['error'] = $field;
        }
        return $return;
    }
endif;
