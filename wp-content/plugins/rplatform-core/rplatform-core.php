<?php
/*
* Plugin Name: rplatform Core
* Plugin URI: http://www.rplatform.com/item/core
* Author: rplatform
* Author URI: http://www.rplatform.com
* License - GNU/GPL V2 or Later
* Description: rplatform Core is a required plugin for this theme.
* Version: 1.7
*/
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// language
add_action( 'init', 'rplatform_core_language_load' );
function rplatform_core_language_load(){
    $plugin_dir = basename(dirname(__FILE__))."/languages/";
    load_plugin_textdomain( 'rplatform-core', false, $plugin_dir );
}

if( !function_exists("rplatform_cat_list") ){
    // List of Group
    function rplatform_cat_list( $category ){
        global $wpdb;
        $sql = $wpdb->prepare("SELECT * FROM `".$wpdb->prefix."term_taxonomy` INNER JOIN `".$wpdb->prefix."terms` ON `".$wpdb->prefix."term_taxonomy`.`term_taxonomy_id`=`".$wpdb->prefix."terms`.`term_id` AND `".$wpdb->prefix."term_taxonomy`.`taxonomy`='%s'",$category);
        $results = $wpdb->get_results( $sql );

        $cat_list = array();
        $cat_list['All'] = 'rplatformall';
        if(is_array($results)){
            foreach ($results as $value) {
                $cat_list[$value->name] = $value->slug;
            }
        }
        return $cat_list;
    }
}

if(!function_exists('rp_excerpt_max_char')):
    function rp_excerpt_max_char($charlength) {
        $excerpt = get_the_excerpt();
        $charlength++;

        if ( mb_strlen( $excerpt ) > $charlength ) {
            $subex = mb_substr( $excerpt, 0, $charlength - 5 );
            $exwords = explode( ' ', $subex );
            $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
            if ( $excut < 0 ) {
                return mb_substr( $subex, 0, $excut );
            } else {
                return $subex;
            }

        } else {
            return $excerpt;
        }
    }
endif;


// Metabox Include
include_once( 'post-type/meta_box.php' );
include_once( 'post-type/meta-box/meta-box.php' );

# photo gallery post type
include_once( 'post-type/photo-gallery.php' );
include_once( 'post-type/rp-timeline.php' );


// Redux integration
global $rplatform_options;
if ( !class_exists( 'ReduxFramework' ) ) {
    include_once( 'lib/redux/framework.php' );
    include_once( 'lib/admin-config.php' );
    include_once( 'import-functions.php' );
}
//login up
include_once( 'lib/registration.php' );

//widget
include_once( 'widget/popular-post.php' );

// New Shortcode
include_once( 'vc-addons/fontawesome-helper.php' );
include_once( 'vc-addons/currency-helper.php' );
include_once( 'vc-addons/rplatform-heading.php' );
include_once( 'vc-addons/rplatform-latest-news.php' );
include_once( 'vc-addons/rplatform-call-to-signup.php' );
include_once( 'vc-addons/rplatform-video-popup.php' );
include_once( 'vc-addons/rplatform-mission-vision.php' );
include_once( 'vc-addons/rplatform-donate.php' );
include_once( 'vc-addons/rplatform-slider2.php' );
include_once( 'vc-addons/rplatform-features.php' );
include_once( 'vc-addons/rplatform-woocommerce-product.php' );
include_once( 'vc-addons/rplatform-video-carousel.php' );
include_once( 'vc-addons/rplatform-timeline.php' );
include_once( 'vc-addons/rplatform-video-popup-timecounter.php' );
include_once('vc-addons/bcat/bcat-faq.php');
include_once('vc-addons/bcat/payment-liqpay.php');
# Home 2
include_once( 'vc-addons/rplatform-counter.php' );
include_once( 'vc-addons/rplatform-news-feed.php' );
include_once( 'vc-addons/rplatform-call-to-join.php' );
include_once( 'vc-addons/rplatform-volunteer.php' );
include_once( 'vc-addons/rplatform-resume.php' );

# single photo gallery
include_once( 'vc-addons/single-photo-gallery.php' );
include_once( 'vc-addons/rplatform-gallery-list.php' );
include_once( 'vc-addons/rp-event-counter.php' );

add_action( 'plugins_loaded', 'load_rplatform_slider' );
function load_rplatform_slider(){
    include_once( 'vc-addons/rplatform-slider.php' );
}


# Add CSS for Frontend
add_action( 'wp_enqueue_scripts', 'rplatform_core_style' );
if(!function_exists('rplatform_core_style')):
    function rplatform_core_style(){

        # CSS
        wp_enqueue_style('animate',plugins_url('assets/css/animate.css',__FILE__));
        wp_enqueue_style('reduxadmincss',plugins_url('assets/css/reduxadmincss.css',__FILE__));
        wp_enqueue_style('magnific-popup',plugins_url('assets/css/magnific-popup.css',__FILE__));
        wp_enqueue_style('rplatform-owl-carousel',plugins_url('assets/css/owl.carousel.css',__FILE__));
        wp_enqueue_style('rplatform-core',plugins_url('assets/css/rplatform-core.css',__FILE__));

        #js
        wp_enqueue_script('jquery.inview.min',plugins_url('assets/js/jquery.inview.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('jquery.countdown.min',plugins_url('assets/js/jquery.countdown.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('jquery.counterup.min',plugins_url('assets/js/jquery.counterup.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('owl-carousel-min',plugins_url('assets/js/owl.carousel.min.js',__FILE__), array('jquery')); 
        wp_enqueue_script('jquery.magnific-popup',plugins_url('assets/js/jquery.magnific-popup.min.js',__FILE__), array('jquery'));        
        wp_enqueue_script('rplatformcore-js',plugins_url('assets/js/main.js',__FILE__), array('jquery'));

    }
endif;

add_action( 'wp_enqueue_scripts', 'bcat_custom_style', 999 );
function bcat_custom_style(){
    # CSS
    wp_enqueue_style('custom',plugins_url('assets/css/custom.css',__FILE__));
    #js
    wp_enqueue_script('custom',plugins_url('assets/js/custom.js',__FILE__), ['jquery', 'rplatformcore-js', 'main'],false, true);
}

function beackend_theme_reduxadmincss()
{
    wp_enqueue_style('reduxadmincss',plugins_url('assets/css/reduxadmincss.css',__FILE__));
}
add_action( 'admin_print_styles', 'beackend_theme_reduxadmincss' );

function beackend_theme_update_notice()
{
    wp_enqueue_style('woonotice',plugins_url('assets/css/woonotice.css',__FILE__));
}
add_action( 'admin_print_styles', 'beackend_theme_update_notice' );
