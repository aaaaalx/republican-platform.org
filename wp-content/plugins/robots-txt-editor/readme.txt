=== Robots.txt Editor ===
Contributors: Processby
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=WWBYBLD8U42YN&source=url
Tags: robots.txt, robots, seo, crawler
Requires at least: 4.0
Tested up to: 5.2.2
Stable tag: 1.1.2
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Robots.txt for WordPress

== Description ==

The plugin allows you to create and edit the robots.txt file on your site.

= Features =

* Works with multisite network on Subdomains;
* An example of the correct file for WordPress;
* Works out of the box;
* Totally Free.

== Screenshots ==

1. Robots.txt settings

== Installation ==

1. Unzip the downloaded zip file.
1. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
1. Activate `Robots.txt Editor` from Plugins page

== Changelog ==

= 1.1.2 =

Release Date: Jul 27, 2019

* Add - WordPress 5.2.2 compatibility

= 1.1.1 =

Release Date: May 06, 2019

* Improved - site map detection

= 1.1 =

Release Date: May 06, 2019

* Add - View robots.txt link

= 1.0 =

Release Date: May 02, 2019

* Initial release

