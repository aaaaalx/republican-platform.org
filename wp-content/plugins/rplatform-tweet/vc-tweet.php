<?php

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Tweets", 'rplatform-tweet'),
		"base" => "rplatform_tweet",
		'icon' => 'icon-thm-tweets',
		"class" => "",
		"description" => esc_html__("Widget Tweets Show", 'rplatform-tweet'),
		"category" => esc_html__('rp', 'rplatform-tweet'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Username", "rplatform-tweet"),
				"param_name" => "username",
				"value" => "rplatform",
				),

		    array(
		        "type" => "textfield",
		        "heading" => esc_html__("Number of Post Show", "rplatform-tweet"),
		        "param_name" => "count",
		        "value" => "",
		        ),

		    array(
		          'type' => 'checkbox',
		          'heading' => esc_html__( 'Show Avatar', 'rplatform-tweet' ),
		          'param_name' => 'avatar',
		          'value' => array( esc_html__( 'avatar', 'rplatform-tweet' ) => true )
		        ),		

		    array(
		          'type' => 'checkbox',
		          'heading' => esc_html__( 'Tweet Time', 'rplatform-tweet' ),
		          'param_name' => 'tweet_time',
		          'value' => array( esc_html__( 'Tweet Time', 'rplatform-tweet' ) => true )
		        ),	

			array(
		        "type" => "textfield",
		        "heading" => esc_html__("Add Class", "rplatform-tweet"),
		        "param_name" => "add_class",
		        "value" => "",
		      ),

			)

		));
}