<?php
/*
 * Template Name: Список видео
 */
wp_enqueue_style ('theme-style', get_template_directory_uri().'/css/video/style.css');
wp_enqueue_script ('theme-style', get_template_directory_uri().'/js/load-more.js',array('jquery'));
get_header();

$params = array(
    'post_type' => 'video',
    'posts_per_page' => 8,
    'orderby' => 'id',
    'post_status' => 'publish',
);
if(isset($_GET['year-select'])){
    $params['year'] = $_GET['year-select'];
}
if(isset($_GET['media-search'])){
    $search = esc_sql($_GET['media-search']);
    $params['s'] = $search;
}
$recipes = new WP_Query($params);
$range = getPostsDatesRange('video');
set_query_var( 'search', $search );
set_query_var( 'range', $range );
?>
<section class="gallery-section" id="main">
    <?php get_template_part('lib/sub-header')?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="filter-wrap">
                    <div class="row">
                        <?php get_template_part('lib/photo_video_filter') ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="media-title">
                            <h3></h3>
                        </div>
                    </div>
                    <div class="list">
                        <?php if($recipes->have_posts()) : ?>
                            <?php while($recipes->have_posts()) : $recipes->the_post(); ?>
                                <?php get_template_part('lib/video/content_for_list')?>
                            <?php endwhile; ?>
                        <?php endif; wp_reset_query(); ?>
                    </div>

                </div>
                <div class="row" id="load_button">
                    <?php if (  $recipes->max_num_pages > 1 ) : ?>
                        <script>
                            var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
                            var true_posts = '<?php echo addslashes(serialize($recipes->query_vars)); ?>';
                            var type = 'video';
                            var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
                            var max_pages = '<?php echo $recipes->max_num_pages; ?>';
                            var year = document.getElementById("year-select").value,
                                search = document.getElementById("filter-search").value;
                        </script>
                        <div class="loadmore-wrap">
                            <div class="top-button donate btn-plain" id="true_loadmore"><?php echo __('Load more','rp'); ?></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    <?php if(isset($_GET['year-select'])) { ?>
    $(document).ready(function() {
        $("#year-select").val("<?php echo $_GET['year-select'];?>");
    });
    <?php } ?>
    $(document).on('change','#year-select, #search-input',function () {
        $('#data-filter').submit();
    });

</script>
<?php get_footer(); ?>
