<?php
/**
 * Product Loop End
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     5.0.0
 */
?>
</div>
<?php
global $woocommerce_loop;

if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );

if(isset( $woocommerce_loop['rplatform_increment'] )){
	if( $woocommerce_loop['rplatform_increment'] != 1 ){
		echo '</div>'; //row
	}
}
