<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     5.0.0
 */
?>
<div class="clearfix"></div>
<div class="products clearfix">
<?php 

global $woocommerce_loop;
$woocommerce_loop['rplatform_increment'] = 1;