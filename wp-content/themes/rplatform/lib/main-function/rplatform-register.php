<?php 
/*-------------------------------------------*
 *      rplatform Widget Registration
 *------------------------------------------*/

if(!function_exists('rp_widdget_init')):

    function rp_widdget_init()
    {

        global $rplatform_options;

        if(!isset($rplatform_options['bottom-column'])){
            $rplatform_options['bottom-column'] = 4;
        }
        $bottomcolumn = $rplatform_options['bottom-column'];

        register_sidebar(array(
                'name'          => esc_html__( 'Menu', 'rp' ),
                'id'            => 'menu',
                'description'   => esc_html__( 'Widgets in this area will be shown after menu.' , 'rp'),
            )
        );

        register_sidebar(array( 'name'          => esc_html__( 'Sidebar', 'rp' ),
                                'id'            => 'sidebar',
                                'description'   => esc_html__( 'Widgets in this area will be shown on Sidebar.', 'rp' ),
                                'before_title'  => '<h3 class="widget_title">',
                                'after_title'   => '</h3>',
                                'before_widget' => '<div id="%1$s" class="widget %2$s" >',
                                'after_widget'  => '</div>'
                    )
        );

        register_sidebar(array( 
                        'name'          => esc_html__( 'Bottom', 'rp' ),
                        'id'            => 'bottom',
                        'description'   => esc_html__( 'Widgets in this area will be shown before Footer.' , 'rp'),
                        'before_title'  => '<h3 class="widget_title">',
                        'after_title'   => '</h3>',
                        'before_widget' => '<div class="col-sm-6 col-md-'.esc_attr($bottomcolumn).' bottom-widget"><div id="%1$s" class="widget %2$s" >',
                        'after_widget'  => '</div></div>'
                        )
        );

        register_sidebar(array(
                'name'          => esc_html__( 'Bottom Menu Area', 'rp' ),
                'id'            => 'bottom-menu-area',
                'description'   => esc_html__( 'Widgets in this area will be shown after footer menu.' , 'rp'),
                'before_title'  => '',
                'after_title'   => '',
                'before_widget' => '',
                'after_widget'  => ''
            )
        );
        
        global $woocommerce;
        if($woocommerce) {
            register_sidebar(array(
                'name'          => __( 'Shop', 'rp' ),
                'id'            => 'shop',
                'description'   => __( 'Widgets in this area will be shown on Shop Sidebar.', 'rp' ),
                'before_title'  => '<div class="widget_title"><h3 class="widget_title">',
                'after_title'   => '</h3></div>',
                'before_widget' => '<div id="%1$s" class="widget %2$s" >',
                'after_widget'  => '</div>'
                )
            );
        }   

    }
    
    add_action('widgets_init','rp_widdget_init');

endif;


/*-------------------------------------------*
 *      rplatform Style
 *------------------------------------------*/

if(!function_exists('rp_style')):

    function rp_style(){

        wp_enqueue_media();
        wp_enqueue_style('thm-style', get_stylesheet_uri());
        wp_enqueue_script('bootstrap',rp_JS.'bootstrap.min.js',array(),false,true);       
        wp_enqueue_script('jquery.prettySocial',rp_JS.'jquery.prettySocial.min.js',array(),false,true);
        wp_enqueue_script('jquery.ajax.login',rp_JS.'ajax-login-script.js',array(),false,true);

        global $rplatform_options;
        if( isset($rplatform_options['custom-preset-en']) && $rplatform_options['custom-preset-en']==0 ) {
            wp_enqueue_style( 'rplatform-preset', get_template_directory_uri(). '/css/presets/preset' . $rplatform_options['preset'] . '.css', array(),false,'all' );
        }
        wp_enqueue_script('main',rp_JS.'main.js',array(),false,true);
        wp_enqueue_script('maskedinput', get_template_directory_uri().'/js/jquery.maskedinput.min.js', ['jquery']);
        wp_enqueue_script('simple-social-share', get_template_directory_uri().'/js/jquery.simpleSocialShare.min.js', ['jquery']);


}

    add_action('wp_enqueue_scripts','rp_style');

endif;




if(!function_exists('rp_admin_style')):

    function rp_admin_style(){
        wp_enqueue_style('custom', get_template_directory_uri().'/css/admin/custom.css');
        wp_enqueue_media();
        wp_register_script('thmpostmeta', get_template_directory_uri() .'/js/admin/post-meta.js');
        wp_enqueue_script('thmpostmeta');
        wp_enqueue_script('custom', get_template_directory_uri().'/js/admin/custom.js', ['jquery']);
    }
    add_action('admin_enqueue_scripts','rp_admin_style');

endif;


/*-------------------------------------------------------
*           Include the TGM Plugin Activation class
*-------------------------------------------------------*/

require_once( get_template_directory()  . '/lib/class-tgm-plugin-activation.php');

add_action( 'tgmpa_register', 'rp_plugins_include');

if(!function_exists('rp_plugins_include')):

    function rp_plugins_include()
    {
        $plugins = array(

                array(
                    'name'                  => 'rplatform Core',
                    'slug'                  => 'rplatform-core',
                    'source'                => 'http://demo.rplatform.com/wordpress/plugins/rp/rplatform-core.zip',
                    'required'              => true,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ), 
                array(
                    'name'                  => 'WPBakery Visual Composer',
                    'slug'                  => 'js_composer',
                    'source'                => 'http://demo.rplatform.com/wordpress/plugins/js_composer.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),
                array(
                    'name'                  => 'revslider',
                    'slug'                  => 'revslider',
                    'source'                => 'http://demo.rplatform.com/wordpress/plugins/revslider.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),                 
                array(
                    'name'                  => 'rplatform Tweet',
                    'slug'                  => 'rplatform-tweet',
                    'source'                => 'http://demo.rplatform.com/wordpress/plugins/rp/rplatform-tweet.zip',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => '',
                ),                                               
                array(
                    'name'                  => 'MailChimp for WordPress',
                    'slug'                  => 'mailchimp-for-wp',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/mailchimp-for-wp.3.1.4.zip',
                ),   
                array(
                    'name'                  => 'Woocoomerce',
                    'slug'                  => 'woocommerce',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/woocommerce.3.0.4.zip', 
                ),                                                                            
                array(
                    'name'                  => 'Widget Importer Exporter',
                    'slug'                  => 'widget-importer-exporter',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/widget-importer-exporter.1.4.5.zip',
                ),
                array(
                    'name'                  => 'Contact Form 7',
                    'slug'                  => 'contact-form-7',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/contact-form-7.4.4.zip',
                ),                
                array(
                    'name'                  => 'the events calendar',
                    'slug'                  => 'the-events-calendar',
                    'required'              => false,
                    'version'               => '',
                    'force_activation'      => false,
                    'force_deactivation'    => false,
                    'external_url'          => 'https://downloads.wordpress.org/plugin/the-events-calendar.4.1.3.zip',
                ),
            );
    $config = array(
            'domain'            => 'rp',           
            'default_path'      => '',                           
            'parent_menu_slug'  => 'themes.php',                 
            'parent_url_slug'   => 'themes.php',                
            'menu'              => 'install-required-plugins',   
            'has_notices'       => true,                         
            'is_automatic'      => false,                      
            'message'           => '',                     
            'strings'           => array(
                        'page_title'                                => esc_html__( 'Install Required Plugins', 'rp' ),
                        'menu_title'                                => esc_html__( 'Install Plugins', 'rp' ),
                        'installing'                                => esc_html__( 'Installing Plugin: %s', 'rp' ), 
                        'oops'                                      => esc_html__( 'Something went wrong with the plugin API.', 'rp'),
                        'return'                                    => esc_html__( 'Return to Required Plugins Installer', 'rp'),
                        'plugin_activated'                          => esc_html__( 'Plugin activated successfully.','rp'),
                        'complete'                                  => esc_html__( 'All plugins installed and activated successfully. %s', 'rp' ) 
                )
    );

    tgmpa( $plugins, $config );

    }

endif;