<form type="GET" id="data-filter">
<div class="col-md-6 col-sm-12">
    <select  name="year-select" id="year-select" class="filter-select blue">
        <option value="" selected><?php echo __( 'Select year', 'rp' ); ?></option>
        <?php if($range): ?>
        <?php foreach($range as $year): ?>
        <option value="<?= $year ?>"><?= $year ?></option>
        <?php endforeach; ?>
        <?php endif; ?>
    </select>
</div>
<div class="col-md-6 col-sm-12">
    <input class="filter-search orange" id="search-input" name="media-search" value="<?= $search; ?>" placeholder="<?php echo __( 'Search . . . . .', 'rp' ); ?>">
</div>
</form>
