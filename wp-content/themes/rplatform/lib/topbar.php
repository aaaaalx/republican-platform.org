<div class="topbar">
    <div class="container">
      <div class="row">

        <?php if ( rp_options('topbar-social') ) { ?>
          <div class="col-sm-12 col-md-6">
            <?php get_template_part('lib/social-icon'); ?>
          </div>
        <?php } ?>

        <div class="col-sm-12 col-md-6">
            <div class="menu-social text-right">
              <?php if ( rp_options('topbar-login') ) { ?>
                  <div class="top-align home-login">
                      <a href="#sign-in" data-toggle="modal" data-target="#sign-in"><i class="fa fa-user"></i></a>
                  </div> 
              <?php } ?>   
              <?php if ( rp_options('topbar-cart') ) {             
                  global $woocommerce;
                  if($woocommerce) { ?>               
                    <div class="top-align home-cart woocart">
                      <a href="#" class="btn-cart"></a>
                              <span id="rplatform-woo-cart" class="woo-cart" style="display:none;">
                               <i class="fa fa-shopping-basket"></i> 
                                      <?php
                                          $has_products = '';
                                          $has_products = 'cart-has-products';
                                      ?>
                                     (<span class="woo-cart-items"><span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span></span>)
                                  <?php the_widget( 'WC_Widget_Cart', 'title= ' ); ?>
                              </span>
                          
                    </div>
                  <?php } 
               } ?> 
              <?php if ( rp_options('topbar-search') ) { ?>

                  <?php icl_languages(); ?>

                  <div class="top-align home-search-field">
                      <form method="get" action="/">
                          <input type="text" value="" name="s" id="s" class="form-control" placeholder="<?= esc_html__('Search . . . . .','rp') ?>" autocomplete="off">
                          <button type="submit"><i class="fa fa-search"></i></button>
                      </form>
                  </div>
              <?php } ?> 
            </div>
        </div>
      </div><!--/.row--> 
    </div><!--/.container--> 
</div>