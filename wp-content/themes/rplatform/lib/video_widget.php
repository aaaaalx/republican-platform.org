<?php
$params = array(
    'post_type' => 'video',
    'posts_per_page' => 6,
    'orderby' => 'id',
    'post_status' => 'publish',

);
$recipes = new WP_Query($params);
?>
<?php if(count($recipes->posts)): ?>
<section class="video-section" style="display: none; background-color: #eaeae0;padding: 65px 0 40px 0;text-align: center;">
    <div class="container" >
        <div class="row">
            <div class="col-xs-12">
                <div class="rplatform-title">
                    <h2><?php echo __( 'Video', 'rp' ); ?></h2>
                    <h3 class="style-title">
                    <?php
                    $fieldID = currentLang()?'video-widget-text-'.currentLang():'video-widget-text';
                    if(rp_options($fieldID)){
                        echo balanceTags(rp_options($fieldID));
                    }
                    ?>
                    </h3>
                </div>
                <div class="rplatform-video-carosuel-2 owl-carousel owl-theme">
                    <?php if($recipes->have_posts()) : ?>
                        <?php while($recipes->have_posts()) : $recipes->the_post(); ?>

                        <?php
                        $imageUrl = wp_get_attachment_image_url(
                            get_post_meta(get_the_ID(),'photo',true),
                            'rp-medium'
                        );
                        $videoLink = get_field('youtube_link', get_the_ID(), false);
                        ?>

                        <?php if($videoLink): ?>
                            <?php
                                if(!$imageUrl){
                                    parse_str(parse_url($videoLink,PHP_URL_QUERY),$matches);
                                    $videoID = !empty($matches['v'])?$matches['v']:null;
                                    if($videoID){
                                        $imageUrl = 'https://img.youtube.com/vi/'.$videoID.'/0.jpg';
                                    }
                                }
                            ?>
                            <div class="item text-center">
                                <div class="champ-video">
                                    <img src="<?= $imageUrl ?>" alt="video">
                                    <a href="<?= $videoLink; ?>"><i class="fa fa-youtube-play"></i></a>
                                </div>
                                <div class="gallery-video-desc media-description">
                                    <span class="gallery-video-desc-date media-date orange">
                                        <i class="fa fa-calendar" aria-hidden="true"></i><?= get_the_date('j F, Y'); ?>
                                    </span>
                                    <p><?= mb_strimwidth(get_the_title(), 0, 60, '...'); ?></p>
                                    <a href="<?php the_permalink(); ?>" class="readmore"><?= __( 'Read More', 'rp' ); ?></a>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php endwhile; ?>
                    <?php endif; wp_reset_query(); ?>
                </div>
                <div class="vc_btn3-container btn-own btn-blue">
                    <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern" href="/video/" title="<?= __( 'All videos', 'rp' ); ?>"><?= __( 'All videos', 'rp' ); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

