<?php
$params = array(
    'post_type' => 'photo',
    'posts_per_page' => 4,
    'orderby' => 'id',
    'post_status' => 'publish',

);
$recipes = new WP_Query($params);
?>
<?php if(count($recipes->posts)): ?>
<section class="photo-section" style="display: none; background-color: #eaeae0; padding: 65px 0 40px 0; text-align: center;">
    <div class="container">
        <div id="content" class="site-content-home" role="main">
            <div class="vc_row wpb_row vc_row-fluid vc_custom_1559910832624">
                <div class="section-photo wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="rplatform-title">
                                <h2><?php echo __( 'Photo', 'rp' ); ?></h2>
                                <h3 class="style-title">
                                    <?php
                                    $fieldID = currentLang()?'photo-widget-text-'.currentLang():'photo-widget-text';
                                    if(rp_options($fieldID)){
                                        echo balanceTags(rp_options($fieldID));
                                    }
                                    ?>
                                </h3>
                            </div>
                            <div class="conference-area">
                                <?php if($recipes->have_posts()) : ?>
                                    <?php while($recipes->have_posts()) : $recipes->the_post(); ?>
                                        <div class="col-xs-12 col-sm-6 col-md-3 no-padding">
                                            <div class="conference-img">
                                                <div class="photo" style="margin:20px;">
                                                    <a href="<?php echo wp_get_attachment_image_url(
                                                        get_post_meta(get_the_ID(),'photo',true),
                                                        'large'
                                                    ) ?>" class="plus-icon">
                                                        <img src="<?php echo wp_get_attachment_image_url(
                                                            get_post_meta(get_the_ID(),'photo',true),
                                                            'rp-medium'
                                                        ) ?>" class="img-responsive" alt="photo"></a>
                                                </div>
                                            </div>
                                            <div class="gallery-video-desc media-description">
                                                <span class="gallery-video-desc-date media-date orange">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i><?= get_the_date('j F, Y'); ?>
                                                </span>
                                                <p><?= mb_strimwidth(get_the_title(), 0, 60, '...'); ?></p>
                                                <a href="<?php the_permalink(); ?>" class="readmore"><?= __( 'Read More', 'rp' ); ?></a>
                                            </div>
                                        </div>

                                    <?php endwhile; ?>
                                <?php endif; wp_reset_query(); ?>
                            </div>
                            <div class="vc_btn3-container btn-own btn-blue">
                                <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern" href="/photo/" title="<?php echo __( 'All photo', 'rp' ); ?>"><?php echo __( 'All photo', 'rp' ); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>