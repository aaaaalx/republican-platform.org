<section class="to_us">
    <div class="section-wrap">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="wrapper">

                        <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                            <h2 class="call-title"><?= __( 'Join our team!', 'rp' ); ?></h2>
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12 text-right buttons-block">
                            <a class="bordered-button-3 champ btn-plain" href="/to-be-volunteer/"><?= __( 'Become a volunteer', 'rp' ); ?></a>
                            <a class="bordered-button-3 donate btn-plain" href="/add-to-team/"><?= __( 'Join the party', 'rp' ); ?></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


