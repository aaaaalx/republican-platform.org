<!-- Related Posts -->
<?php $orig_post = $post; 
  global $post; 
  $tags = wp_get_post_tags($post->ID); 
  
  if ($tags):
    $tag_ids = array(); 
    foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $number_of_posts = 6; // number of posts to display
    $query = "
      SELECT ".$wpdb->posts.".*, COUNT(".$wpdb->posts.".ID) as q
      FROM ".$wpdb->posts." INNER JOIN ".$wpdb->term_relationships."
      ON (".$wpdb->posts.".ID = ".$wpdb->term_relationships.".object_id)
      WHERE ".$wpdb->posts.".ID NOT IN (".$post->ID.")
      AND ".$wpdb->term_relationships.".term_taxonomy_id IN (".implode(",",$tag_ids).")
      AND ".$wpdb->posts.".post_type = 'post'
      AND ".$wpdb->posts.".post_status = 'publish'
      GROUP BY ".$wpdb->posts.".ID
      ORDER BY q
      DESC LIMIT ".$number_of_posts."";
    $related_posts = $wpdb->get_results($query, OBJECT);
    if($related_posts): ?>
    <div class="related-posts"> 
        <h3 class="common-title"><?php esc_html_e('Related Stories','rp');?></h3>
          <div class="row">
            <?php foreach($related_posts as $post): ?>
            <?php setup_postdata($post); ?>

            <div class="col-sm-6 single-related-posts">            
              <div class="thumbnail-news media">
                  <div class="news-img pull-left">
                      <div class="news-date"><time datetime="<?php the_time( 'c' ); ?>"><?php echo date_i18n( get_option('date_format')); ?></time></div>
                      <a href="<?php the_permalink(); ?>">
                          <?php the_post_thumbnail('rp-small', array('class' => 'img-responsive')); ?>
                      </a>
                  </div>
                  <div class="small-news media-body">
                      <h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4> 
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed.</p>
                  </div>
                  <div class="clearfix"></div>
              </div>
            </div>
            <?php endforeach; ?>
      </div>
    </div>
    <?php endif;
  endif;
$post = $orig_post; 
?>