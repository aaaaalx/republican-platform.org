<div class="gal-item col-md-3 col-sm-6 col-xs-12"
     data-year="<?php echo get_the_date( 'y'); ?>"
     data-month="<?php echo get_the_date( 'F'); ?>">
    <div class="prev-wrap photo">
        <div class="img-box">
            <a href="<?php the_permalink(); ?>" ><img src="<?php echo wp_get_attachment_image_url(
                    get_post_meta(get_the_ID(),'photo',true),
                    'rp-medium'
                ) ?>" class="img-responsive" alt=""></a>
        </div>
        <div class="thumb-description">
            <div class="thumb-date"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date( 'd.m.y'); ?></div>
            <div class="short-desc"><?= mb_strimwidth(get_the_title(), 0, 60, '...'); ?></div>
            <a href="<?php echo get_permalink(); ?>" class="readmore blue"><?php echo __( 'Read More', 'rp' ); ?></a>
        </div>
    </div>
</div>