<?php
define('rp_NAME', wp_get_theme()->get( 'Name' ));
define('rp_CSS', get_template_directory_uri().'/css/');
define('rp_JS', get_template_directory_uri().'/js/');


/*-----------------------------------------------------
 * 				rp Options
*----------------------------------------------------*/
if(!function_exists('rp_options')):
	function rp_options($arg) {
		global $rplatform_options;
		if (isset($rplatform_options[$arg])) {
			return $rplatform_options[$arg];
		} else {
			return false;
		}
	}
endif;

if(!function_exists('rp_options_url')):
	function rp_options_url($arg,$arg2) {
		global $rplatform_options;
		if (isset($rplatform_options[$arg][$arg2])) {
			return $rplatform_options[$arg][$arg2];
		} else {
			return false;
		}
	}
endif;


# Shop Page Column Set
function loop_columns() {
return 3; 
}
add_filter('loop_shop_columns', 'loop_columns');


/*-------------------------------------------*
 *				Register Navigation
 *------------------------------------------*/
register_nav_menus( array(
	'primary' => esc_html__( 'Primary Menu', 'rp' ),
	'leftmenu' => esc_html__( 'Left Menu', 'rp' ),
	'menuright' => esc_html__( 'Right Menu', 'rp' ),
	'footernav' => esc_html__( 'Footer Menu', 'rp' )
) );

/*-------------------------------------------*
 *				title tag
 *------------------------------------------*/


add_action( 'after_setup_theme', 'rp_slug_setup' );
if(!function_exists( 'rp_slug_setup' )):
    function rp_slug_setup() {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-formats', array( 'link', 'quote' ) );
    }
endif;

/*-------------------------------------------*
 *				Navwalker
 *------------------------------------------*/
require_once( get_template_directory()  . '/lib/menu/admin-megamenu-walker.php');
require_once( get_template_directory()  . '/lib/menu/meagmenu-walker.php');
require_once( get_template_directory()  . '/lib/menu/mobile-navwalker.php');
add_filter( 'wp_edit_nav_menu_walker', function( $class, $menu_id ){
	return 'rplatform_Megamenu_Walker';
}, 10, 2 );


/*-------------------------------------------*
 *				rp Register
 *------------------------------------------*/
require_once( get_template_directory()  . '/lib/main-function/rplatform-register.php');


/*-------------------------------------------------------
 *			rplatform Core
 *-------------------------------------------------------*/
require_once( get_template_directory()  . '/lib/main-function/rplatform-core.php');


/*-------------------------------------------------------
 *          rplatform Login Registration
 *-------------------------------------------------------*/
require_once( get_template_directory()  . '/lib/main-function/ajax-login.php');
require_once( get_template_directory()  . '/lib/main-function/login-registration.php');


/*-------------------------------------------------------
 *          Bcat Widgets
 *-------------------------------------------------------*/
require_once get_template_directory().'/widgets/widget-contact-us.php';
require_once get_template_directory().'/widgets/widget-html.php';

/*-----------------------------------------------------
 * 				Custom Excerpt Length
 *----------------------------------------------------*/

if(!function_exists('rp_excerpt_max_charlength')):
	function rp_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				return mb_substr( $subex, 0, $excut );
			} else {
				return $subex;
			}

		} else {
			return $excerpt;
		}
	}
endif;


/*-------------------------------------------*
 *				woocommerce support
 *------------------------------------------*/
add_action( 'after_setup_theme', 'rp_woocommerce_support' );
function rp_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


/*-----------------------------------------------------
 * 				Custom body class
 *----------------------------------------------------*/
add_filter( 'body_class', 'rp_body_class' );
function rp_body_class( $classes ) {
     $menu_style = 'none';
     if ( rp_options('boxfull-en') ) {
      $layout = esc_attr(rp_options('boxfull-en'));
     }else{
        $layout = 'fullwidth';
     }
     $classes[] = $layout.'-bg'; 
	return $classes;
}

/*lang switcher*/
function icl_languages(){
    $languages = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str');
    $currentLanguage = apply_filters( 'wpml_current_language', NULL );
    if(!empty($languages)){
        $currentLanguage = $languages[$currentLanguage];
        unset($languages[$currentLanguage['tag']]);
        $languages = array_filter($languages);
        echo '<div class="custom-sel">';
        echo '<span class="selected"><img src="'.$currentLanguage['country_flag_url'].'">'.$currentLanguage['tag'].'<i class="fa fa-angle-down"></i></span>';
        echo '<div class="hidden" >';
        foreach($languages as $l){
            echo '<a href="'.$l['url'].'"><img src="'.$l['country_flag_url'].'">'.$l['tag'].'</a>';
        }
        echo '</div></div>';
    }
}

function true_load_posts(){

    $args = unserialize( stripslashes( $_POST['query'] ) );
    $args['paged'] = $_POST['page'] + 1; // следующая страница
    $args['post_status'] = 'publish';
    if(isset($_POST['year'])){
        $args['year'] = $_POST['year'];
    }
    if(isset($_POST['media-search'])){
        $args['s'] = $_POST['media-search'];
    }
    // обычно лучше использовать WP_Query, но не здесь
    query_posts( $args );
    // если посты есть
    if( have_posts() ) :
        // запускаем цикл
        while( have_posts() ): the_post();
            get_template_part( 'lib/'.$_POST['type'].'/content_for_list', get_post_format() );
        endwhile;
    endif;
    die();
}


add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');

show_admin_bar(false);

function repeatingCapitalCharactersToLowerCase($text, $pattern = '/\b\p{Lu}{3,}\b/u'){
    preg_match_all($pattern, $text, $matches);
    if(!empty($matches[0])){
        foreach ($matches[0] as $item){
            $text = str_replace($item, mb_strtolower($item), $text);
        }
    }
    return $text;
}


function currentLang(){
    return apply_filters( 'wpml_current_language', NULL );
}

function getPostsDatesRange($post_type = 'post'){
    global $wpdb;
    $result = $wpdb->get_col("
                                    SELECT year(post_date)
                                    FROM {$wpdb->posts} 
                                    WHERE post_type = '{$post_type}' 
                                    AND post_status = 'publish' 
                                    GROUP BY year(post_date)
                                    ORDER BY post_date ASC;");
    return $result;
}