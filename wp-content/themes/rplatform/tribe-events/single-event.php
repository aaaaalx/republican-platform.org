<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
wp_enqueue_style ('theme-style', get_template_directory_uri().'/css/video/style.css');
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();
$venue_details = tribe_get_venue_details();
$event_id = get_the_ID();

?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <a href="<?php echo esc_url( tribe_get_events_link() ); ?>" class="btn-return orange">
                <i class="fa fa-angle-left" aria-hidden="true"></i><?php printf( esc_html__( 'All %s', 'rp' ), $events_label_plural ); ?></a>

        </div>
        <div class="col-xs-12">
            <div class="media-wrap">
                <div class="media-caption">
                    <?php the_title( '<h3>', '</h3>' ); ?>
                    <span class="media-date orange">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <?php echo tribe_events_event_schedule_details( $event_id ); ?>

                    </span>
                    <?php if ( $venue_details ) : ?>
                    <span class="media-location blue">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                             <!-- Venue Display Info -->
                            <?php echo implode( ', ', $venue_details ); ?>
                    </span>
                    <?php endif; ?>
                </div>
                <div class="media-image">
                    <?php echo tribe_event_featured_image( $event_id, 'rp-large', false ); ?>
                </div>
                <div class="media-description">
                    <?php the_content(); ?>
                </div>
                <div>
                    <?php tribe_get_template_part( 'modules/meta' ); ?>
                </div>
            </div>
            <div class="single-page-social">
                <?php get_template_part('post-format/social-buttons')?>
            </div>
        </div>
    </div>
</div>
