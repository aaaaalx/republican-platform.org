<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
wp_enqueue_style ('theme-style', get_template_directory_uri().'/css/video/style.css');
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>

<div class="row">
    <div class="col-md-6">
        <!-- Event Image -->
        <?php echo tribe_event_featured_image( null, 'large' ) ?>
    </div>
    <div class="col-md-6">
        <!-- Event Content -->
        <?php do_action( 'tribe_events_before_the_content' ) ?>
            <div class="tribe-events-list-event-description tribe-events-content">
            <!-- Event Title -->
            <?php do_action( 'tribe_events_before_the_event_title' ) ?>
            <h3 class="tribe-events-list-event-title">
                <a class="tribe-event-url" style="color: black" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
                    <?php the_title() ?>
                </a>
            </h3>
            <?php do_action( 'tribe_events_after_the_event_title' ) ?>

            <!-- Event Meta -->
            <?php do_action( 'tribe_events_before_the_meta' ) ?>
                <div class="media-caption">
                    <span class="media-date orange">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <?php echo tribe_events_event_schedule_details( $event_id ); ?>

                    </span>
                    <?php if ( $venue_details ) : ?>
                        <span class="media-location blue">
                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                            <!-- Venue Display Info -->
                            <?php echo implode( ', ', $venue_details ); ?>
                    </span>
                    <?php endif; ?>
                </div>
            <?php do_action( 'tribe_events_after_the_meta' ) ?>
            <?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>
            <div class="event-link">
                <a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more blue" rel="bookmark"><?php esc_html_e( 'Read more', 'rp' ) ?> <i class="fa fa-long-arrow-right"></i></a>
            </div>
        </div><!-- .tribe-events-list-event-description -->
        <?php
        do_action( 'tribe_events_after_the_content' ); ?>
    </div>
</div>




