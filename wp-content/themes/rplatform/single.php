<?php get_header(); ?>

<section id="main">
    <?php get_template_part('lib/sub-header')?>
    <div class="container">
        <div class="row">
            <div id="content" class="site-content col-sm-9" role="main">
                <?php if ( have_posts() ) :  ?> 

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php get_template_part( 'post-format/content', get_post_format() ); ?>

                         <?php if ( rp_options('post-nav-en') ) { ?>
                            <div class="clearfix post-navigation">
                                <?php previous_post_link('<span class="previous-post pull-left">%link</span>',esc_html__("Previous Article",'rp')); ?>
                                <?php next_post_link('<span class="next-post pull-right">%link</span>',esc_html__("Next Article",'rp')); ?>
                            </div> <!-- .post-navigation -->
                        <?php } ?>

                        <div class="single-page-social">

                            <?php if ( rp_options('blog-author') ) { ?>
                                <?php if ( get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "" ) { ?>
                                   <a class="single-social-info user" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><i class="fa fa-user"></i><?php echo get_the_author_meta('first_name');?> <?php echo get_the_author_meta('last_name');?></a>
                                <?php } else { ?>
                                    <span class="author-meta single-social-info user"><i class="fa fa-user"></i> <?php the_author_posts_link() ?></span>
                                <?php }?>
                            <?php }?>  

                            <?php if ( rp_options('blog-social') ) { ?>
                                <?php get_template_part( 'post-format/social-buttons' ); ?>
                            <?php } ?>
                        </div>

                        <div style="margin-top: 30px;">

                            <div style="display: block; margin-bottom: 30px;">
                            <?php if ( rp_options('blog-tag') && $posttags = get_the_tags()) { ?>
                                <span class="tags-in"><?php _e('Tags: ','rp');?> <?php the_tags('', ', ', '<br />'); ?></span>
                            <?php }?>
                            </div>

                            <?php if ( rp_options('blog-comment') && is_single() ) { ?>
                                <?php if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) : ?>
                                    <span class="comments-in">
                                        <?php _e('Comments: ','rp');?>
                                        <?= get_comments_number() ; ?>
                                    </span>
                                <?php endif; //.comment-link ?>
                            <?php }?>

                            <?php
                                if ( rp_options('blog-comment') ) {
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    }
                                }
                            ?>
                        </div>

                        <?php
                            if ( is_singular( 'post' ) ){
                                $count_post = esc_attr( get_post_meta( $post->ID, '_post_views_count', true) );
                                if( $count_post == ''){
                                    $count_post = 1;
                                    add_post_meta( $post->ID, '_post_views_count', $count_post);
                                }else{
                                    $count_post = (int)$count_post + 1;
                                    update_post_meta( $post->ID, '_post_views_count', $count_post);
                                }
                            }
                        ?>
                    <?php endwhile; ?>    
                <?php else: ?>
                    <?php get_template_part( 'post-format/content', 'none' ); ?>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div> <!-- #content -->
            <?php get_sidebar(); ?>
            <!-- #sidebar -->
            </div> <!-- .row -->
        </div> <!-- .container -->
    </section> <!-- .container -->

<?php get_footer();