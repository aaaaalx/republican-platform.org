<?php 
    global $rplatform_options;


        $footer_style ='';
        if (isset( $_REQUEST['footer-demo'])) {
          $footer_style = esc_attr($_REQUEST['footer-demo']);
        }else {
          if ( rp_options('footer-style') ) { 
            $footer_style = esc_attr( rp_options('footer-style') );
          } else {
            $footer_style = 'footer1';
          }
        }
        
    ?> 

        <?php if(is_active_sidebar('bottom') && isset($rplatform_options['bottom-section'])){ ?>
            <div class="bottom-wrap">
                <?php dynamic_sidebar('bottom'); ?>
            </div>
        <?php } ?>  

    <?php if($footer_style=='footer1'){ ?>
        <!-- start footer 1 -->
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="footer-menu-area col-md-12 col-xs-12 text-center">
                        <div class="menu-area">
                        <?php if ( has_nav_menu( 'footernav' ) ) {
                            $default = array(
                                'theme_location'  => 'footernav',
                                'container'       => '',
                                'menu_class'      => 'footer-menu',
                                'menu_id'         => 'footer-menu',
                                'fallback_cb'     => 'wp_page_menu',
                                'depth'           => 0
                            );
                            wp_nav_menu($default);
                        } ?>
                        </div>
                        <div class="menu-area">
                            <?php if(is_active_sidebar('bottom-menu-area') && isset($rplatform_options['bottom-section'])){ ?>
                                    <?php dynamic_sidebar('bottom-menu-area'); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 copyright">
                        <p class="copy-right">
                            <?php
                            $fieldID = currentLang()?'copyright-text-'.currentLang():'copyright-text';
                            if(rp_options($fieldID)){
                                echo balanceTags(rp_options($fieldID));
                            }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end footer 1 -->
    <?php } ?>

    <?php if($footer_style=='footer2'){ ?>
        <!-- Footer Area Start -->
        <footer class="footer-two-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-xs-12 text-center">
                        <div class="footer-logo">
                            <?php if( rp_options_url('footer-logo','url') ){  ?>
                            <img src="<?php echo esc_url( rp_options_url('footer-logo','url') ); ?>" alt="">
                            <?php } ?>
                        </div>
                        <?php get_template_part('lib/social-link'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-md-offset-1 col-xs-12 text-center">
                        <p class="copy-right"><?php if( rp_options('copyright-en') ){  echo balanceTags( rp_options('copyright-text') ); } ?></p>
                    </div>
                    <div class="col-md-9 col-xs-12 text-left">
                        <?php if ( has_nav_menu( 'footernav' ) ) { 
                            $default = array( 
                              'theme_location'  => 'footernav',
                              'container'       => '', 
                              'menu_class'      => 'footer-menu',
                              'menu_id'         => 'footer-menu',
                              'fallback_cb'     => 'wp_page_menu',
                              'depth'           => 1
                            );
                            wp_nav_menu($default);
                        } ?>
                    </div>
                </div>
            </div>
        </footer>
    <?php } ?>
</div> <!-- #page -->

<div class="to-top"></div>

<?php if(rp_options('google-analytics')) echo rp_options('google-analytics'); ?>
<?php wp_footer(); ?>
<script type="text/javascript">
    <?php print rp_options('custom_js'); ?>
</script>
</body>
</html>