jQuery(document).ready(function($){
    $('.color_field').each(function(){
        $(this).wpColorPicker();
    });

    $(document).on('widget-updated', function(e, widget) {
        widget.find('.color_field').each(function(){
            $(this).wpColorPicker().on('change', function () {
                $(this).trigger('change');
            });
        });
    });

    function media_upload(button_selector) {
        var _custom_media = true;
        var _orig_send_attachment = wp.media.editor.send.attachment;
        $('body').on('click', button_selector, function () {
            var button_id = $(this).attr('id');
            wp.media.editor.send.attachment = function (props, attachment) {
                if (_custom_media) {
                    $('.' + button_id + '_img').attr('src', attachment.url).trigger('change');
                    $('.' + button_id + '_url').val(attachment.url);
                } else {
                    return _orig_send_attachment.apply($('#' + button_id), [props, attachment]);
                }
            };
            wp.media.editor.add($('#' + button_id),{
                multiple: false,
                library: {
                    type: 'image'
                }
            });
            wp.media.editor.open($('#' + button_id));
            return false;
        });

    }
    media_upload('.js_custom_upload_media');
    media_remove('.remove-image');
    function media_remove(button_selector) {
        $('body').on('click', button_selector, function () {
            var button_id = $(this).data('id');
            $('.' + button_id + '_url').val(null).trigger('change');
            $('.' + button_id + '_img').attr('src', null);
        });
    }


});