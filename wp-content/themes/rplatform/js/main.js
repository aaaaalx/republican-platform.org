/*global $:false */
jQuery(document).ready(function($){'use strict';
    
    // Loader
    $(window).load(function () {
        if ($(".loader-wrap").length > 0)
        {
            $(".loader-wrap").delay(500).fadeOut("slow");
        }
    });

    /* -------------------------------------- */
    //      RTL Support Visual Composer
    /* -------------------------------------- */
    var delay = 1;
    function rplatform_rtl() {
        if (jQuery("html").attr("dir") == 'rtl') {
            if (jQuery(".entry-content > div").attr("data-vc-full-width") == 'true') {
                var right = jQuery('.entry-content > div').css('right');
                jQuery('.entry-content > div').css({ 'left': 'auto', 'right': right });
            }
            var $fullwidth = $('[data-vc-full-width="true"]');
            $fullwidth.removeAttr('style').css('width': $fullwidth.width());
        }
    }
    setTimeout( rplatform_rtl , delay);

    jQuery( window ).resize(function() {
        setTimeout( rplatform_rtl , delay);
    }); 

    // Sticky Nav
    $(window).on('scroll', function(){
        if ( $(window).scrollTop() > 0 ) {
            $('#masthead').addClass('sticky');
        } else {
            $('#masthead').removeClass('sticky');
        }
    });

	// Social Share ADD
	$('.prettySocial').prettySocial();


    //title first word
    $('#sidebar .widget_title').not('#sidebar .widget_rss .widget_title').each(function() {'use strict';
      var txt = $(this).html();
      var index = txt.indexOf(' ');
      if (index == -1) {
         index = txt.length;
      }
    $(this).html('<span>' + txt.substring(0, index) + '</span>' + txt.substring(index, txt.length));
    });    

    //Woocommerce
    jQuery( ".woocart" ).hover(function() {
        jQuery(this).find('.widget_shopping_cart').stop( true, true ).fadeIn();
    }, function() {
        jQuery(this).find('.widget_shopping_cart').stop( true, true ).fadeOut();
    }); 

    jQuery('.woocart a').html( jQuery('.woo-cart').html() );

    jQuery('.add_to_cart_button').on('click',function(){'use strict';

        jQuery('.woocart a').html( jQuery('.woo-cart').html() );            

        var total = 0;
        if( jQuery('.woo-cart-items span.cart-has-products').html() != 0 ){
            if( jQuery('woocart ul.cart_list').length  > 0 ){
                for ( var i = 1; i <= jQuery('.woocart ul.cart_list').length; i++ ) {
                    var total_string = jQuery('.woocart ul.cart_list li:nth-child('+i+') .quantity').text();
                    total_string = total_string.substring(-3, total_string.length);
                    total_string = total_string.replace('×', '');
                    total_string = parseInt( total_string.trim() );
                    //alert( total_string );
                    if( !isNaN(total_string) ){ total = total_string + total; }
                }
            }
        }
        jQuery('.woo-cart-items span.cart-has-products').html( total+1 );
    }); 

    // Search onclick
    $('.hd-search-btn').on('click', function(event) { 'use strict';
        event.preventDefault();
        var $searchBox = $('.home-search');
        if ($searchBox.hasClass('show')) {
            $searchBox.removeClass('show');
            $searchBox.fadeOut('fast');
        }else{
            $searchBox.addClass('show');
            $searchBox.fadeIn('slow');
        }
    });

    $('.hd-search-btn-close').on('click', function(event) { 'use strict';
        event.preventDefault();

        var $searchBox = $('.home-search');
        $searchBox.removeClass('show');
        $searchBox.fadeOut('fast');
    }); 


    $('.cloud-zoom').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-img-mobile',
        image: {
                verticalFit: true
            },
    });



});



