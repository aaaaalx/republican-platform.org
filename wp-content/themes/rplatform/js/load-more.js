jQuery(document).ready(function($){
    'use strict';
    $('#true_loadmore').click(function(){
        var data = {
            'action': 'loadmore',
            'query': true_posts,
            'page' : current_page,
            'type' : type,
            'year': year,
            'search': search
        };
        $.ajax({
            url:ajaxurl, // обработчик
            data:data, // данные
            type:'POST', // тип запроса
            success:function(data){
                if( data ) {
                    $('.list').append(data);// вставляем новые посты
                    current_page++; // увеличиваем номер страницы на единицу
                    if (current_page == max_pages) $("#true_loadmore").remove(); // если последняя страница, удаляем кнопку
                } else {
                    $('#true_loadmore').remove(); // если мы дошли до последней страницы постов, скроем кнопку
                }
            }
        });
    });

});