<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    if(is_front_page()) {
        $fieldID = currentLang()?'share-logo-image-'.currentLang():'share-logo-image';
        if ($shareImage = rp_options($fieldID)) {
            if (!empty($shareImage['url'])) {
                echo '<meta property="og:image" content="'.$shareImage['url'].'">';
            }
        }
    }
    elseif(is_single() || (!is_front_page() && is_page())){
        if( has_post_thumbnail( get_the_ID() ) ){
            $thumb_src =  wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'rp-medium' );
            $media_url = $thumb_src[0];
        }
        elseif($_image = wp_get_attachment_image_url(get_post_meta(get_the_ID(),'photo',true), 'rp-medium')){
            $media_url = $_image;
        }
        $metaContent = esc_html(rp_excerpt_max_char(300));
        $metaContent = $metaContent?$metaContent:get_the_date( 'd.m.y');
        echo '<meta property="og:description" content="'.$metaContent.'">';

        echo '<meta property="og:image" content="'.$media_url.'">';

    }
    ?>
      <?php  if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {
          if( rp_options('favicon') ){  ?>
            <link rel="shortcut icon" href="<?php echo esc_url( rp_options_url('favicon','url') ); ?>" type="image/x-icon"/>
          <?php }else{ ?>
            <link rel="shortcut icon" href="<?php echo esc_url(get_template_directory_uri().'/images/plus.png'); ?>" type="image/x-icon"/>
          <?php }
        }
        ?>
      <link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
      <?php wp_head(); ?>
</head>

 <?php
     $menu_style = 'none';
     if ( rp_options('boxfull-en') ) {
      $layout = esc_attr( rp_options('boxfull-en') );
     }else{
        $layout = 'fullwidth';
     }
 ?>

<body <?php body_class(); ?>>  
  <?php if ( rp_options('preloader-en') ) { ?>
    <div class="loader-wrap">
        <div class="loader"><?php  esc_html_e( 'Loading...', 'rp' ); ?></div>
    </div>
  <?php }?>
  <div id="page" class="hfeed site <?php echo esc_attr($layout); ?>">
  <?php 
    $menustyle ='';
    if (isset( $_REQUEST['menu-demo'])) {
      $menustyle = esc_attr($_REQUEST['menu-demo']);
    }else {
      if ( rp_options('menu-style') ) { 
        $menustyle = esc_attr( rp_options('menu-style') );
      }
    }
  ?> 

<!--Logo center with Social/search-->
<?php if ($menustyle == 'menulogocenterwithsc') { ?>
  <header id="masthead" class="site-header header header-<?php echo $menustyle;?>">
    
    <!--Add Topbar-->
    <?php 
      if ( rp_options('topbar-en') ) {
        get_template_part('lib/topbar');
      } 
    ?>

      <!--Home Search--> 
      <div class="home-search-wrap">
        <div class="containerfluid">
            <div class="home-search">
                <?php echo get_search_form();?>
                <a href="#" class="hd-search-btn-close"><i class='fa fa-close'></i></a>
            </div>
         </div> 
      </div><!--/.home-search-wrap--> 
    
      <!--responsive header--> 
      <div class="responsive-header hidden-lg hidden-md">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-6"> 
              <?php get_template_part('lib/social-icon'); ?>
            </div>
            <div class="col-xs-6">
                <div class="menu-social text-right">
                <?php if ( rp_options('menu-login') ) { ?>

                  <?php if(is_user_logged_in()){
                        echo ' <div class="top-align home-login"><a href="'.wp_logout_url( home_url() ).'"><i class="fa fa-sign-out"></i></a></div>';
                    }else{ ?>
                      <div class="top-align home-login">
                        <a href="#sign-in" data-toggle="modal" data-target="#sign-in"><i class="fa fa-user"></i></a>
                    </div>
                  <?php } ?>

                <?php  } ?>
                  <?php if ( rp_options('menu-cart') ) {
                    global $woocommerce;
                    if($woocommerce) { ?>
                      <div class="top-align home-cart woocart">
                        <a href="<?php echo site_url('cart'); ?>" class="btn-cart"></a>
                                <div id="rplatform-woo-cart-responsive" class="woo-cart" style="display:none;">
                                 <i class="fa fa-shopping-basket"></i>
                                        <?php
                                            $has_products = '';
                                            $has_products = 'cart-has-products';
                                        ?>
                                       (<span class="woo-cart-items"><span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span></span>)
                                    <?php the_widget( 'WC_Widget_Cart', '' ); ?>
                                </div>

                      </div>
                    <?php }
                  } ?>
                <?php if ( rp_options('menu-search') ) { ?>
                  <span class="top-align home-search-btn">
                    <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                  </span>
                <?php  } ?>
                </div>
            </div>
          </div><!--/.row--> 
        </div><!--/.container--> 
      </div> <!--/.responsive-header--> 

      <!--Menu Wrap--> 
      <div class="main-menu-wrap clearfix">

        <!--Social Share--> 
        <?php if ( rp_options('menu-social') ) { ?>
          <div class="common-menu hidden-xs hidden-sm"> 
            <?php get_template_part('lib/social-icon'); ?>
          </div><!--/.common-menu-->
        <?php } ?>
        
        <!--responsive Mobile Menu--> 
        <button type="button" class="navbar-toggle tablet-responsive" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div id="mobile-menu" class="hidden-lg hidden-md">
          <div class="collapse navbar-collapse tablet-responsive-collapse">
              <?php 
                if ( has_nav_menu( 'leftmenu' ) ) {
                    wp_nav_menu( array(
                        'theme_location'      => 'leftmenu',
                        'container'           => false,
                        'menu_class'          => 'leftmenu nav navbar-nav',
                        'fallback_cb'         => 'wp_page_menu',
                        'depth'               => 4,
                        'walker'              => new wp_bootstrap_mobile_navwalker()
                        )
                    ); 
                }
                ?>
              <?php 
                if ( has_nav_menu( 'menuright' ) ) {
                    wp_nav_menu( array(
                        'theme_location'      => 'menuright',
                        'container'           => false,
                        'menu_class'          => 'menuright nav navbar-nav',
                        'fallback_cb'         => 'wp_page_menu',
                        'depth'               => 4,
                        'walker'              => new wp_bootstrap_mobile_navwalker()
                        )
                    ); 
                }
              ?>
            </div>
        </div><!--/.#mobile-menu-->

        <!--Left Menu--> 
        <?php if ( has_nav_menu( 'leftmenu' ) ) {?>
          <div class="common-menu hidden-xs hidden-sm">
            <div id="left-menu" class="common-menu-wrap">
                <?php
                    wp_nav_menu(  array(
                        'theme_location' => 'leftmenu',
                        'container'      => '', 
                        'menu_class'     => 'nav',
                        'fallback_cb'    => 'wp_page_menu',
                        'depth'          => 4,
                        'walker'         => new Megamenu_Walker()
                        )
                    ); 
                ?>      
            </div><!--/#left-menu-->    
          </div><!--/.common-menu-->    
        <?php } ?>

        <!--Logo--> 
        <div class="common-menu">
          <div class="cuisine-navbar-header">
            <div class="logo-wrapper">
                <h1>
                  <a class="rp-navbar-brand" href="<?php echo esc_url(site_url()); ?>">
                        <?php
                            if ( rp_options('logo') )
                           {
                                if( rp_options('logo-text-en') ) { ?>
                                    <h1> <?php echo esc_html( rp_options('logo-text') ); ?> </h1>
                                <?php }
                                else
                                {
                                    $logo = rp_options('logo');
                                    if( !empty($logo) ) {
                                      if(isset($_GET['logocolor'])){ ?>
                                          <img class="rp-logo img-responsive" src="<?php echo esc_url( get_template_directory_uri().'/images/logo2.png' ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                      <?php }else{ ?>
                                        <img class="rp-logo img-responsive" src="<?php echo esc_url( rp_options_url('logo','url') ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                      <?php
                                      }
                                    }else{
                                      echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                                    }
                                }
                           }
                            else
                           {
                              echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                           }
                        ?>
                     </a>
                  </h1>
              </div>     
          </div><!--/#cuisine-navbar-header-->   
        </div><!--/.common-menu -->   

        <!--Right Menu--> 
        <?php if ( has_nav_menu( 'menuright' ) ) { ?>
          <div class="common-menu hidden-xs hidden-sm">
            <div id="right-menu" class="common-menu-wrap">
                <?php 
                    wp_nav_menu(  array(
                        'theme_location' => 'menuright',
                        'container'      => '', 
                        'menu_class'     => 'nav',
                        'fallback_cb'    => 'wp_page_menu',
                        'depth'          => 4,
                        'walker'         => new Megamenu_Walker()
                        )
                    ); 
                ?>      
            </div><!--/#right-menu-->     
          </div><!--/.common-menu--> 
        <?php  } ?>

          <!--Search/Cart-->
        <div class="common-menu hidden-xs hidden-sm">
            <div class="menu-social text-right">
              <?php if ( rp_options('menu-login') ) { ?>
                  <?php if(is_user_logged_in()){
                      echo ' <div class="top-align home-login"><a href="'.wp_logout_url( home_url() ).'"><i class="fa fa-sign-out"></i></a></div>';
                    }else{ ?>
                      <div class="top-align home-login">
                        <a href="#sign-in" data-toggle="modal" data-target="#sign-in"><i class="fa fa-user"></i></a>
                    </div>   
                  <?php } ?>   
              <?php  } ?>  
              <?php if ( rp_options('menu-cart') ) {
                global $woocommerce;
                if($woocommerce) { ?>       
                <?php if(!is_page( 'cart' ) && !is_page('checkout')){ ?>        
                  <div class="top-align home-cart woocart">
                    <a href="<?php echo site_url('cart'); ?>" class="btn-cart"></a>
                            <div id="rplatform-woo-cart" class="woo-cart" style="display:none;">
                             <i class="fa fa-shopping-basket"></i> 
                                    <?php
                                        $has_products = '';
                                        $has_products = 'cart-has-products';
                                    ?>
                                   (<span class="woo-cart-items"><span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span></span>)
                                <?php the_widget( 'WC_Widget_Cart', '' ); ?>
                            </div>
                        
                  </div>
                <?php }?>
                <?php } 
              } ?>  
              <?php if ( rp_options('menu-search') ) { ?>
                <span class="top-align home-search-btn">
                  <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                </span>
              <?php  } ?>  
            </div>
        </div><!--/.common-menu-->
      </div><!--/.main-menu-wrap-->    
  </header><!--/.header-->

  <!--menu classic-->
  <?php } elseif ($menustyle == 'menuclassic') { ?>
    <header id="masthead" class="site-header header header-<?php echo $menustyle;?>">

      <!--Topbar-->
      <?php 
        if ( rp_options('topbar-en') ) {
          get_template_part('lib/topbar');
        } 
      ?>

        <!--Home Search--> 
        <div class="home-search-wrap">
          <div class="containerfluid">
              <div class="home-search">
                  <?php echo get_search_form();?>
                  <a href="#" class="hd-search-btn-close"><i class='fa fa-close'></i></a>
              </div>
           </div> 
        </div><!--/.home-search-wrap--> 

      <div class="container">
        <div class="row">
          <div class="main-menu-wrap clearfix">
            <!--Responsive Menu--> 
            <button type="button" class="navbar-toggle tablet-responsive" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div id="mobile-menu" class="hidden-lg hidden-md">
              <div class="collapse navbar-collapse tablet-responsive-collapse">
                  <?php 
                    if ( has_nav_menu( 'primary' ) ) {
                        wp_nav_menu( array(
                            'theme_location'      => 'primary',
                            'container'           => false,
                            'menu_class'          => 'nav navbar-nav',
                            'fallback_cb'         => 'wp_page_menu',
                            'depth'               => 3,
                            'walker'              => new wp_bootstrap_mobile_navwalker()
                            )
                        ); 
                    }
                    ?>
                </div>
            </div><!--/.#mobile-menu-->

            <!--Logo--> 
            <div class="col-md-3">
              <div class="leftlogo-navbar-header">
                <div class="logo-wrapper">
                    <h1>
                      <a class="leftlogo-navbar-brand" href="<?php echo esc_url(home_url()); ?>">
                            <?php
                                if ( rp_options('logo') )
                               {
                                    if( rp_options('logo-text-en') ) { ?>
                                        <h1> <?php echo esc_html( rp_options('logo-text') ); ?> </h1>
                                    <?php }
                                    else
                                    {

                                        $logo = rp_options('logo');
                                        if( !empty($logo) ) {
                                         if(isset($_GET['logocolor'])){ ?>
                                              <img class="rp-logo img-responsive" src="<?php echo esc_url( get_template_directory_uri().'/images/logo2.png' ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                          <?php }else{
                                             $fieldID = currentLang()?'logo-'.currentLang():'logo';
                                             $imageUrl = esc_url(rp_options_url($fieldID,'url'));
                                             $imageUrl = $imageUrl?$imageUrl:esc_url(rp_options_url('logo','url'));
                                             ?>
                                            <img class="rp-logo img-responsive" src="<?= $imageUrl; ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                          <?php
                                          }
                                        }else{
                                          echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                                        }
                                    }
                               } else
                               {
                                  echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                               }
                            ?>
                         </a>
                      </h1>
                  </div>     
              </div><!--/#cuisine-navbar-header-->   
            </div><!--/logo-->   

            <!--Main Menu--> 
            <div class="col-md-9">

                <?php if(is_active_sidebar('menu')){ ?>
                    <div class="menu-bar">
                        <?php dynamic_sidebar('menu'); ?>
                    </div>
                <?php } ?>

                <?php if ( has_nav_menu( 'primary' ) ) { ?>
                  <div id="main-menu" class="common-menu-wrap classic-menu hidden-xs hidden-sm">
                      <?php 
                          wp_nav_menu(  array(
                              'theme_location' => 'primary',
                              'container'      => '', 
                              'menu_class'     => 'nav',
                              'fallback_cb'    => 'wp_page_menu',
                              'depth'          => 4,
                              'walker'         => new Megamenu_Walker()
                              )
                          ); 
                      ?>                           
                  </div><!--/#main-menu-->
                <?php  } ?> 

                <?php if ( rp_options('menu3-search') ) { ?>
                <!--Search Classic-->
                  <span class="top-align classic-search home-search-btn">
                    <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                  </span>
                <?php  } ?>  
            </div><!--/.common-menu--> 
          </div><!--/.main-menu-wrap-->     
        </div><!--/.row--> 
      </div><!--/.container--> 
    </header><!--/.header-->
  <!--Logo center-->
  <?php } elseif ($menustyle == 'menulogocenter') { ?>

  <header id="masthead" class="site-header header header-<?php echo $menustyle;?>">
    
    <!--Add Topbar-->
    <?php 
      if ( rp_options('topbar-en') ) {
        get_template_part('lib/topbar');
      } 
    ?>

      <!--Home Search-->
      <div class="home-search-wrap">
        <div class="containerfluid">
            <div class="home-search">
                <?php echo get_search_form();?>
                <a href="#" class="hd-search-btn-close"><i class='fa fa-close'></i></a>
            </div>
         </div>
      </div><!--/.home-search-wrap-->

      <!--Menu Wrap--> 
      <div class="main-menu-wrap clearfix">

        <!--responsive Mobile Menu--> 
        <button type="button" class="navbar-toggle tablet-responsive" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div id="mobile-menu" class="hidden-lg hidden-md">
          <div class="collapse navbar-collapse tablet-responsive-collapse">
              <?php 
                if ( has_nav_menu( 'leftmenu' ) ) {
                    wp_nav_menu( array(
                        'theme_location'      => 'leftmenu',
                        'container'           => false,
                        'menu_class'          => 'leftmenu nav navbar-nav',
                        'fallback_cb'         => 'wp_page_menu',
                        'depth'               => 4,
                        'walker'              => new wp_bootstrap_mobile_navwalker()
                        )
                    ); 
                }
                ?>
              <?php 
                if ( has_nav_menu( 'menuright' ) ) {
                    wp_nav_menu( array(
                        'theme_location'      => 'menuright',
                        'container'           => false,
                        'menu_class'          => 'menuright nav navbar-nav',
                        'fallback_cb'         => 'wp_page_menu',
                        'depth'               => 4,
                        'walker'              => new wp_bootstrap_mobile_navwalker()
                        )
                    ); 
                }
              ?>
            </div>
        </div><!--/.#mobile-menu-->

        <!--Left Menu--> 
        <?php if ( has_nav_menu( 'leftmenu' ) ) {?>
          <div class="common-menu hidden-xs hidden-sm">
            <div id="left-menu" class="common-menu-wrap text-right">
                <?php
                    wp_nav_menu(  array(
                        'theme_location' => 'leftmenu',
                        'container'      => '', 
                        'menu_class'     => 'nav',
                        'fallback_cb'    => 'wp_page_menu',
                        'depth'          => 4,
                        'walker'         => new Megamenu_Walker()
                        )
                    ); 
                ?>      
            </div><!--/#left-menu-->    
          </div><!--/.common-menu-->    
        <?php } ?>

        <!--Logo--> 
        <div class="common-menu">
          <div class="cuisine-navbar-header">
            <div class="logo-wrapper">
                <h1>
                  <a class="rp-navbar-brand" href="<?php echo esc_url(site_url()); ?>">
                        <?php
                            if ( rp_options('logo') )
                           {
                                if( rp_options('logo-text-en') ) { ?>
                                    <h1> <?php echo esc_html( rp_options('logo-text') ); ?> </h1>
                                <?php }
                                else
                                {
                                    $logo = rp_options('logo');
                                    if( !empty($logo) ) {
                                      if(isset($_GET['logocolor'])){ ?>
                                          <img class="rp-logo img-responsive" src="<?php echo esc_url( get_template_directory_uri().'/images/logo-color.png' ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                      <?php }else{ ?>
                                        <img class="rp-logo img-responsive" src="<?php echo esc_url( rp_options_url('logo','url') ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                      <?php
                                      }
                                    }else{
                                      echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                                    }
                                }
                           }
                            else
                           {
                              echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                           }
                        ?>
                     </a>
                  </h1>
              </div>     
          </div><!--/#cuisine-navbar-header-->   
        </div><!--/.common-menu -->   

        <!--Right Menu--> 
        <?php if ( has_nav_menu( 'menuright' ) ) { ?>
          <div class="common-menu hidden-xs hidden-sm">
            <div id="right-menu" class="common-menu-wrap">
                <?php 
                    wp_nav_menu(  array(
                        'theme_location' => 'menuright',
                        'container'      => '', 
                        'menu_class'     => 'nav',
                        'fallback_cb'    => 'wp_page_menu',
                        'depth'          => 4,
                        'walker'         => new Megamenu_Walker()
                        )
                    ); 
                ?>      
            </div><!--/#right-menu-->     
          </div><!--/.common-menu--> 
        <?php  } ?>   
      </div><!--/.main-menu-wrap-->    
  </header><!--/.header-->  

  <!--Left Logo with Search/cart-->
  <?php }  elseif ($menustyle == 'menuwithsearch') { ?>

  <header id="masthead" class="site-header header header-<?php echo $menustyle;?>">

      <!--Topbar-->
      <?php 
        if ( rp_options('topbar-en') ) {
          get_template_part('lib/topbar');
        } 
      ?>

      <!--Home Search--> 
      <div class="home-search-wrap">
        <div class="containerfluid">
            <div class="home-search">
                <?php echo get_search_form();?>
                <a href="#" class="hd-search-btn-close"><i class='fa fa-close'></i></a>
            </div>
         </div> 
      </div><!--/.home-search-wrap--> 


    <div class="container">
        <div class="row">

          <!--Menu Wrap-->   
          <div class="main-menu-wrap clearfix">
              
              <!--Responsive Menu--> 
              <button type="button" class="navbar-toggle tablet-responsive" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div id="mobile-menu" class="hidden-lg hidden-md">
                <div class="collapse navbar-collapse tablet-responsive-collapse">
                    <?php 
                      if ( has_nav_menu( 'primary' ) ) {
                          wp_nav_menu( array(
                              'theme_location'      => 'primary',
                              'container'           => false,
                              'menu_class'          => 'nav navbar-nav',
                              'fallback_cb'         => 'wp_page_menu',
                              'depth'               => 3,
                              'walker'              => new wp_bootstrap_mobile_navwalker()
                              )
                          ); 
                      }
                      ?>
                  </div>
              </div><!--/.#mobile-menu-->

              <!--Logo--> 
              <div class="col-xs-6 col-sm-6 col-md-3 common-menu">
                <div class="leftlogo-navbar-header">
                  <div class="logo-wrapper">
                      <h1>
                        <a class="leftlogo-navbar-brand" href="<?php echo esc_url(site_url()); ?>">
                              <?php
                                  if ( rp_options('logo') )
                                 {
                                      if( rp_options('logo-text-en') ) { ?>
                                          <h1> <?php echo esc_html( rp_options('logo-text') ); ?> </h1>
                                      <?php }
                                      else
                                      {
                                          $logo = rp_options('logo');
                                          if( !empty($logo) ) {
                                           if(isset($_GET['logocolor'])){ ?>
                                                <img class="rp-logo img-responsive" src="<?php echo esc_url( get_template_directory_uri().'/images/logo2.png' ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                            <?php }else{ ?>
                                              <img class="rp-logo img-responsive" src="<?php echo esc_url( rp_options_url('logo','url') ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                            <?php
                                            }
                                          }else{
                                            echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                                          }
                                      }
                                 }
                                  else
                                 {
                                    echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                                 }
                              ?>
                           </a>
                        </h1>
                    </div>     
                </div><!--/#cuisine-navbar-header-->   
              </div><!--/.col-sm-3-->   

              <!--Main Menu--> 
              <?php if ( has_nav_menu( 'primary' ) ) { ?>
                <?php if ( rp_options('menu2-login') ||  rp_options('menu2-cart') || rp_options('menu2-search') ) { ?>
                  <div class="col-md-6 common-menu hidden-xs hidden-sm">
                <?php  } else { ?>
                  <div class="col-md-9 common-menu hidden-xs hidden-sm">
                <?php  } ?> 
                  <div id="main-menu" class="common-menu-wrap">
                      <?php 
                          wp_nav_menu(  array(
                              'theme_location' => 'primary',
                              'container'      => '', 
                              'menu_class'     => 'nav',
                              'fallback_cb'    => 'wp_page_menu',
                              'depth'          => 4,
                              'walker'         => new Megamenu_Walker()
                              )
                          ); 
                      ?>      
                  </div><!--/#main-menu-->     
                </div><!--/.col-sm-9--> 
              <?php  } ?> 

              <?php if ( rp_options('menu2-login') ||  rp_options('menu2-cart') || rp_options('menu2-search') ) { ?>
                <!--Search/Cart-->         
                <div class="col-xs-6 col-sm-6 col-md-3 menu-social common-menu text-right">
                    <?php if ( rp_options('menu2-login') ) { ?>
                      <?php if(is_user_logged_in()){
                            echo ' <div class="top-align home-login"><a href="'.wp_logout_url( home_url() ).'"><i class="fa fa-sign-out"></i></a></div>';
                        }else{ ?>
                          <div class="top-align home-login">
                            <a href="#sign-in" data-toggle="modal" data-target="#sign-in"><i class="fa fa-user"></i></a>
                        </div>   
                      <?php } ?>   
                    <?php  } ?>  
                    <?php if ( rp_options('menu2-cart') ) {           
                      global $woocommerce;
                      if($woocommerce) { ?>               
                        <div class="top-align home-cart woocart">
                          <a href="<?php echo site_url('cart'); ?>" class="btn-cart"></a>
                              <div id="rplatform-woo-cart" class="woo-cart" style="display:none;">
                               <i class="fa fa-shopping-basket"></i> 
                                      <?php
                                          $has_products = '';
                                          $has_products = 'cart-has-products';
                                      ?>
                                     (<span class="woo-cart-items"><span class="<?php echo $has_products; ?>"><?php echo $woocommerce->cart->cart_contents_count; ?></span></span>)
                                  <?php the_widget( 'WC_Widget_Cart', '' ); ?>
                              </div>
                              
                        </div>
                      <?php }
                    } ?> 
                    <?php if ( rp_options('menu2-search') ) { ?>
                      <span class="top-align home-search-btn">
                        <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                      </span>
                    <?php  } ?> 
                </div>
              <?php  } ?> 

          </div><!--/.main-menu-wrap-->     
        </div><!--/.row--> 
    </div><!--/.container--> 
  </header><!--/.header-->

  <!--Classic nav-->
<?php } else { ?>

  <header id="masthead" class="site-header header header-<?php echo $menustyle;?>">

    <!--Topbar-->
    <?php 
      if ( rp_options('topbar-en') ) {
        get_template_part('lib/topbar');
      } 
    ?>

      <!--Home Search--> 
      <div class="home-search-wrap">
        <div class="containerfluid">
            <div class="home-search">
                <?php echo get_search_form();?>
                <a href="#" class="hd-search-btn-close"><i class='fa fa-close'></i></a>
            </div>
         </div> 
      </div><!--/.home-search-wrap--> 

    <div class="container">
      <div class="row">
        <div class="main-menu-wrap clearfix">
          <!--Responsive Menu--> 
          <button type="button" class="navbar-toggle tablet-responsive" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div id="mobile-menu" class="hidden-lg hidden-md">
            <div class="collapse navbar-collapse tablet-responsive-collapse">
                <?php 
                  if ( has_nav_menu( 'primary' ) ) {
                      wp_nav_menu( array(
                          'theme_location'      => 'primary',
                          'container'           => false,
                          'menu_class'          => 'nav navbar-nav',
                          'fallback_cb'         => 'wp_page_menu',
                          'depth'               => 3,
                          'walker'              => new wp_bootstrap_mobile_navwalker()
                          )
                      ); 
                  }
                  ?>
              </div>
          </div><!--/.#mobile-menu-->

          <!--Logo--> 
          <div class="col-md-3">
            <div class="leftlogo-navbar-header">
              <div class="logo-wrapper">
                  <h1>
                    <a class="leftlogo-navbar-brand" href="<?php echo esc_url(site_url()); ?>">
                          <?php
                              if ( rp_options('logo') )
                             {
                                  if( rp_options('logo-text-en') ) { ?>
                                      <h1> <?php echo esc_html( rp_options('logo-text') ); ?> </h1>
                                  <?php }
                                  else
                                  {
                                      $logo = rp_options('logo');
                                      if( !empty($logo) ) {
                                       if(isset($_GET['logocolor'])){ ?>
                                            <img class="rp-logo img-responsive" src="<?php echo esc_url( get_template_directory_uri().'/images/logo2.png' ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                        <?php }else{ ?>
                                          <img class="rp-logo img-responsive" src="<?php echo esc_url( rp_options_url('logo','url') ); ?>" alt="<?php  esc_html_e( 'Logo', 'rp' ); ?>" title="<?php  esc_html_e( 'Logo', 'rp' ); ?>">
                                        <?php
                                        }
                                      }else{
                                        echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                                      }
                                  }
                             } else
                             {
                                echo '<span class="text-navbar-header">'. esc_html(get_bloginfo('name')). '</span>';
                             }
                          ?>
                       </a>
                    </h1>
                </div>     
            </div><!--/#cuisine-navbar-header-->   
          </div><!--/logo-->   

          <!--Main Menu--> 
          <div class="col-md-9">
              <?php if ( has_nav_menu( 'primary' ) ) { ?>
                <div id="main-menu" class="common-menu-wrap classic-menu hidden-xs hidden-sm">
                    <?php 
                        wp_nav_menu(  array(
                            'theme_location' => 'primary',
                            'container'      => '', 
                            'menu_class'     => 'nav',
                            'fallback_cb'    => 'wp_page_menu',
                            'depth'          => 4,
                            'walker'         => new Megamenu_Walker()
                            )
                        ); 
                    ?>                           
                </div><!--/#main-menu-->     
              <?php  } ?> 

              <?php if ( rp_options('menu3-search') ) { ?>
              <!--Search Classic--> 
                <span class="top-align classic-search home-search-btn">
                  <a href="#" class="hd-search-btn"><i class="fa fa-search"></i></a>
                </span>
              <?php  } ?>  
          </div><!--/.common-menu--> 
        </div><!--/.main-menu-wrap-->     
      </div><!--/.row--> 
    </div><!--/.container--> 
  </header><!--/.header-->
<?php }?>

  <!-- sign in form -->
    <div id="sign-form">
         <div id="sign-in" class="modal fade">
            <div class="modal-dialog modal-md">
                 <div class="modal-content">
                     <div class="modal-header">
                         <i class="fa fa-close close" data-dismiss="modal"></i>
                     </div>
                     <div class="modal-body">
                         <h3><?php esc_html_e('Already Registered','rp'); ?></h3>
                         <form id="login" action="login" method="post">
                            <div class="login-error alert alert-info" role="alert"></div>
                            <input type="text"  id="username" name="username" class="form-control" placeholder="<?php esc_html_e('User Name','rp'); ?>">
                            <input type="password" id="password" name="password" class="form-control" placeholder="<?php esc_html_e('Password','rp'); ?>">
                            <input type="submit" class="btn btn-default btn-block submit_button"  value="Login" name="submit">
                            <a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><strong><?php esc_html_e('Forgot password?','rp'); ?></strong></a>
                            <p><?php esc_html_e('Not a member?','rp'); ?> <a href="<?php echo esc_url(get_permalink(get_option('register_page_id'))); ?>"><strong><?php esc_html_e('Join today','rp'); ?></strong></a></p>
                            <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                         </form>
                     </div>
                 </div>
             </div> 
         </div>
    </div> <!-- end sign-in form -->
    <div id="logout-url" class="hidden"><?php echo wp_logout_url( esc_url( home_url('/') )); ?></div>
    