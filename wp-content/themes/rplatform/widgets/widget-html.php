<?php
class BCatHtmlWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('bcat-html-widget', __('Html | BCat', 'rp'),
           ['description' => __('Html', 'rp')]);
    }

    public function form($instance) {
        $script = '';
        $status = '';

        if (!empty($instance)) {
            $script = $instance['script'];
            $status = esc_attr($instance['status']);
        }


        $Id = $this->get_field_id('script');
        $Name = $this->get_field_name('script');
        echo '<label for="' . $Id . '">'.__( 'Html', 'rp' ).': </label>';
        echo '<textarea class="widefat" rows="6" id="' . $Id . '" name="' . $Name . '">' . $script . '</textarea></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'rp') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['script'] = $newInstance['script'];
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $script = do_shortcode($instance['script']);

        if(count($instance)>0) {

            ?>

            <!--scripts-->
            <?= $script; ?>
            <!--scripts-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("BCatHtmlWidget");
});