<?php
class BCatContactsUSWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('bcat-contact-us-widget', __('Contact Us | BCat', 'rp'),
           ['description' => __('Contact us buttons', 'rp')]);
    }

    public function form($instance) {
        $leftButtonTitle = '';
        $leftButtonUrl = '';
        $rightButtonTitle = '';
        $rightButtonUrl = '';
        $img = '';
        $background = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $leftButtonTitle = esc_attr($instance['leftButtonTitle']);
            $leftButtonUrl = esc_attr(esc_attr($instance['leftButtonUrl']));
            $rightButtonTitle = esc_attr($instance['rightButtonTitle']);
            $rightButtonUrl = esc_attr($instance['rightButtonUrl']);
            $img = esc_attr($instance['img']);
            $background = esc_attr($instance['background']);
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('leftButtonTitle');
        $Name = $this->get_field_name('leftButtonTitle');
        echo '<p><label for="' . $Id . '">' . __('Left Button Title', 'rp') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $leftButtonTitle . '"></p>';

        $Id = $this->get_field_id('leftButtonUrl');
        $Name = $this->get_field_name('leftButtonUrl');
        echo '<p><label for="' . $Id . '">' . __('Left Button Url', 'rp') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $leftButtonUrl . '"></p>';

        $Id = $this->get_field_id('rightButtonTitle');
        $Name = $this->get_field_name('rightButtonTitle');
        echo '<p><label for="' . $Id . '">' . __('Right Button Title', 'rp') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $rightButtonTitle . '"></p>';

        $Id = $this->get_field_id('rightButtonUrl');
        $Name = $this->get_field_name('rightButtonUrl');
        echo '<p><label for="' . $Id . '">' . __('Right Button Url', 'rp') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $rightButtonUrl . '"></p>';

        $Id = $this->get_field_id('img');
        $Name = $this->get_field_name('img');
        echo '<p><label for="'.$Id.'">' . __('Image', 'rp') . ': </label><br />';
        echo '<img class="js_custom_upload_media_img '.$Id.'_img" src="'.$img.'"/>';
        echo '<input type="text" class="widefat js_custom_upload_media_input '.$Id.'_url" name="' . $Name . '" value="' . $img . '" />';
        echo '<input type="button" id="'.$Id.'" class="js_custom_upload_media" value="' . __('Select Image', 'rp') . '" />';
        echo '<span data-id="'.$Id.'" class="remove-image">' . __('Remove Image', 'rp') . '</span></p>';

        $Id = $this->get_field_id('background');
        $Name = $this->get_field_name('background');
        echo '<p><label for="' . $Id . '">' . __('Background', 'rp') . ': </label>';
        echo '<input class="color_field widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $background . '"></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'rp') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'rp') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = [];
        $values['leftButtonTitle'] = htmlentities($newInstance['leftButtonTitle']);
        $values['leftButtonUrl'] = htmlentities($newInstance['leftButtonUrl']);
        $values['rightButtonTitle'] = htmlentities($newInstance['rightButtonTitle']);
        $values['rightButtonUrl'] = htmlentities($newInstance['rightButtonUrl']);
        $values['img'] = htmlentities($newInstance['img']);
        $values['background'] = htmlentities($newInstance['background']);
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $leftButtonTitle = $instance['leftButtonTitle'];
        $leftButtonUrl =$instance['leftButtonUrl'];
        $rightButtonTitle = $instance['rightButtonTitle'];
        $rightButtonUrl = $instance['rightButtonUrl'];
        $img = $instance['img'];
        $background = $instance['background'];
        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }
        if($background){
            $background = "background-color: $background; ";
        }

        if(count($instance)>0) {

            ?>

            <div<?= $sectionId; ?> class="col-lg-12" style="<?= $background ?>">
                <div class="container">
                    <div class="row">
                        <div class="widget-wrap col-md-12">
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <a href="<?= $leftButtonUrl ?>" class="bordered-button-2"><?= $leftButtonTitle ?></a>
                            </div>
                            <div class="col-sm-12 col-md-2 col-lg-2">
                                <?php if($img): ?>
                                <img width="75" height="74" src="<?= $img ?>" class="vc_single_image-img attachment-thumbnail" alt="">
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <a href="<?= $rightButtonUrl ?>" class="bordered-button-2"><?= $rightButtonTitle ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php

        }

    }

}

// queue up the necessary js
function bcat_enqueue()
{
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
}
add_action('admin_enqueue_scripts', 'bcat_enqueue');

add_action("widgets_init", function () {
    register_widget("BCatContactsUSWidget");
});