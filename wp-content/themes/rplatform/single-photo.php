<?php
/*
 * Template name: Шаблон страницы фото
 * Template Post Type: photo
 * */
wp_enqueue_style ('theme-style', get_template_directory_uri().'/css/video/style.css');
get_header();

?>
<section id="main">
    <?php get_template_part('lib/sub-header')?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="/photo" class="btn-return orange"><i class="fa fa-angle-left" aria-hidden="true"></i><?= __( 'All photo', 'rp' ); ?></a>
            </div>
            <div class="col-xs-12">
                <div class="media-wrap">
                    <div class="media-caption">
                        <h3><?php echo the_title(); ?></h3>
                        <span class="media-date orange"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date( 'd.m.y'); ?></span>
                        <span class="media-location blue"><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo get_post_meta(get_the_ID(),'city',true); ?>, <?php echo get_post_meta(get_the_ID(),'country',true); ?></span>
                    </div>
                    <div class="media-image">
                        <img src="<?php echo wp_get_attachment_image_url(
                            get_post_meta(get_the_ID(),'photo',true),
                            'rp-large'
                        ) ?>" alt="">
                    </div>
                    <div class="media-description">
                        <p><?= apply_filters( 'the_content', get_post_meta(get_the_ID(),'desc',true) ); ?></p>
                    </div>
                    </div>
                    <div class="single-page-social">
                        <?php get_template_part('post-format/social-buttons')?>
                    </div>
                <?php
                if ( rp_options('blog-comment') ) {
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
                }
                ?>
                </div>

            </div>
        </div>
    </div>
</section>
<?php get_footer();