<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="featured-wrap">
        <div class="entry-content-gallery">
            <?php if(function_exists('rwmb_meta')){ ?>
                <?php  if ( get_post_meta(get_the_ID(),'rplatform_gallery_images') ) { ?>
                    <?php $slides = get_post_meta(get_the_ID(),'rplatform_gallery_images'); ?>
                        <?php if(count($slides) > 0) { ?>
                            <div id="blog-gallery-slider<?php echo get_the_ID(); ?>" class="carousel slide blog-gallery-slider">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php $slide_no = 1; ?>
                                    <?php foreach( $slides as $slide ) { ?>
                                    <div class="item <?php if($slide_no == 1) echo 'active'; ?>">
                                        <?php $images = wp_get_attachment_image_src( $slide, 'rp-large' ); ?>
                                        <img class="img-responsive" src="<?php echo esc_url($images[0]); ?>" alt="<?php  esc_html_e( 'image', 'rp' ); ?>">
                                    </div>
                                    <?php $slide_no++; ?>
                                    <?php } ?>
                                </div>
                                <!-- Controls -->
                                <a class="left carousel-left" href="#blog-gallery-slider<?php echo get_the_ID(); ?>" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right carousel-right" href="#blog-gallery-slider<?php echo get_the_ID(); ?>" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div><!--/.entry-content-gallery-->
        <?php if (  rp_options('blog-date') ) { ?>
            <div class="news-date"><time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date( get_option('date_format')); ?></time></div>
        <?php }?> 
    </div>

    <?php get_template_part( 'post-format/entry-content' ); ?> 

</article> <!--/#post -->



