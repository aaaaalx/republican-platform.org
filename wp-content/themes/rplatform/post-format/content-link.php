<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="featured-wrap">
        <div class="entry-link-post-format">
	        <?php if(function_exists('rwmb_meta')){ ?>
	        	<?php  if ( get_post_meta( get_the_ID(), 'rplatform_link',true ) ) { ?>
	            	<h4><?php echo esc_url( get_post_meta( get_the_ID(), 'rplatform_link',true ) ); ?></h4>
	            <?php } ?>
	        <?php } ?>
        </div> 
    	<?php if (  rp_options('blog-date') ) { ?>
        	<div class="news-date"><time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date( get_option('date_format')); ?></time></div>
        <?php }?>   
    </div>
    <?php get_template_part( 'post-format/entry-content' ); ?> 
</article> <!--/#post -->