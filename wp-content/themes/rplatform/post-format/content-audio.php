<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="featured-wrap">
        <?php if(function_exists('rwmb_meta')){ ?>
            <?php  if ( get_post_meta( get_the_ID(), 'rplatform_audio_code',true ) ) { ?>
    	        <div class="entry-audio embed-responsive embed-responsive-16by9">
    	            <?php echo get_post_meta( get_the_ID(), 'rplatform_audio_code',true ); ?>
    	        </div> <!--/.audio-content -->
    	    <?php } ?>
        <?php } ?>
        <?php if (  rp_options('blog-date') ) { ?>
            <div class="news-date"><time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date( get_option('date_format')); ?></time></div>
        <?php }?> 
    </div>    
    <?php get_template_part( 'post-format/entry-content' ); ?> 
</article> <!--/#post -->
