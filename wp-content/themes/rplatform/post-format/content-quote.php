<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="featured-wrap">
        <div class="entry-quote-post-format">
            <blockquote>
            <?php if(function_exists('rwmb_meta')){ ?>
                <?php  if ( get_post_meta( get_the_ID(), 'rplatform_qoute',true ) ) { ?>
                    <p><?php echo esc_html(get_post_meta( get_the_ID(), 'rplatform_qoute',true )); ?></p>
                    <small><?php echo esc_html(get_post_meta( get_the_ID(), 'rplatform_qoute_author',true )); ?></small>
                <?php } ?>
            <?php } ?>
            </blockquote>
        </div> 
        <?php if (  rp_options('blog-date') ) { ?>
            <div class="news-date"><time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date( get_option('date_format')); ?></time></div>
        <?php }?>    
    </div>
    <?php get_template_part( 'post-format/entry-content' ); ?> 
</article> <!--/#post -->