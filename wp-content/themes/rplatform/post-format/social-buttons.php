<?php
$permalink = get_permalink(get_the_ID());
$titleget = get_the_title();
$tw_username = '';
$tw_username = rp_options('twitter-username');
$media_url ='';
$date = get_the_date( 'd.m.y');
if( has_post_thumbnail( get_the_ID() ) ){
    $thumb_src =  wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'rp-large' );
    $media_url = $thumb_src[0];
}
elseif($_image = wp_get_attachment_image_url(get_post_meta(get_the_ID(),'photo',true), 'rp-large')){
    $media_url = $_image;
}
?>
<div>
    <a
            class="share-button single-social-info facebook"
            data-share-url="<?= esc_url($permalink); ?>"
            data-share-network="facebook"
            data-share-title="<?= $titleget ?>"
            data-share-text="<?= esc_html(rp_excerpt_max_char(300)); ?>"
            data-share-media="<?= esc_url( $media_url ); ?>"
            href="#">
        <i class="fa fa-facebook-square"></i>
        <?php _e('Share With Facebook','rp');?>
    </a>
    <a
            class="share-button single-social-info twitter"
            data-share-url="<?= esc_url($permalink); ?>"
            data-share-network="twitter"
            data-share-text="<?= $titleget ?>"
            data-share-via="<?= $tw_username ?>"
            data-share-media="<?= esc_url( $media_url ); ?>"
            href="#">
        <i class="fa fa-twitter"></i>
        <?php _e('Tweet With Twitter','rp');?>
    </a>
</div>