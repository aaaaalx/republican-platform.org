<?php global $rplatform_options; ?>

<div class="entry-blog">
    <div class="entry-headder">
        <h2 class="entry-title blog-entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
            <?php if ( is_sticky() && is_home() && ! is_paged() ) { ?>
            <sup class="featured-post"><i class="fa fa-star-o"></i><?php esc_html_e( 'Sticky', 'rp' ) ?></sup>
            <?php } ?>
        </h2> <!-- //.entry-title --> 
    </div>
    <?php if ( rp_options('blog-author') ) { ?>
        <?php if ( get_the_author_meta('first_name') != "" || get_the_author_meta('last_name') != "" ) { ?>
            <span class="meta-author"><?php esc_html_e( 'By', 'rp' ) ?> <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php echo get_the_author_meta('first_name');?> <?php echo get_the_author_meta('last_name');?></a></span>
        <?php } else { ?>
            <span class="meta-author"><?php esc_html_e( 'By', 'rp' ) ?> <?php the_author_posts_link() ?></span>
        <?php }?>
    <?php }?>  

    <?php if ( rp_options('blog-category') ) { ?>
        <span class="meta-category"><?php esc_html_e( 'Category: ', 'rp' ) ?> <?php echo get_the_category_list(', '); ?></span>
    <?php }?>
    
    <div class="entry-summary clearfix">
        <?php if ( is_single() ) {
            the_content();
        } else {
            echo rp_excerpt_max_charlength(300);
            if ( isset($rplatform_options['blog-continue-en']) && $rplatform_options['blog-continue-en']==1 ) {
                if ( isset($rplatform_options['blog-continue']) && $rplatform_options['blog-continue'] ) {
                    $continue = esc_html($rplatform_options['blog-continue']);
                    echo '<p class="wrap-btn-style"><a class="btn-blog" href="'.get_permalink().'">'. esc_html__($continue, 'rp' ) .' <i class="fa fa-long-arrow-right"></i></a></p>';
                } else {
                    echo '<p class="wrap-btn-style"><a class="btn-blog" href="'.get_permalink().'">'. esc_html__( 'Read More', 'rp' ) .' <i class="fa fa-long-arrow-right"></i></a></p>';
                } 
            }
     
        } 
        wp_link_pages( array(
            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'rp' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
        ) );
        ?>
    </div> <!-- //.entry-summary -->
</div> <!--/.entry-meta -->



