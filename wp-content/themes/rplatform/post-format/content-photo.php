<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( get_post_meta(the_ID(),'mini_photo', true) ){ ?>
        <div class="featured-wrap">
            <a href="<?php the_permalink(); ?>" rel="bookmark">
                <img src="<?= get_post_meta(the_ID(),'mini_photo', true); ?>" alt="" class="img-responsive">
            </a>
            <?php if (  rp_options('blog-date') ) { ?>
                <div class="news-date"><time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date( get_option('date_format')); ?></time></div>
            <?php }?>
        </div>
    <?php }
    get_template_part( 'post-format/entry-content' ); ?>
</article> <!--/#po